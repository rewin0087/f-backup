Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # sidekiq web ui monitoring
  # FIXME: scope to admin user only
  require 'sidekiq/web'
  if Rails.env.production?
    authenticate :admin_user, lambda { |u| u.administrator? } do
      mount Sidekiq::Web => '/admin/sidekiq'
    end
  else
    mount Sidekiq::Web => '/sidekiq'
  end


  devise_for :user, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    password: 'password',
    confirmation: 'verification'
  }

  devise_scope :user do
    # student registration routes
    get 'student/register' => 'devise_extentions/student#new'
    post 'student/register' => 'devise_extentions/student#create'
    # mentor registration routes
    get 'mentor/register' => 'devise_extentions/mentor#new'
    post 'mentor/register' => 'devise_extentions/mentor#create'
  end

  resources :careers, only: [:index]
  resources :contacts, only: [:index]
  resources :dashboard, only: [:index]
  resources :landing, only: [:index]
  resources :services, only: [:index]
  resources :scholastics, only: [:index] do
    collection do
      get 'universities'
      get 'languages'
      get 'majors'
      get 'countries'
      get 'timezones'
      get 'search_tags'
    end
  end

  resources :mentors, only: [:index] do
    get 'search', on: :collection
    resources :bookmark, only: [:create, :destroy]
    member do
      post :notify_student
    end
  end

  resources :students, only: [:index] do
    get 'search', on: :collection
  end

  resources :profile, except: [:index, :destroy, :create, :new] do
    member do
      get 'mentors'
      get 'students'
      get 'account_settings'
      put 'primary_photo'
      match 'private_message', via: [:get, :post]
    end

    resources :recommendations, only: [:create]
    resources :dashboard, only: [:index]
    resources :messenger, only: [:show] do
      post :message
      get :video_call
      put :decline_video_call
    end

    resources :mentors, only: [:update] do
      put 'account_settings', on: :member
    end

    resources :students, only: [:update] do
      put 'account_settings', on: :member
    end

    resources :orders, only: [:index, :new, :create, :show] do
      member do
        put :instruction
        put :comment
        put :accept
        put :confirm
        put :in_progress
        put :completed
        put :decline
        put :cancel
        put :close
      end
    end

    resources :tasks, only: [:show, :update] do
      member do
        get :upload_request_file
        put :upload_request_file
        get :upload_revised_file
        put :upload_revised_file
        put :in_progress
        put :done
        put :completed
        put :instruction
        put :comment
        get :appointment
        put :appointment
      end

      resources :ratings
    end

    resources :appointments, except: [:destroy, :update] do
      member do
        put :decline
        put :confirm
        put :cancel
      end

      collection do
        post :request_schedule
      end
    end

    resources :schedules do
      member do
        put :close
      end

      get :checker, on: :collection
    end

    resources :images, only: [:index, :create, :destroy] do
      put :set_as_primary_photo, on: :member
    end

    resources :documents, only: [:index, :create, :destroy]
    resources :notifications, only: [:index]
    resources :transactions do
      post :hook, on: :member
    end

    resources :wallets
    resources :schools, only: [:create, :update, :destroy]
    resources :videos, only: [:create, :destroy]
    resources :alternative_contacts, only: [:create, :update, :destroy]
  end

  root 'landing#index'
end
