Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_HOST_AND_PORT'] }
end

Sidekiq.configure_client do |config|
  config.redis = { url: ENV['REDIS_HOST_AND_PORT'], password: nil }
end

Sidekiq.default_worker_options = { 'backtrace' => true }
