if Rails.env.production?
  Rack::Timeout.service_timeout = 6000
else
  Rack::Timeout::Logger.disable
end