CarrierWave.configure do |config|
  config.fog_credentials = {
    provider:              'AWS',                        # required
    aws_access_key_id:     ENV['S3_AWS_ACCESS_KEY_ID'],     # required
    aws_secret_access_key: ENV['S3_AWS_SECRET_ACCESS_KEY'], # required
    region:                ENV['S3_AWS_REGION'],            # optional, defaults to 'us-east-1'
    host:                  nil, # ENV['S3_AWS_HOST'],              # optional, defaults to nil
    endpoint:              nil, # ENV['S3_AWS_ENDPOINT']           # optional, defaults to nil
  }
  config.fog_directory  = 'afriendabroad'                         # required
  config.fog_public     = false                                        # optional, defaults to true
  config.fog_attributes = { 'Cache-Control' => "max-age=#{365.day.to_i}" } # optional, defaults to {}
  config.storage = Rails.env.production? ? :fog : :file
end