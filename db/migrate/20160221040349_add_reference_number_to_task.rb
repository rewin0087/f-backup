class AddReferenceNumberToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :reference_number, :string
  end
end
