class CreateAlternativeContacts < ActiveRecord::Migration
  def change
    create_table :alternative_contacts do |t|
      t.string :account_type
      t.string :account_value
      t.references :contact, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
