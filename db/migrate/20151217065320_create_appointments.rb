class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :timezone
      t.string :status, default: 'PENDING'
      t.references :schedule, index: true, foreign_key: true
      t.references :mentor, index: true, foreign_key: true
      t.references :task, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
