class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :message
      t.string :status, default: 'PENDING'
      t.references :user, index: true, foreign_key: true
      t.string :notifiable_type
      t.references :notifiable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
