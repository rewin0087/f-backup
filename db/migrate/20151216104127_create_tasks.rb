class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :description
      t.string :requested_file
      t.string :revised_file
      t.string :status, default: 'CREATED'
      t.text :additional_instructions
      t.text :additional_comments
      t.decimal :price
      t.decimal :actual_price
      t.string :currency
      t.string :currency_symbol
      t.string :task_type, default: 'PAYED'
      t.references :service, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.references :mentor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
