class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.text :university
      t.text :major
      t.references :scholastic, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
