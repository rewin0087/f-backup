class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :source
      t.string :status
      t.references :imageable, polymorphic: true, index: true
      t.string :imageable_type

      t.timestamps null: false
    end
  end
end
