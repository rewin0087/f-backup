class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.text :additional_instructions
      t.text :additional_comments
      t.decimal :price
      t.decimal :actual_price
      t.string :currency
      t.string :currency_symbol
      t.references :student, index: true, foreign_key: true
      t.references :mentor, index: true, foreign_key: true
      t.string :status, default: 'PENDING'

      t.timestamps null: false
    end
  end
end
