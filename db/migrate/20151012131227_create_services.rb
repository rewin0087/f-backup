class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title
      t.string :description
      t.decimal :price
      t.string :currency
      t.string :currency_symbol
      t.timestamps null: false
    end
  end
end
