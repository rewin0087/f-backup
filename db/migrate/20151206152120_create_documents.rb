class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.text :description
      t.text :original_filename
      t.string :filename
      t.references :documentable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
