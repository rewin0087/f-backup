class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.float :speed
      t.float :knowledgeability
      t.float :overall_rating
      t.float :average
      t.string :feedbacks
      t.references :task, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
