class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.text :target_countries, array: true
      t.text :target_degree, array: true
      t.text :looking_for
      t.text :questions_to_mentor
      t.text :remarks
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end