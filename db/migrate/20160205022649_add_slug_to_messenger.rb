class AddSlugToMessenger < ActiveRecord::Migration
  def change
    add_column :messengers, :slug, :string
  end
end
