class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.text :url
      t.text :source
      t.references :videoable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
