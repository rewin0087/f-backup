class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.date :start_date
      t.time :start_time
      t.date :end_date
      t.time :end_time
      t.string :timezone
      t.string :status, default: 'AVAILABLE'
      t.references :student, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
