class AddUserToAlternativeContact < ActiveRecord::Migration
  def change
    add_reference :alternative_contacts, :user, index: true, foreign_key: true
  end
end
