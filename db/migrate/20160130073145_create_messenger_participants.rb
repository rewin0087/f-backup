class CreateMessengerParticipants < ActiveRecord::Migration
  def change
    create_table :messenger_participants do |t|
      t.references :user, index: true, foreign_key: true
      t.references :messenger, index: true, foreign_key: true
      t.string :user_type

      t.timestamps null: false
    end
  end
end
