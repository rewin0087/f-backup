class CreateMentors < ActiveRecord::Migration
  def change
    create_table :mentors do |t|
      t.string :university_email
      t.string :home_town
      t.text :degree, array: true
      t.text :undergraduate_universities
      t.boolean :undergraduate_universities_early_acceptance
      t.text :graduate_universities
      t.boolean :graduate_universities_early_acceptance
      t.decimal :basic_consultation_charge
      t.string :basic_consultation_charge_currency, default: '$'
      t.string :work
      t.boolean :mentor_student
      t.text :reason
      t.text :free_tip
      t.text :essay_services, array: true
      t.text :experience
      t.text :awards_and_honors
      t.boolean :display_ratings
      t.boolean :display_reviews
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
