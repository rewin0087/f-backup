class AddPendingBalanceToWallet < ActiveRecord::Migration
  def change
    add_column :wallets, :pending_balance, :decimal
  end
end
