# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160306074833) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "full_name",              limit: 255
    t.string   "username",               limit: 255
    t.string   "timezone",               limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "alternative_contacts", force: :cascade do |t|
    t.string   "account_type",  limit: 255
    t.string   "account_value", limit: 255
    t.integer  "contact_id",    limit: 4
    t.string   "contact_type",  limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id",       limit: 4
  end

  add_index "alternative_contacts", ["contact_type", "contact_id"], name: "index_alternative_contacts_on_contact_type_and_contact_id", using: :btree
  add_index "alternative_contacts", ["user_id"], name: "index_alternative_contacts_on_user_id", using: :btree

  create_table "appointments", force: :cascade do |t|
    t.string   "timezone",    limit: 255
    t.string   "status",      limit: 255, default: "PENDING"
    t.integer  "schedule_id", limit: 4
    t.integer  "mentor_id",   limit: 4
    t.integer  "task_id",     limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "appointments", ["mentor_id"], name: "index_appointments_on_mentor_id", using: :btree
  add_index "appointments", ["schedule_id"], name: "index_appointments_on_schedule_id", using: :btree
  add_index "appointments", ["task_id"], name: "index_appointments_on_task_id", using: :btree

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "student_id", limit: 4
    t.integer  "mentor_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "bookmarks", ["mentor_id"], name: "index_bookmarks_on_mentor_id", using: :btree
  add_index "bookmarks", ["student_id"], name: "index_bookmarks_on_student_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.text     "description",       limit: 65535
    t.text     "original_filename", limit: 65535
    t.string   "filename",          limit: 255
    t.integer  "documentable_id",   limit: 4
    t.string   "documentable_type", limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "documents", ["documentable_type", "documentable_id"], name: "index_documents_on_documentable_type_and_documentable_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "source",         limit: 255
    t.string   "status",         limit: 255
    t.integer  "imageable_id",   limit: 4
    t.string   "imageable_type", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "images", ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id", using: :btree

  create_table "mentors", force: :cascade do |t|
    t.string   "university_email",                            limit: 255
    t.string   "home_town",                                   limit: 255
    t.text     "degree",                                      limit: 65535
    t.text     "undergraduate_universities",                  limit: 65535
    t.boolean  "undergraduate_universities_early_acceptance", limit: 1
    t.text     "graduate_universities",                       limit: 65535
    t.boolean  "graduate_universities_early_acceptance",      limit: 1
    t.decimal  "basic_consultation_charge",                                 precision: 10
    t.string   "basic_consultation_charge_currency",          limit: 255,                  default: "$"
    t.string   "work",                                        limit: 255
    t.boolean  "mentor_student",                              limit: 1
    t.text     "reason",                                      limit: 65535
    t.text     "free_tip",                                    limit: 65535
    t.text     "essay_services",                              limit: 65535
    t.text     "experience",                                  limit: 65535
    t.text     "awards_and_honors",                           limit: 65535
    t.boolean  "display_ratings",                             limit: 1
    t.boolean  "display_reviews",                             limit: 1
    t.integer  "user_id",                                     limit: 4
    t.datetime "created_at",                                                                             null: false
    t.datetime "updated_at",                                                                             null: false
  end

  add_index "mentors", ["user_id"], name: "index_mentors_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "text",         limit: 65535
    t.integer  "messenger_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "messages", ["messenger_id"], name: "index_messages_on_messenger_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "messenger_participants", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "messenger_id", limit: 4
    t.string   "user_type",    limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "messenger_participants", ["messenger_id"], name: "index_messenger_participants_on_messenger_id", using: :btree
  add_index "messenger_participants", ["user_id"], name: "index_messenger_participants_on_user_id", using: :btree

  create_table "messengers", force: :cascade do |t|
    t.integer  "task_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "slug",       limit: 255
  end

  add_index "messengers", ["task_id"], name: "index_messengers_on_task_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.string   "message",         limit: 255
    t.string   "status",          limit: 255, default: "PENDING"
    t.integer  "user_id",         limit: 4
    t.string   "notifiable_type", limit: 255
    t.integer  "notifiable_id",   limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "url",             limit: 255
  end

  add_index "notifications", ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.text     "additional_instructions", limit: 65535
    t.text     "additional_comments",     limit: 65535
    t.decimal  "price",                                 precision: 10
    t.decimal  "actual_price",                          precision: 10
    t.string   "currency",                limit: 255
    t.string   "currency_symbol",         limit: 255
    t.integer  "student_id",              limit: 4
    t.integer  "mentor_id",               limit: 4
    t.string   "status",                  limit: 255,                  default: "PENDING"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.string   "slug",                    limit: 255
    t.string   "reference_number",        limit: 255
  end

  add_index "orders", ["mentor_id"], name: "index_orders_on_mentor_id", using: :btree
  add_index "orders", ["student_id"], name: "index_orders_on_student_id", using: :btree

  create_table "ratings", force: :cascade do |t|
    t.float    "speed",            limit: 24
    t.float    "knowledgeability", limit: 24
    t.float    "overall_rating",   limit: 24
    t.float    "average",          limit: 24
    t.string   "feedbacks",        limit: 255
    t.integer  "task_id",          limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ratings", ["task_id"], name: "index_ratings_on_task_id", using: :btree

  create_table "recommendations", force: :cascade do |t|
    t.text     "message",    limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "recommendations", ["user_id"], name: "index_recommendations_on_user_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.date     "start_date"
    t.time     "start_time"
    t.date     "end_date"
    t.time     "end_time"
    t.string   "timezone",   limit: 255
    t.string   "status",     limit: 255, default: "AVAILABLE"
    t.integer  "student_id", limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "schedules", ["student_id"], name: "index_schedules_on_student_id", using: :btree

  create_table "schools", force: :cascade do |t|
    t.text     "university",      limit: 65535
    t.text     "major",           limit: 65535
    t.integer  "scholastic_id",   limit: 4
    t.string   "scholastic_type", limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "schools", ["scholastic_type", "scholastic_id"], name: "index_schools_on_scholastic_type_and_scholastic_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.text     "description",     limit: 65535
    t.decimal  "price",                         precision: 10
    t.string   "currency",        limit: 255
    t.string   "currency_symbol", limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "service_type",    limit: 255
  end

  create_table "students", force: :cascade do |t|
    t.text     "target_countries",    limit: 65535
    t.text     "target_degree",       limit: 65535
    t.text     "looking_for",         limit: 65535
    t.text     "questions_to_mentor", limit: 65535
    t.text     "remarks",             limit: 65535
    t.integer  "user_id",             limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "students", ["user_id"], name: "index_students_on_user_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.text     "description",             limit: 65535
    t.string   "requested_file",          limit: 255
    t.string   "revised_file",            limit: 255
    t.string   "status",                  limit: 255,                  default: "CREATED"
    t.text     "additional_instructions", limit: 65535
    t.text     "additional_comments",     limit: 65535
    t.decimal  "price",                                 precision: 10
    t.decimal  "actual_price",                          precision: 10
    t.string   "currency",                limit: 255
    t.string   "currency_symbol",         limit: 255
    t.string   "task_type",               limit: 255,                  default: "PAYED"
    t.integer  "service_id",              limit: 4
    t.integer  "order_id",                limit: 4
    t.integer  "mentor_id",               limit: 4
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.string   "slug",                    limit: 255
    t.string   "reference_number",        limit: 255
  end

  add_index "tasks", ["mentor_id"], name: "index_tasks_on_mentor_id", using: :btree
  add_index "tasks", ["order_id"], name: "index_tasks_on_order_id", using: :btree
  add_index "tasks", ["service_id"], name: "index_tasks_on_service_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.string   "transaction_type",    limit: 255
    t.string   "payment_gateway",     limit: 255
    t.decimal  "amount",                            precision: 10
    t.text     "notification_params", limit: 65535
    t.string   "status",              limit: 255
    t.string   "transaction_id",      limit: 255
    t.datetime "purchased_at"
    t.integer  "wallet_id",           limit: 4
    t.integer  "order_id",            limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  add_index "transactions", ["order_id"], name: "index_transactions_on_order_id", using: :btree
  add_index "transactions", ["wallet_id"], name: "index_transactions_on_wallet_id", using: :btree

  create_table "user_services", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "service_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "user_services", ["service_id"], name: "index_user_services_on_service_id", using: :btree
  add_index "user_services", ["user_id"], name: "index_user_services_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "full_name",              limit: 255
    t.string   "username",               limit: 255
    t.string   "role",                   limit: 255
    t.text     "about_me",               limit: 65535
    t.text     "additional_information", limit: 65535
    t.string   "primary_photo",          limit: 255
    t.string   "current_country",        limit: 255
    t.string   "other_degree",           limit: 255
    t.string   "phone_number",           limit: 255
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.integer  "failed_attempts",        limit: 4,     default: 0,  null: false
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "timezone",               limit: 255
    t.string   "slug",                   limit: 255
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.text     "url",            limit: 65535
    t.text     "source",         limit: 65535
    t.integer  "videoable_id",   limit: 4
    t.string   "videoable_type", limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "videos", ["videoable_type", "videoable_id"], name: "index_videos_on_videoable_type_and_videoable_id", using: :btree

  create_table "wallets", force: :cascade do |t|
    t.decimal  "amount",                    precision: 10
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.decimal  "pending_balance",           precision: 10
  end

  add_index "wallets", ["user_id"], name: "index_wallets_on_user_id", using: :btree

  add_foreign_key "alternative_contacts", "users"
  add_foreign_key "appointments", "mentors"
  add_foreign_key "appointments", "schedules"
  add_foreign_key "appointments", "tasks"
  add_foreign_key "bookmarks", "mentors"
  add_foreign_key "bookmarks", "students"
  add_foreign_key "mentors", "users"
  add_foreign_key "messages", "messengers"
  add_foreign_key "messages", "users"
  add_foreign_key "messenger_participants", "messengers"
  add_foreign_key "messenger_participants", "users"
  add_foreign_key "messengers", "tasks"
  add_foreign_key "notifications", "users"
  add_foreign_key "orders", "mentors"
  add_foreign_key "orders", "students"
  add_foreign_key "ratings", "tasks"
  add_foreign_key "recommendations", "users"
  add_foreign_key "schedules", "students"
  add_foreign_key "students", "users"
  add_foreign_key "tasks", "mentors"
  add_foreign_key "tasks", "orders"
  add_foreign_key "tasks", "services"
  add_foreign_key "transactions", "orders"
  add_foreign_key "transactions", "wallets"
  add_foreign_key "user_services", "services"
  add_foreign_key "user_services", "users"
  add_foreign_key "wallets", "users"
end
