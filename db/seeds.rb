# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Service::ALL.map do |service|
  Service.where(title: service[:title], description: service[:description], price: service[:price], currency: service[:currency], currency_symbol: service[:currency_symbol], service_type: service[:service_type]).
  first_or_create
end

AdminUser.where(email: 'admin@example.com', username: 'admin', first_name: 'Administrator', last_name: '', full_name: 'Administrator', timezone: 'Asia/Manila').first_or_create(password: 'password', password_confirmation: 'password')