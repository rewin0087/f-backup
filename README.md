### FOR DEVELOPER GUIDE: ###
  * Please check ./.DEVELOPERS.rdoc
### FOR PRODUCTION SETUP: ###
  * Please check ./.PRODUCTION_SETUP.rdoc and ./server_configs/* for actual templates.*

### FOR APP SPECIFIC SETUP CONFIGS: ###
  Please copy all template config file from ./config/* and create file without .template and put the correct values of each configs.

  * files to be created:
    * config/application.yml
    * config/database.yml
    * config/secrets.yml

### REQUIREMENTS TO INSTALL: ###
  * rbenv | rvm
  * ruby 2.3.0
  * bundler
  * minimagick
  * mysql
  * redis
  * rails 4.2.2
  * sidekiq

### TO RUN AFTER ALL REQUIREMENTS ARE INSTALLED: (run from the root directory of the app) ###
  * cmd: bundle install
  * cmd: rake db:setup or rake db:create db:migrate db:seed
  * cmd: rails server
  * cmd: rake websocket_rails:start_server (separated terminal) # for standalone websocket server
  * cmd: redis-server (separated terminal)
  * cmd: bundle exec sidekiq (separated terminal)