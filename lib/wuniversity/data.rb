class Wuniversity::Data
  RAW = JSON.parse(File.read(File.join(File.dirname(__FILE__), 'data.json')))

  ALL = RAW.map{|u| u['name']}.flatten

  COUNTRIES = ISO3166::Country.all.map{|d| d.data['name']}
end