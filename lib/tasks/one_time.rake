namespace :one_time do
  desc "generate temporary values for services"
  task generate_services: :environment do
    services = [
      ['Consultation', 'School selection; CV review; detailed application timeline; student life; etc'],
      ['Scholarships', 'Finding scholarships and application review'],
      ['Recommendation', 'Recommendation letter questions'],
      ['Mock Interviews', 'Mock Interviews'],
      ['College Visit', 'Personal Tour Guide'],
      ['Find a job', 'How to find a job after college']
    ]
    services.map do |value|
      Service.where(title: value[0], description: value[1]).first_or_create(title: value[0], description: value[1])
    end
  end
end

namespace :import_data do
  task import_mentor_from_csv_file: :environment do
    require 'open-uri'
    require 'csv'
    # set path as env variable when running rake task: file_path='path/to/file.csv'
    Rails.logger.info('running import script for mentors csv...')
    p 'running import script for mentors csv...'
    path = ENV['file_path'] || '../../../mentors.csv'
    Rails.logger.info('opening file and parsing csv...')
    p 'opening file and parsing csv...'
    data = File.open(path, 'r:ISO-8859-1')
    csv = CSV.parse(data)

    Rails.logger.info('csv successfully opened and parse!')
    p 'csv successfully opened and parsed!'

    legend = {
      username: 0,
      email: 1,
      first_name: 2,
      last_name: 3,
      current_country: 4,
      home_town: 5,
      degrees: 6,
      majors: 7,
      undergraduate_universities: 8,
      services_offered: 9,
      reason: 10,
      free_tip: 11,
      schools_last_attended: 12,
      additional_information: 13
    }

    row_count = csv.count
    csv.each_with_index do |row, index|
      next if index == 0

      Rails.logger.info("importing row #{index+1} of #{row_count}...")
      p "importing row #{index+1} of #{row_count}..."

      degrees = row[legend[:degrees]].split(',').map{ |value| User::DEGREES.key(value) } if row[legend[:degrees]].present?
      services = row[legend[:services_offered]].split(',') if row[legend[:services_offered]].present?
      service_ids = Service.where(title: services).pluck(:id)
      majors = row[legend[:majors]].split(',') if row[legend[:majors]].present?
      undergraduate_universities = row[legend[:undergraduate_universities]].split(',') if row[legend[:undergraduate_universities]].present?
      schools_last_attended = row[legend[:schools_last_attended]].split(',') if row[legend[:schools_last_attended]].present?

      user = User.new(
        username: row[legend[:username]],
        email: row[legend[:email]],
        first_name: row[legend[:first_name]],
        last_name: row[legend[:last_name]],
        current_country: row[legend[:current_country]],
        service_ids: service_ids,
        role: User.mentor,
        password: '12345678',
        timezone: Timezone::Zone.list.sample[:zone],
        additional_information: row[legend[:additional_information]]
      )
      user.save(validate: false)

      mentor = Mentor.new(
        home_town: row[legend[:home_town]],
        degree: degrees,
        majors: majors,
        undergraduate_universities: undergraduate_universities,
        reason: row[legend[:reason]],
        free_tip: row[legend[:free_tip]],
        schools_last_attended: schools_last_attended,
        user_id: user.id
      )
      mentor.save(validate: false)
    end

    Rails.logger.info('import complete!')
    p 'import complete!'
  end
end
