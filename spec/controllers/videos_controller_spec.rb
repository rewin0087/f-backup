# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  url            :text(65535)
#  source         :text(65535)
#  videoable_id   :integer
#  videoable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe VideosController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service]) }
  let!(:user_student) { create(:user_student, services: [consult_service]) }
  let!(:video) { create(:video, videoable: user_mentor.mentor) }

  describe 'when user is not logged in' do
    context 'POST create' do
      before do
        post :create, profile_id: user_student.id, video: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'DELETE destroy' do
      before do
        delete :destroy, profile_id: user_student.id, id: video.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'POST create' do
        context 'success' do
          before do
            stub_request(:get, "https://www.youtube.com/watch?v=ac3HkriqdGQ").
             with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
             to_return(:status => 200, :body => "<html><head><meta property='og:video:secure_url' content='https://www.youtube.com/embed/ac3HkriqdGQ'></head><body></body></html>", :headers => {})
            post :create, profile_id: user_mentor.id, video: { url: 'https://www.youtube.com/watch?v=ac3HkriqdGQ', videoable_id: user_mentor.mentor.id, videoable_type: 'Mentor' }, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('videos/create') }
          it { expect(assigns(:video).errors.empty?).to eql(true) }
          it { expect(assigns(:video).url).to eql('https://www.youtube.com/watch?v=ac3HkriqdGQ') }
          it { expect(assigns(:video).source).to eql('https://www.youtube.com/embed/ac3HkriqdGQ') }
          it { expect(assigns(:video).videoable).to eql(user_mentor.mentor) }
        end

        context 'error' do
          before do
            post :create, profile_id: user_mentor.id, video: { url: '', videoable_id: user_mentor.id, videoable_type: 'Mentor' }, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('videos/create') }
          it { expect(assigns(:video).errors.empty?).to eql(false) }
          it { expect(assigns(:video).errors.full_messages.first).to eql('Url can\'t be blank') }
          it { expect(assigns(:video).errors.size).to eql(1) }
        end
      end

      context 'DELETE destroy' do
        before do
          delete :destroy, profile_id: user_mentor.id, id: video.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('videos/destroy') }
        it { expect(assigns(:video).destroyed?).to eql(true) }
        it { expect(Video.exists?(video.id)).to eql(false) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'POST create' do
        it { expect { post :create, profile_id: user_student.id, video: {} }.to raise_error(ActionController::RoutingError) }
      end

      context 'DELETE destroy' do
        it { expect { delete :destroy, profile_id: user_student.id, id: video.id }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
