# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

require 'rails_helper'

RSpec.describe OrdersController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index, profile_id: user_student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET new' do
      before do
        get :new, profile_id: user_student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST create' do
      before do
        post :create, profile_id: user_student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET show' do
      before do
        get :show, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT instruction' do
      before do
        put :instruction, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT comment' do
      before do
        put :comment, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT accept' do
      before do
        put :accept, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT confirm' do
      before do
        put :confirm, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT in_progress' do
      before do
        put :in_progress, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT completed' do
      before do
        put :completed, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT decline' do
      before do
        put :decline, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT cancel' do
      before do
        put :cancel, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT close' do
      before do
        put :close, profile_id: user_student.id, id: order.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        before do
          get :index, profile_id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/index') }
        it { expect(assigns(:orders)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:orders).to_a).to be_a(Array) }
        it { expect(assigns(:orders)).not_to be_empty }
        it { expect(assigns(:orders).size).to eql(1) }
      end

      context 'GET new' do
        it { expect { get :new, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST create' do
        it { expect { post :create, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET show' do
        before do
          get :show, profile_id: user_mentor.id, id: order.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/show') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order).mentor).to eql(user_mentor.mentor) }
      end

      context 'PUT instruction' do
        it { expect { put :instruction, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT comment' do
        before do
          xhr :put, :comment, profile_id: user_mentor.id, id: order.id, format: :js, order: { additional_comments: 'Test Additional Comments' }
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/comment') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect(assigns(:order).additional_comments).to eql('Test Additional Comments') }
        it { expect { xhr :put, :comment, profile_id: user_mentor.id, id: order.id, format: :js, order: { additional_comments: 'Test Additional Comments' } }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT accept' do
        before do
          xhr :put, :accept, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/accept') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.accepted) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :accept, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT confirm' do
        it { expect { put :confirm, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT in_progress' do
        it { expect { put :in_progress, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT completed' do
        it { expect { put :completed, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT decline' do
        before do
          xhr :put, :decline, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/decline') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.declined) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :decline, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT cancel' do
        it { expect { put :cancel, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT close' do
        it { expect { put :close, profile_id: user_mentor.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET index' do
        before do
          get :index, profile_id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/index') }
        it { expect(assigns(:orders)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:orders).to_a).to be_a(Array) }
        it { expect(assigns(:orders)).not_to be_empty }
        it { expect(assigns(:orders).size).to eql(1) }
      end

      context 'GET new' do
        before do
          get :new, profile_id: user_student.id, mentor: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/new') }
        it { expect(assigns(:order).mentor).to eql(user_mentor.mentor) }
        it { expect(assigns(:order).student).to eql(user_student.student) }
      end

      context 'POST create' do
        let(:order_params) {
          {
            student_id: user_student.student.id,
            mentor_id: user_mentor.mentor.id,
            tasks: {
              service_id: [
                consult_service.id.to_s,
                basic_review.id.to_s
              ]
            }
          }
        }

        context 'when student has enough funds' do
          before do
            post :create, profile_id: user_student.id, order: order_params, format: :js
          end

          it { expect(assigns(:order).id).not_to be_nil }
          it { expect(assigns(:order).errors.empty?).to eql(true) }
          it { expect(assigns(:order).tasks.size).to eql(2) }
          it { expect(assigns(:order).status).to eql(Order.pending) }
          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('orders/create') }
          it { expect { post :create, profile_id: user_student.id, order: order_params, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
        end

        context 'when student dont have enough funds' do
          before do
            wallet = user_student.wallet
            wallet.amount = 0
            wallet.save
            post :create, profile_id: user_student.id, order: order_params, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('orders/create') }
          it { expect(assigns(:order).id).to be_nil }
          it { expect(assigns(:order).tasks.size).to eql(2) }
          it { expect(assigns(:order).status).to eql(Order.pending) }
          it { expect(assigns(:order).errors.empty?).to eql(false) }
          it { expect(assigns(:order).errors.full_messages.first).to eql('You have an insufficient balance. Kindly add funds your wallet.') }
        end
      end

      context 'GET show' do
        before do
          get :show, profile_id: user_student.id, id: order.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/show') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order).student).to eql(user_student.student) }
      end

      context 'PUT instruction' do
        before do
          xhr :put, :instruction, profile_id: user_mentor.id, id: order.id, format: :js, order: { additional_instructions: 'Test Additional Instructions' }
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/instruction') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).additional_instructions).to eql('Test Additional Instructions') }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :instruction, profile_id: user_mentor.id, id: order.id, format: :js, order: { additional_instructions: 'Test Additional Instructions' } }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT comment' do
        it { expect { put :comment, profile_id: user_student.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT accept' do
        it { expect { put :accept, profile_id: user_student.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT confirm' do
        before do
          xhr :put, :confirm, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/confirm') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.confirmed) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :confirm, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT in_progress' do
        before do
          xhr :put, :in_progress, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/in_progress') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.in_progress) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :in_progress, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT completed' do
        before do
          order.tasks.map {|t| create(:appointment, mentor: order.mentor, task: t, status: Appointment.completed) if t.service.service_type == Service.appointment_service }
          order.tasks.map(&:completed!)
          xhr :put, :completed, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/completed') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.completed) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :completed, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end

      context 'PUT decline' do
        it { expect { put :decline, profile_id: user_student.id, id: order.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT cancel' do
        before do
          xhr :put, :cancel, profile_id: user_mentor.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/cancel') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.canceled) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
        it { expect { xhr :put, :cancel, profile_id: user_mentor.id, id: order.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(2) }
      end

      context 'PUT close' do
        before do
          order.tasks.map {|t| create(:appointment, mentor: order.mentor, task: t, status: Appointment.completed) if t.service.service_type == Service.appointment_service }
          order.tasks.map(&:completed!)
        end

        before do
          xhr :put, :close, profile_id: user_student.id, id: order.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('orders/close') }
        it { expect(assigns(:order).id).to eql(order.id) }
        it { expect(assigns(:order)).to be_a(Order) }
        it { expect(assigns(:order).status).to eql(Order.closed) }
        it { expect(assigns(:order).errors.empty?).to eql(true) }
      end
    end
  end
end
