# == Schema Information
#
# Table name: wallets
#
#  id      :integer          not null, primary key
#  amount  :decimal(10, )
#  user_id :integer
#

require 'rails_helper'

RSpec.describe WalletsController, :type => :controller do

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET create" do
    it "returns http success" do
      get :create
      expect(response).to have_http_status(:success)
    end
  end

end
