# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

require 'rails_helper'

RSpec.describe MentorsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:essay_review) { create(:essay_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [essay_review]) }
  let!(:mentor_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_mentor.mentor) }
  let!(:student_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_student.student) }
  let(:user_params) {
    {
      # user params
      username: 'testusername',
      email: 'test@email.com',
      first_name: 'testfirstname',
      last_name: 'testlastname',
      phone_number: 'testphonenumber',
      current_country: 'Japan',
      about_me: 'Test About Me',
      additional_information: 'Test Additional Information',
      timezone: 'Japan',
      other_degree: 'Test Other Degree',
      service_ids: [consult_service.id],
      # mentor params
      university_email: 'universitytest@email.com',
      home_town: 'Japan',
      display_reviews: 1,
      display_ratings: 1,
      undergraduate_universities: ['University of Baguio'],
      degree: [FFaker::Education.degree],
      experience: 'Test experience',
      awards_and_honors: 'Test Awards and Honors'
    }
  }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:top_mentors)).to be_a(ActiveRecord::Relation) }
      it { expect(assigns(:top_mentors).to_a).to be_a(Array) }
      it { expect(assigns(:top_mentors).size).to eql(2) }
      it { expect(assigns(:top_mentors)).not_to be_nil }
      it { expect(assigns(:top_suggested)).not_to be_a(ActiveRecord::Relation) }
      it { expect(assigns(:top_suggested).to_a).to be_a(Array) }
      it { expect(assigns(:top_suggested)).to be_empty }
      it { expect(assigns(:search_tags)).to be_a(Array) }
      it { expect(assigns(:search_tags)).to be_empty }
      it { expect(response).to render_template('layouts/branding') }
      it { expect(response).to render_template('mentors/index') }
    end

    context 'GET search' do
      context 'when no keyword pass' do
        before do
          get :search
        end

        it { expect(assigns(:mentors)).not_to be_nil }
        it { expect(assigns(:mentors).size).to eql(2) }
        it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:mentors).to_a).to be_a(Array) }
        it { expect(assigns(:keyword)).to be_a(Array) }
        it { expect(assigns(:keyword)).to be_empty }
        it { expect(assigns(:search_tags)).to be_a(Array) }
        it { expect(assigns(:search_tags)).to be_empty }
        it { expect(assigns(:country)).to be_a(Array) }
        it { expect(assigns(:country)).to be_empty }
        it { expect(assigns(:majors)).to be_a(Array) }
        it { expect(assigns(:majors)).to be_empty }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('mentors/search') }
      end

      context 'when filters are presents' do
        before do
          get :search,
            keyword: [user_mentor.first_name],
            country: [user_mentor.current_country],
            majors: user_mentor.mentor.schools.map(&:major),
            degrees: user_mentor.degree,
            services: user_mentor.service_ids
        end

        it { expect(assigns(:mentors)).not_to be_nil }
        it { expect(assigns(:mentors).size).to eql(1) }
        it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:mentors).to_a).to be_a(Array) }
        it { expect(assigns(:keyword)).to be_a(Array) }
        it { expect(assigns(:keyword)).not_to be_empty }
        it { expect(assigns(:keyword).size).to eql(1) }
        it { expect(assigns(:keyword)).to eql([user_mentor.first_name]) }
        it { expect(assigns(:search_tags)).to be_a(Array) }
        it { expect(assigns(:search_tags)).not_to be_empty }
        it { expect(assigns(:search_tags)).to eql([user_mentor.first_name]) }
        it { expect(assigns(:search_tags).size).to eql(1) }
        it { expect(assigns(:country)).to be_a(Array) }
        it { expect(assigns(:country)).not_to be_empty }
        it { expect(assigns(:country)).to eql([user_mentor.current_country]) }
        it { expect(assigns(:majors)).to be_a(Array) }
        it { expect(assigns(:majors)).to eql(user_mentor.mentor.schools.map(&:major)) }
        it { expect(assigns(:majors)).not_to be_empty }
        it { expect(assigns(:majors).size).to eql(1) }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('mentors/search') }
      end
    end

    context 'PUT update' do
      before { put :update,  profile_id: user_mentor.id, id: user_mentor.mentor.id }

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT account_settings' do
      before { put :account_settings,  profile_id: user_mentor.id, id: user_mentor.mentor.id }

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST notify_student' do
      before { put :notify_student,  id: user_student.id }

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as user mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        it { expect { get :index }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET search' do
        it { expect { get :search }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT update' do
        before do
          put :update,
            profile_id: user_mentor.id,
            id: user_mentor.mentor.id,
            user: user_params,
            format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:profile).about_me).to eql(user_params[:about_me]) }
        it { expect(assigns(:profile).additional_information).to eql(user_params[:additional_information]) }
        it { expect(assigns(:profile).display_reviews).to eql(true) }
        it { expect(assigns(:profile).display_ratings).to eql(true) }
        it { expect(assigns(:profile).experience).to eql(user_params[:experience]) }
        it { expect(assigns(:profile).awards_and_honors).to eql(user_params[:awards_and_honors]) }
        it { expect(assigns(:profile).undergraduate_universities).to eql(user_params[:undergraduate_universities]) }
        it { expect(assigns(:profile).mentor.display_reviews).to eql(true) }
        it { expect(assigns(:profile).mentor.display_ratings).to eql(true) }
        it { expect(assigns(:profile).mentor.experience).to eql(user_params[:experience]) }
        it { expect(assigns(:profile).mentor.awards_and_honors).to eql(user_params[:awards_and_honors]) }
        it { expect(assigns(:profile).mentor.undergraduate_universities).to eql(user_params[:undergraduate_universities]) }
        it { expect(assigns(:documents).to_a).to be_a(Array) }
        it { expect(assigns(:documents)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:documents).empty?).to eql(true) }
        it { expect(response).to render_template('mentors/update') }
      end

      context 'PUT account_settings' do
        before do
          put :update,
            profile_id: user_mentor.id,
            id: user_mentor.mentor.id,
            user: user_params,
            format: :js
        end

        it { expect(assigns(:profile).username).to eql(user_params[:username]) }
        it { expect(assigns(:profile).first_name).to eql(user_params[:first_name]) }
        it { expect(assigns(:profile).last_name).to eql(user_params[:last_name]) }
        it { expect(assigns(:profile).email).to eql(user_params[:email]) }
        it { expect(assigns(:profile).username).to eql(user_params[:username]) }
        it { expect(assigns(:profile).university_email).to eql(user_params[:university_email]) }
        it { expect(assigns(:profile).phone_number).to eql(user_params[:phone_number]) }
        it { expect(assigns(:profile).degree).to eql(user_params[:degree]) }
        it { expect(assigns(:profile).service_ids).to eql(user_params[:service_ids]) }
        it { expect(assigns(:profile).timezone).to eql(user_params[:timezone]) }
        it { expect(assigns(:profile).mentor.degree).to eql(user_params[:degree]) }
        it { expect(assigns(:profile).mentor.university_email).to eql(user_params[:university_email]) }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('mentors/update') }
      end

      context 'POST notify_student' do
        before do
          post :notify_student, id: user_student.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('mentors/notify_student') }
        it { expect { post :notify_student, id: user_student.id, format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
      end
    end

    context 'as user student' do
      before { sign_in user_student }

      context 'GET index' do
        before do
          get :index
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:top_mentors)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:top_mentors).to_a).to be_a(Array) }
        it { expect(assigns(:top_mentors).size).to eql(2) }
        it { expect(assigns(:top_mentors)).not_to be_nil }
        it { expect(assigns(:top_suggested)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:top_suggested).to_a).to be_a(Array) }
        it { expect(assigns(:top_suggested).size).to eql(1) }
        it { expect(assigns(:search_tags)).to be_a(Array) }
        it { expect(assigns(:search_tags)).to be_empty }
        it { expect(response).to render_template('layouts/application') }
        it { expect(response).to render_template('mentors/index') }
      end

      context 'GET search' do
        context 'when no keyword pass' do
          before do
            get :search
          end

          it { expect(assigns(:mentors)).not_to be_nil }
          it { expect(assigns(:mentors).size).to eql(2) }
          it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:mentors).to_a).to be_a(Array) }
          it { expect(assigns(:mentors).to_a).to eql([user_mentor.mentor, user_mentor2.mentor]) }
          it { expect(assigns(:keyword)).to be_a(Array) }
          it { expect(assigns(:keyword)).to be_empty }
          it { expect(assigns(:search_tags)).to be_a(Array) }
          it { expect(assigns(:search_tags)).to be_empty }
          it { expect(assigns(:country)).to be_a(Array) }
          it { expect(assigns(:country)).to be_empty }
          it { expect(assigns(:majors)).to be_a(Array) }
          it { expect(assigns(:majors)).to be_empty }
          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('mentors/search') }
        end

        context 'when filters are presents' do
          before do
            get :search,
              keyword: [user_mentor.first_name],
              country: [user_mentor.current_country],
              majors: user_mentor.mentor.schools.map(&:major),
              degrees: user_mentor.degree,
              services: user_mentor.service_ids
          end

          it { expect(assigns(:mentors)).not_to be_nil }
          it { expect(assigns(:mentors).size).to eql(1) }
          it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:mentors).to_a).to be_a(Array) }
          it { expect(assigns(:keyword)).to be_a(Array) }
          it { expect(assigns(:keyword)).not_to be_empty }
          it { expect(assigns(:keyword).size).to eql(1) }
          it { expect(assigns(:keyword)).to eql([user_mentor.first_name]) }
          it { expect(assigns(:search_tags)).to be_a(Array) }
          it { expect(assigns(:search_tags)).not_to be_empty }
          it { expect(assigns(:search_tags)).to eql([user_mentor.first_name]) }
          it { expect(assigns(:search_tags).size).to eql(1) }
          it { expect(assigns(:country)).to be_a(Array) }
          it { expect(assigns(:country)).not_to be_empty }
          it { expect(assigns(:country)).to eql([user_mentor.current_country]) }
          it { expect(assigns(:majors)).to be_a(Array) }
          it { expect(assigns(:majors)).to eql(user_mentor.mentor.schools.map(&:major)) }
          it { expect(assigns(:majors)).not_to be_empty }
          it { expect(assigns(:majors).size).to eql(1) }
          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('mentors/search') }
        end
      end

      context 'PUT update' do
        it { expect { put :update, profile_id: user_student.id, id: user_student.student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT account_settings' do
        it { expect { put :account_settings, profile_id: user_student.id, id: user_student.student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST notify_student' do
        it { expect { post :notify_student, profile_id: user_student.id, id: user_student.student.id }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
