# == Schema Information
#
# Table name: students
#
#  id                  :integer          not null, primary key
#  target_countries    :text(65535)
#  target_degree       :text(65535)
#  looking_for         :text(65535)
#  questions_to_mentor :text(65535)
#  remarks             :text(65535)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

RSpec.describe StudentsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:essay_review) { create(:essay_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:user_student2) { create(:user_student, services: [essay_review]) }
  let!(:mentor_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_mentor.mentor) }
  let!(:student_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_student.student) }
  let(:user_params) {
    {
      # user params
      username: 'testusername',
      email: 'test@email.com',
      first_name: 'testfirstname',
      last_name: 'testlastname',
      phone_number: 'testphonenumber',
      current_country: 'Japan',
      about_me: 'Test About Me',
      additional_information: 'Test Additional Information',
      timezone: 'Japan',
      other_degree: 'Test Other Degree',
      service_ids: [consult_service.id],
      # student params
      target_degree: [FFaker::Education.degree],
      target_countries: [FFaker::Address.country],
      looking_for: 'Test Looking for'
    }
  }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index
      end

      it { expect(response).to have_http_status 200 }
      it { expect(response).to render_template('students/index') }
      it { expect(response).to render_template('layouts/branding') }
      it { expect(assigns(:students).to_a).to be_a(Array) }
      it { expect(assigns(:students)).to be_a(ActiveRecord::Relation) }
      it { expect(assigns(:students).size).to eql(2) }
    end

    context 'GET search' do
      before do
        get :search
      end

      it { expect(response).to have_http_status 200 }
      it { expect(response).to render_template('students/search') }
      it { expect(response).to render_template('layouts/branding') }
    end

    context 'PUT update' do
      before do
        put :update, profile_id: user_student.id, id: user_student.student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT account_settings' do
      before do
        put :account_settings, profile_id: user_student.id, id: user_student.student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as user mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        before do
          get :index
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('students/index') }
        it { expect(response).to render_template('layouts/application') }
        it { expect(assigns(:students).to_a).to be_a(Array) }
        it { expect(assigns(:students)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:students).size).to eql(2) }
      end

      context 'GET search' do
        before do
          get :search
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('students/search') }
        it { expect(response).to render_template('layouts/application') }
      end

      context 'PUT update' do
        it { expect { put :update, profile_id: user_mentor.id, id: user_mentor.mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT account_settings' do
        it { expect { put :update, profile_id: user_mentor.id, id: user_mentor.mentor.id }.to raise_error(ActionController::RoutingError) }
      end
    end

    context 'as user student' do
      before { sign_in user_student }

      context 'GET index' do
        it { expect { get :index }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET search' do
        it { expect { get :search }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT update' do
        before do
          put :update,
            profile_id: user_student.id,
            id: user_student.student.id,
            user: user_params,
            format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:profile).about_me).to eql(user_params[:about_me]) }
        it { expect(assigns(:profile).looking_for).to eql(user_params[:looking_for]) }
        it { expect(assigns(:profile).additional_information).to eql(user_params[:additional_information]) }
        it { expect(response).to render_template('students/update') }
      end

      context 'PUT account_settings' do
        before do
          put :update,
            profile_id: user_student.id,
            id: user_student.student.id,
            user: user_params,
            format: :js
        end

        it { expect(assigns(:profile).username).to eql(user_params[:username]) }
        it { expect(assigns(:profile).first_name).to eql(user_params[:first_name]) }
        it { expect(assigns(:profile).last_name).to eql(user_params[:last_name]) }
        it { expect(assigns(:profile).email).to eql(user_params[:email]) }
        it { expect(assigns(:profile).username).to eql(user_params[:username]) }
        it { expect(assigns(:profile).phone_number).to eql(user_params[:phone_number]) }
        it { expect(assigns(:profile).target_degree).to eql(user_params[:target_degree]) }
        it { expect(assigns(:profile).service_ids).to eql(user_params[:service_ids]) }
        it { expect(assigns(:profile).timezone).to eql(user_params[:timezone]) }
        it { expect(assigns(:profile).student.target_degree).to eql(user_params[:target_degree]) }
        it { expect(assigns(:profile).student.target_countries).to eql(user_params[:target_countries]) }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('students/update') }
      end
    end
  end
end
