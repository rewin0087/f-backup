# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe AppointmentsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule) }
  let!(:messenger) { create(:messenger, task: tasks[0]) }
  let!(:mentor_messenger_participant) { create(:messenger_participant, user: user_mentor, messenger: messenger, user_type: 'Mentor') }
  let!(:student_messenger_participant) { create(:messenger_participant, user: user_student, messenger: messenger, user_type: 'Student') }
  let(:appointment_params) {
    {
      timezone: 'Asia/Manila',
      schedule_id: schedule.id,
      mentor_id: user_mentor.mentor.id,
      task_id: tasks[1].id
    }
  }

  let(:schedule_params) {
    {
      start_date: DateTimeHelper.date_format(DateTime.now),
      start_time: '01:00 AM',
      end_date: DateTimeHelper.date_format(DateTime.now),
      end_time: '02:00 AM',
      timezone: 'Asia/Manila',
      student_id: user_student.student.id
    }
  }

  describe 'when user is not logged in' do
    context 'GET index' do
      context 'when response is a json' do
        before do
          get :index, profile_id: user_mentor.id, format: :json
        end

        it { expect(response).to have_http_status 401 }
        it { expect(JSON.parse(response.body)['error']).to eql('You need to sign in or sign up before continuing.') }
      end

      context 'when response is a html' do
        before do
          get :index, profile_id: user_mentor.id
        end

        it { expect(response).to redirect_to new_user_session_path }
      end
    end

    context 'GET new' do
      before do
        get :new, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST create' do
      before do
        post :create, profile_id: user_mentor.id, appointment: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT request_schedule' do
      before do
        put :request_schedule, profile_id: user_mentor.id, id: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET show' do
      before do
        get :show, profile_id: user_mentor.id, id: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT decline' do
      before do
        put :decline, profile_id: user_student.id, id: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT confirm' do
      before do
        put :confirm, profile_id: user_mentor.id, id: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT cancel' do
      before do
        put :cancel, profile_id: user_mentor.id, id: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as a student' do
      before { sign_in user_student }

      context 'GET index' do
        context 'as json request' do
          it { expect{ get :index, profile_id: user_student.id, format: :json }.to raise_error(ActionController::RoutingError) }
        end

        context 'as html request' do
          it { expect{ get :index, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
        end
      end

      context 'GET new' do
        it { expect { get :new, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST create' do
        it { expect { post :create, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST request_schedule' do
        it { expect { post :request_schedule, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET show' do
        before do
          xhr :get, :show, profile_id: user_student.id, id: appointment.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(response).to render_template('appointments/show') }
      end

      context 'PUT decline' do
        before do
          xhr :put, :decline, profile_id: user_student.id, id: appointment.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(flash[:error]).to be_nil }
        it { expect(response).to render_template('appointments/decline') }
      end

      context 'PUT cancel' do
        before do
          xhr :put, :cancel, profile_id: user_student.id, id: appointment.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(flash[:error]).to be_nil }
        it { expect(response).to render_template('appointments/cancel') }
      end
    end

    context 'as a mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        context 'as json request' do
          before do
            get :index, profile_id: user_mentor.id, timezone: 'Asia/Manila', format: :json
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:appointments)).to be_a(Array) }
          it { expect(assigns(:appointments).size).to eql(1) }
          it { expect(!!JSON.parse(response.body)).to eql(true) }
        end

        context 'as html request' do
          before do
            get :index, profile_id: user_mentor.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:appointments)).to be_nil }
          it { expect(response).to render_template('appointments/index') }
        end
      end

      context 'GET new' do
        context 'as existing schedule' do
          before do
            xhr :get,
              :new,
              profile_id: user_mentor.id,
              status: Schedule.available,
              schedule: schedule.id,
              mentor: user_mentor.mentor.id,
              task: tasks[1].id,
              format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:schedule)).to eql(schedule) }
          it { expect(assigns(:appointment).id).to be_nil }
          it { expect(assigns(:appointment).schedule).to eql(schedule) }
          it { expect(assigns(:appointment).task).to eql(tasks[1]) }
          it { expect(assigns(:appointment).mentor).to eql(user_mentor.mentor) }
          it { expect(assigns(:appointment).timezone).to eql(user_mentor.timezone) }
          it { expect(response).to render_template('appointments/new') }
        end

        context 'as request schedule' do
          let(:date) { DateTimeHelper.date_format(DateTime.now) }
          let(:time) { '00:00 AM' }

          before do
            xhr :get,
              :new,
              profile_id: user_mentor.id,
              request_schedule: 1,
              student: user_student.student.id,
              mentor: user_mentor.mentor.id,
              task: tasks[1].id,
              date: date,
              time: time,
              format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:schedule)).to be_a(Schedule) }
          it { expect(assigns(:schedule).id).to be_nil }
          it { expect(assigns(:schedule).student).to eql(user_student.student) }
          it { expect(assigns(:schedule).timezone).to eql(user_mentor.timezone) }
          it { expect(assigns(:schedule).start_date).to eql(date.to_datetime) }
          it { expect(assigns(:schedule).end_date).to eql(date.to_datetime) }
          it { expect(assigns(:schedule).start_time).to eql('2000-01-01 00:00:00 UTC'.to_time.utc) }
          it { expect(assigns(:appointment).id).to be_nil }
          it { expect(assigns(:appointment).schedule).to be_a(Schedule) }
          it { expect(assigns(:appointment).task).to eql(tasks[1]) }
          it { expect(assigns(:appointment).mentor).to eql(user_mentor.mentor) }
          it { expect(assigns(:appointment).timezone).to eql(user_mentor.timezone) }
          it { expect(response).to render_template('appointments/new') }
        end

        context 'as default show appointment details' do
          before do
            xhr :get,
              :new,
              profile_id: user_mentor.id,
              schedule: appointment.id,
              mentor: user_mentor.mentor.id,
              task: tasks[1].id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:appointment)).to eql(appointment) }
          it { expect(assigns(:schedule)).to eql(schedule) }
          it { expect(response).to render_template('appointments/new') }
        end
      end

      context 'POST create' do
        context 'when success' do
          before do
            post :create,
              profile_id: user_mentor.id,
              appointment: appointment_params,
              format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:appointment).timezone).to eql('Asia/Manila') }
          it { expect(assigns(:appointment).schedule_id).to eql(schedule.id) }
          it { expect(assigns(:appointment).schedule).to eql(schedule) }
          it { expect(assigns(:appointment).mentor).to eql(user_mentor.mentor) }
          it { expect(assigns(:appointment).mentor_id).to eql(user_mentor.mentor.id) }
          it { expect(assigns(:appointment).task).to eql(tasks[1]) }
          it { expect(assigns(:appointment).task_id).to eql(tasks[1].id) }
          it { expect(assigns(:appointment).status).to eql(Appointment.pending) }
          it { expect(response).to render_template('appointments/create') }
        end

        context 'when error' do
          context 'when no schedule pass' do
            before do
              appointment_params[:schedule_id] = ''
              post :create,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Schedule can\'t be blank') }
            it { expect(flash[:error]).to eql('Schedule can\'t be blank') }
            it { expect(response).to render_template('appointments/create') }
          end

          context 'when no mentor pass' do
            before do
              appointment_params[:mentor_id] = ''
              post :create,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Mentor can\'t be blank') }
            it { expect(flash[:error]).to eql('Mentor can\'t be blank') }
            it { expect(response).to render_template('appointments/create') }
          end

          context 'when no task pass' do
            before do
              appointment_params[:task_id] = ''
              post :create,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Task can\'t be blank') }
            it { expect(flash[:error]).to eql('Task can\'t be blank') }
            it { expect(response).to render_template('appointments/create') }
          end
        end
      end

      context 'POST request_schedule' do
        before { appointment_params[:schedule] = '' }

        context 'when success' do
          before do
            post :request_schedule,
              profile_id: user_mentor.id,
              appointment: appointment_params,
              schedule: schedule_params,
              format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:appointment).timezone).to eql('Asia/Manila') }
          it { expect(assigns(:appointment).mentor).to eql(user_mentor.mentor) }
          it { expect(assigns(:appointment).mentor_id).to eql(user_mentor.mentor.id) }
          it { expect(assigns(:appointment).task).to eql(tasks[1]) }
          it { expect(assigns(:appointment).task_id).to eql(tasks[1].id) }
          it { expect(assigns(:appointment).status).to eql(Appointment.pending) }
          it { expect(assigns(:appointment).schedule.timezone).to eql('Asia/Manila') }
          it { expect(assigns(:appointment).schedule.formatted_start_time).to eql('01:00 AM') }
          it { expect(assigns(:appointment).schedule.formatted_end_time).to eql('02:00 AM') }
          it { expect(assigns(:appointment).schedule.start_datetime).to eql("#{schedule_params[:start_date]} 01:00 AM") }
          it { expect(assigns(:appointment).schedule.end_datetime).to eql("#{schedule_params[:end_date]} 02:00 AM") }
          it { expect(assigns(:appointment).schedule).to be_a(Schedule) }
          it { expect(response).to render_template('appointments/request_schedule') }
        end

        context 'when error' do
          context 'when no appointment mentor' do
            before do
              appointment_params[:mentor_id] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Mentor can\'t be blank') }
            it { expect(flash[:error]).to eql('Mentor can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no appointment task' do
            before do
              appointment_params[:task_id] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Task can\'t be blank') }
            it { expect(flash[:error]).to eql('Task can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule timezone' do
            before do
              schedule_params[:timezone] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Timezone can\'t be blank') }
            it { expect(flash[:error]).to eql('Timezone can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule start_date' do
            before do
              schedule_params[:start_date] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Start date can\'t be blank') }
            it { expect(flash[:error]).to eql('Start date can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule start_time' do
            before do
              schedule_params[:start_time] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Start time can\'t be blank') }
            it { expect(flash[:error]).to eql('Start time can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule end_date' do
            before do
              schedule_params[:end_date] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('End date can\'t be blank') }
            it { expect(flash[:error]).to eql('End date can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule end_time' do
            before do
              schedule_params[:end_time] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('End time can\'t be blank') }
            it { expect(flash[:error]).to eql('End time can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end

          context 'when no schedule student' do
            before do
              schedule_params[:student_id] = ''
              post :request_schedule,
                profile_id: user_mentor.id,
                appointment: appointment_params,
                schedule: schedule_params,
                format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:appointment).errors.any?).to eql(true) }
            it { expect(assigns(:appointment).errors.size).to eql(1) }
            it { expect(assigns(:appointment).errors.full_messages.first).to eql('Student can\'t be blank') }
            it { expect(flash[:error]).to eql('Student can\'t be blank') }
            it { expect(response).to render_template('appointments/request_schedule') }
          end
        end
      end

      context 'GET show' do
        before do
          xhr :get, :show, profile_id: user_mentor.id, id: appointment.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(response).to render_template('appointments/show') }
      end

      context 'PUT decline' do

        it { expect { put :decline, profile_id: user_mentor.id, id: appointment.id, format: :js }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT cancel' do
        before do
          xhr :put, :cancel, profile_id: user_mentor.id, id: appointment.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(flash[:error]).to be_nil }
        it { expect(response).to render_template('appointments/cancel') }
      end
    end
  end
end
