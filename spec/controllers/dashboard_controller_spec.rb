require 'rails_helper'

RSpec.describe DashboardController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:essay_review) { create(:essay_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:mentor_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_mentor.mentor) }
  let!(:student_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_student.student) }
  let!(:admin_user) { create(:admin_user) }

  describe 'when user is not logged in' do
    before do
      get :index, profile_id: user_mentor.id
    end

    it { expect(response).to redirect_to new_user_session_path }
  end

  describe 'when user is logged in' do
    context 'as admin' do
      before { sign_in admin_user }

      context 'GET index' do
        context 'user dashboard' do
          before do
            get :index, profile_id: user_mentor.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('dashboard/index') }
          it { expect(assigns(:students)).not_to be_nil }
          it { expect(assigns(:students)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:students).to_a).to be_a(Array) }
          it { expect(assigns(:students).size).to eql(1) }
          it { expect(assigns(:mentors)).to be_nil }
        end

        context 'dashboard only' do
          before do
            get :index
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('landing/index') }
          it { expect(response).to render_template(layout: 'branding') }
          it { expect(assigns(:top_mentors)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:top_mentors).to_a).to be_a(Array) }
          it { expect(assigns(:top_mentors).size).to eql(1) }
          it { expect(assigns(:mentors)).to be_nil }
          it { expect(assigns(:students)).to be_nil }
        end
      end
    end

    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        before do
          get :index, profile_id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('dashboard/index') }
        it { expect(assigns(:students)).not_to be_nil }
        it { expect(assigns(:students)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:students).to_a).to be_a(Array) }
        it { expect(assigns(:students).size).to eql(1) }
        it { expect(assigns(:mentors)).to be_nil }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET index' do
        before do
          get :index, profile_id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('dashboard/index') }
        it { expect(assigns(:mentors)).not_to be_nil }
        it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:mentors).to_a).to be_a(Array) }
        it { expect(assigns(:mentors).size).to eql(1) }
        it { expect(assigns(:students)).to be_nil }
      end
    end
  end
end
