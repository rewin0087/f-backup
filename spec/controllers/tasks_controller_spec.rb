# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  description             :text(65535)
#  requested_file          :string(255)
#  revised_file            :string(255)
#  status                  :string(255)      default("CREATED")
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  task_type               :string(255)      default("PAYED")
#  service_id              :integer
#  order_id                :integer
#  mentor_id               :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

require 'rails_helper'
include FileUploadHelper
RSpec.describe TasksController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let(:file) { tempfile_access(fixture_file_upload('image.png', 'image/png')) }

  describe 'when user is not logged in' do
    context 'GET show' do
      before do
        get :show, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT update' do
      before do
        put :update, profile_id: user_student.id, id: tasks[1].id, task: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET upload_request_file' do
      before do
        get :upload_request_file, profile_id: user_student.id, id: tasks[1].id, task: {}, modal: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT upload_request_file' do
      before do
        put :upload_request_file, profile_id: user_student.id, id: tasks[1].id, task: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET upload_revised_file' do
      before do
        get :upload_revised_file, profile_id: user_student.id, id: tasks[1].id, task: {}, modal: 1
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT upload_revised_file' do
      before do
        put :upload_revised_file, profile_id: user_student.id, id: tasks[1].id, task: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT in_progress' do
      before do
        put :in_progress, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT done' do
      before do
        put :done, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT completed' do
      before do
        put :completed, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT instruction' do
      before do
        put :instruction, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT comment' do
      before do
        put :comment, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET appointment' do
      before do
        get :appointment, profile_id: user_student.id, id: tasks[1].id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET show' do
        before do
          get :show, profile_id: user_mentor.id, id: tasks[1].id
        end

        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).not_to be_nil }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/show') }
      end

      context 'PUT update' do
        it { expect { put :update, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT upload_request_file' do
        it { expect { put :upload_request_file, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET upload_request_file' do
        it { expect { get :upload_request_file, profile_id: user_mentor.id, id: tasks[1].id, modal: 1 }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT upload_revised_file' do
        before do
          put :upload_revised_file, profile_id: user_mentor.id, id: tasks[1].id, task: { revised_file: file }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/upload_revised_file') }
        it { expect(assigns(:task).revised_file).not_to be_nil }
        it { expect(assigns(:task).revised_file.file.original_filename.split('____').first).to eql(file.original_filename) }
      end

      context 'GET upload_revised_file' do
        before do
          xhr :get, :upload_revised_file, profile_id: user_mentor.id, id: tasks[1].id, modal: 1, format: :js
        end

        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task)).not_to be_nil }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/upload_revised_file') }
      end

      context 'PUT in_progress' do
        before do
          xhr :put, :in_progress, profile_id: user_mentor.id, id: tasks[1].id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/in_progress') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).status).to eql(Task.in_progress) }
      end

      context 'PUT done' do
        before do
          xhr :put, :done, profile_id: user_mentor.id, id: tasks[1].id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/done') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).status).to eql(Task.done) }
      end

      context 'PUT completed' do
        it { expect { put :completed, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT instruction' do
        it { expect { put :instruction, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT comment' do
        before do
          xhr :put, :comment, profile_id: user_mentor.id, id: tasks[1].id, format: :js, task: { additional_comments: 'Test Additional Comments' }
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/comment') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).additional_comments).to eql('Test Additional Comments') }
      end

      context 'GET appointment' do
        before do
          get :appointment, profile_id: user_mentor.id, id: tasks[1].id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/appointment') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET show' do
        before do
          get :show, profile_id: user_mentor.id, id: tasks[1].id
        end

        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).not_to be_nil }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/show') }
      end

      context 'PUT update' do
        before do
          xhr :put, :update, profile_id: user_student.id, id: tasks[1].id, task: { description: 'Test Description' }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/update') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task)).not_to be_nil }
        it { expect(assigns(:task).description).to eql('Test Description') }
      end

      context 'GET upload_request_file' do
        before do
          xhr :get, :upload_request_file, profile_id: user_student.id, id: tasks[1].id, modal: 1, format: :js
        end

        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task)).not_to be_nil }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/upload_request_file') }
      end

      context 'PUT upload_request_file' do
        before do
          put :upload_request_file, profile_id: user_mentor.id, id: tasks[1].id, task: { requested_file: file }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/upload_request_file') }
        it { expect(assigns(:task).requested_file).not_to be_nil }
        it { expect(assigns(:task).requested_file.file.original_filename.split('____').first).to eql(file.original_filename) }
      end

      context 'PUT upload_revised_file' do
        it { expect { put :upload_revised_file, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET upload_revised_file' do
        it { expect { get :upload_revised_file, profile_id: user_mentor.id, id: tasks[1].id, modal: 1 }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT in_progress' do
        before do
          xhr :put, :in_progress, profile_id: user_student.id, id: tasks[1].id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/in_progress') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).status).to eql(Task.in_progress) }
      end

      context 'PUT done' do
        it { expect { put :done, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT completed' do
        before do
          xhr :put, :completed, profile_id: user_student.id, id: tasks[1].id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/completed') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).status).to eql(Task.completed) }
      end

      context 'PUT instruction' do
        before do
          xhr :put, :instruction, profile_id: user_student.id, id: tasks[1].id, format: :js, task: { additional_instructions: 'Test Additional Instructions' }
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('tasks/instruction') }
        it { expect(assigns(:task).id).to eql(tasks[1].id) }
        it { expect(assigns(:task)).to be_a(Task) }
        it { expect(assigns(:task).additional_instructions).to eql('Test Additional Instructions') }
      end

      context 'PUT comment' do
        it { expect { put :comment, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET appointment' do
        it { expect { get :appointment, profile_id: user_mentor.id, id: tasks[1].id }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
