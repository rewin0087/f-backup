require 'rails_helper'
include FileUploadHelper
RSpec.describe ProfileController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let(:file) { tempfile_access(fixture_file_upload('image.png', 'image/png')) }

  describe 'when user is not logged in' do
    context 'GET show' do
      before do
        get :show, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET edit' do
      before do
        get :edit, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT update' do
      before do
        put :update, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT primary_photo' do
      before do
        put :primary_photo, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET students' do
      before do
        get :students, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET mentors' do
      before do
        get :mentors, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET account_settings' do
      before do
        get :account_settings, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST private_message' do
      before do
        post :private_message, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET private_message' do
      before do
        get :private_message, id: user_student
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET show' do
        before do
          get :show, id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/show') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
      end

      context 'GET edit' do
        before do
          get :edit, id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/edit') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
      end

      context 'PUT update' do
      end

      context 'PUT primary_photo' do
        before do
          xhr :put, :primary_photo, id: user_mentor.id, user: { primary_photo: file }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/primary_photo') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
        it { expect(assigns(:profile).primary_photo.file.original_filename.split('.').last).to eql('png') }
      end

      context 'GET students' do
        before do
          get :students, id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/students') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
        it { expect(assigns(:students)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:students).to_a).to be_a(Array) }
        it { expect(assigns(:students).size).to eql(0) }
      end

      context 'GET mentors' do
        it { expect{ get :mentors, id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET account_settings' do
        before do
          get :account_settings, id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/account_settings') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
      end

      context 'POST private_message' do
        before do
          xhr :post, :private_message, id: user_mentor.id, to: user_student.id, private_message: 'Test Private Message', format: :js
        end

        it { expect { xhr :post, :private_message, id: user_mentor.id, to: user_student.id, private_message: 'Test Private Message', format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/private_message') }
      end

      context 'GET private_message' do
        before do
          xhr :get, :private_message, id: user_mentor.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/private_message') }
        it { expect(assigns(:profile).id).to eql(user_mentor.id) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET show' do
        before do
          get :show, id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/show') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
      end

      context 'GET edit' do
        before do
          get :edit, id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/edit') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
      end

      context 'PUT update' do
      end

      context 'PUT primary_photo' do
        before do
          xhr :put, :primary_photo, id: user_student.id, user: { primary_photo: file }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/primary_photo') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
        it { expect(assigns(:profile).primary_photo.file.original_filename.split('.').last).to eql('png') }
      end

      context 'GET students' do
        it { expect{ get :students, id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET mentors' do
        before do
          get :mentors, id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/mentors') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
        it { expect(assigns(:mentors)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:mentors).to_a).to be_a(Array) }
        it { expect(assigns(:mentors).size).to eql(0) }
      end

      context 'GET account_settings' do
        before do
          get :account_settings, id: user_student.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/account_settings') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
      end

      context 'POST private_message' do
        before do
          xhr :post, :private_message, id: user_student.id, to: user_mentor.id, private_message: 'Test Private Message', format: :js
        end

        it { expect { xhr :post, :private_message, id: user_student.id, to: user_mentor.id, private_message: 'Test Private Message', format: :js }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1) }
        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/private_message') }
      end

      context 'GET private_message' do
        before do
          xhr :get, :private_message, id: user_student.id, recipient: user_student.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('profile/private_message') }
        it { expect(assigns(:profile).id).to eql(user_student.id) }
      end
    end
  end
end
