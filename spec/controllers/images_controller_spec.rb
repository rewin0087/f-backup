# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  status         :string(255)
#  imageable_id   :integer
#  imageable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe ImagesController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:student_image) { create(:image, status: Image.public, imageable: user_student) }
  let!(:mentor_image) { create(:image, status: Image.public, imageable: user_mentor) }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST create' do
      before do
        post :create, profile_id: user_mentor.id, images: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'DELETE destroy' do
      before do
        delete :destroy, profile_id: user_mentor.id, id: mentor_image.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT set_as_primary_photo' do
      before do
        put :set_as_primary_photo, profile_id: user_mentor.id, id: mentor_image.id
      end
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      include_examples 'should have access to all images resource' do
        let(:user) { user_mentor }
        let(:image) { mentor_image }
      end
    end

    context 'as student' do
      include_examples 'should have access to all images resource' do
        let(:user) { user_student }
        let(:image) { student_image }
      end
    end
  end
end
