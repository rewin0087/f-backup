# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  message         :string(255)
#  status          :string(255)      default("PENDING")
#  user_id         :integer
#  notifiable_type :string(255)
#  notifiable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  url             :string(255)
#

require 'rails_helper'

RSpec.describe NotificationsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service]) }
  let!(:user_student) { create(:user_student, services: [consult_service]) }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index, profile_id: user_student.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        context 'visit other user notifications' do
          it { expect { get :index, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
        end

        context 'visit own notifications' do
          before do
            get :index, profile_id: user_mentor.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('notifications/index') }
          it { expect(assigns(:notifications)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:notifications)).to be_empty }
          it { expect(assigns(:notifications).to_a).to be_a(Array) }
          it { expect(assigns(:profile)).to eql(user_mentor) }
        end
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET index' do
        context 'visit other user notifications' do
          it { expect { get :index, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
        end

        context 'visit own notifications' do
          before do
            get :index, profile_id: user_student.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('notifications/index') }
          it { expect(assigns(:notifications)).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:notifications)).to be_empty }
          it { expect(assigns(:notifications).to_a).to be_a(Array) }
          it { expect(assigns(:profile)).to eql(user_student) }
        end
      end
    end
  end
end
