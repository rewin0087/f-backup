require 'rails_helper'

RSpec.describe MessengerController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.approved, task: tasks[0], schedule: schedule) }
  let!(:admin_user) { create(:admin_user) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:messenger) {
    users = [appointment.mentor.user, appointment.student.user]
    TaskMessenger.new(appointment.task, users).create
  }

  let(:message_params) {
    {
      messenger_id: messenger.id
    }
  }

  describe 'when user is not logged in' do
    context 'GET video_call' do
      before do
        get :video_call, profile_id: user_mentor.id, messenger_id: messenger.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT decline_video_call' do
      before do
        put :decline_video_call, profile_id: user_mentor.id, messenger_id: messenger.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST message' do
      before do
        post :message, profile_id: user_mentor.id, messenger_id: messenger.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET show' do
      before do
        get :show, profile_id: user_mentor.id, id: messenger.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET video_call' do
        before do
          get :video_call, profile_id: user_mentor.id, messenger_id: messenger.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/video_call') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end

      context 'PUT decline_video_call' do
        before do
          xhr :put, :decline_video_call, profile_id: user_mentor.id, messenger_id: messenger.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/decline_video_call') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end

      context 'POST message' do
        let(:chat_message) { FFaker::Lorem.paragraph }
        before do
          message_params[:text] = chat_message
          message_params[:user_id] = user_mentor.id
          xhr :post, :message, profile_id: user_mentor.id, messenger_id: messenger.id, format: :js, message: message_params
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/message') }
        it { expect(assigns(:message).errors.empty?).to eql(true) }
        it { expect(assigns(:message).text).to eql(chat_message) }
        it { expect(assigns(:message).user.id).to eql(user_mentor.id) }
        it { expect(assigns(:message).messenger.messages.size).to eql(1) }
      end

      context 'GET show' do
        before do
          get :show, profile_id: user_mentor.id, id: messenger.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/show') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET video_call' do
        before do
          get :video_call, profile_id: user_student.id, messenger_id: messenger.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/video_call') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end

      context 'PUT decline_video_call' do
        before do
          xhr :put, :decline_video_call, profile_id: user_student.id, messenger_id: messenger.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/decline_video_call') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end

      context 'POST message' do
        let(:chat_message) { FFaker::Lorem.paragraph }
        before do
          message_params[:text] = chat_message
          message_params[:user_id] = user_student.id
          xhr :post, :message, profile_id: user_student.id, messenger_id: messenger.id, format: :js, message: message_params
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/message') }
        it { expect(assigns(:message).errors.empty?).to eql(true) }
        it { expect(assigns(:message).text).to eql(chat_message) }
        it { expect(assigns(:message).user.id).to eql(user_student.id) }
        it { expect(assigns(:message).messenger.messages.size).to eql(1) }
      end

      context 'GET show' do
        before do
          get :show, profile_id: user_student.id, id: messenger.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/show') }
        it { expect(assigns(:messenger)).to be_a(Messenger) }
        it { expect(assigns(:messenger)).not_to be_nil }
        it { expect(assigns(:messenger).id).to eql(messenger.id) }
        it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
      end
    end

    context 'as admin' do
      before { sign_in admin_user }

      context 'GET video_call' do
        it { expect { get :video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { get :video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT decline_video_call' do
        it { expect { put :decline_video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { put :decline_video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST message' do
        let(:chat_message) { FFaker::Lorem.paragraph }
        before do
          message_params[:text] = chat_message
          message_params[:user_id] = user_student.id
          xhr :post, :message, profile_id: user_student.id, messenger_id: messenger.id, format: :js, message: message_params
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('messenger/message') }
        it { expect(assigns(:message).errors.empty?).to eql(true) }
        it { expect(assigns(:message).text).to eql(chat_message) }
        it { expect(assigns(:message).user.id).to eql(user_student.id) }
        it { expect(assigns(:message).messenger.messages.size).to eql(1) }
      end

      context 'GET show' do
        context 'mentor profile' do
          before do
            get :show, profile_id: user_mentor.id, id: messenger.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('messenger/show') }
          it { expect(assigns(:messenger)).to be_a(Messenger) }
          it { expect(assigns(:messenger)).not_to be_nil }
          it { expect(assigns(:messenger).id).to eql(messenger.id) }
          it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
        end

        context 'student profile' do
          before do
            get :show, profile_id: user_student.id, id: messenger.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('messenger/show') }
          it { expect(assigns(:messenger)).to be_a(Messenger) }
          it { expect(assigns(:messenger)).not_to be_nil }
          it { expect(assigns(:messenger).id).to eql(messenger.id) }
          it { expect(assigns(:messenger).messages).to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:messenger).messages.to_a).to be_a(Array) }
        end
      end
    end

    context 'as other student but not inclded in the messenger' do
      before { sign_in user_student2 }

      context 'GET video_call' do
        it { expect { get :video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { get :video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT decline_video_call' do
        it { expect { put :decline_video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { put :decline_video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST message' do
        it { expect { post :message, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { post :message, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET show' do
        it { expect { get :show, profile_id: user_mentor.id, id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { get :show, profile_id: user_student.id, id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end
    end

    context 'as other mentor but not inclded in the messenger' do
      before { sign_in user_mentor2 }

      context 'GET video_call' do
        it { expect { get :video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { get :video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT decline_video_call' do
        it { expect { put :decline_video_call, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { put :decline_video_call, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST message' do
        it { expect { post :message, profile_id: user_mentor.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { post :message, profile_id: user_student.id, messenger_id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET show' do
        it { expect { get :show, profile_id: user_mentor.id, id: messenger.id }.to raise_error(ActionController::RoutingError) }
        it { expect { get :show, profile_id: user_student.id, id: messenger.id }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
