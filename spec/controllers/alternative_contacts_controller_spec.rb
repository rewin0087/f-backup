# == Schema Information
#
# Table name: alternative_contacts
#
#  id            :integer          not null, primary key
#  account_type  :string(255)
#  account_value :string(255)
#  contact_id    :integer
#  contact_type  :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

require 'rails_helper'

RSpec.describe AlternativeContactsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:student_alternative_contact) { create(:alternative_contact, contact: user_student.student) }
  let!(:mentor_alternative_contact) { create(:alternative_contact, contact: user_mentor.mentor) }
  let(:alternative_contact_params) { { account_type: 'skype', account_value: 'testuser' } }

  describe 'when user is not logged in' do
    context 'POST create' do
      before do
        post :create, profile_id: user_student.id, alternative_contact: { account_type: 'skype', account_value: 'testuser' }
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT update' do
      before do
        put :update, profile_id: user_student.id, id: student_alternative_contact.id, alternative_contact: alternative_contact_params
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'DELETE destroy' do
      before do
        delete :destroy, profile_id: user_student.id, id: student_alternative_contact.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe "POST create" do
    context 'when user is logged in' do
      context 'as student' do
        before { sign_in user_student }

        context 'success' do
          context 'when account type and account value are empty' do
            before do
              post :create, profile_id: user_student.id, alternative_contact: { account_type: '', account_value: '' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to be_empty }
            it { expect(assigns(:contact).account_value).to be_empty }
            it { expect(response).to render_template('alternative_contacts/create') }
          end

          context 'when account type and account value are present' do
            before do
              post :create, profile_id: user_student.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('skype') }
            it { expect(assigns(:contact).account_value).to eql('testuser') }
            it { expect(response).to render_template('alternative_contacts/create') }
          end
        end

        context 'error' do
          context 'when contact_type is present and contact_value is empty' do
            before do
              alternative_contact_params[:account_value] = ''
              post :create, profile_id: user_student.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).to be_nil }
            it { expect(assigns(:contact).errors.empty?).to eql(false) }
            it { expect(assigns(:contact).errors.size).to eql(1) }
            it { expect(assigns(:contact).errors.full_messages.first).to eql('Account value can\'t be blank') }
            it { expect(response).to render_template('alternative_contacts/create') }
          end
        end
      end

      context 'as mentor' do
        before { sign_in user_mentor }

        context 'success' do
          context 'when account type and account value are empty' do
            before do
              post :create, profile_id: user_mentor.id, alternative_contact: { account_type: '', account_value: '' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to be_empty }
            it { expect(assigns(:contact).account_value).to be_empty }
            it { expect(response).to render_template('alternative_contacts/create') }
          end

          context 'when account type and account value are present' do
            before do
              post :create, profile_id: user_mentor.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('skype') }
            it { expect(assigns(:contact).account_value).to eql('testuser') }
            it { expect(response).to render_template('alternative_contacts/create') }
          end
        end

        context 'error' do
          context 'when contact_type is present and contact_value is empty' do
            before do
              alternative_contact_params[:account_value] = ''
              post :create, profile_id: user_mentor.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).to be_nil }
            it { expect(assigns(:contact).errors.empty?).to eql(false) }
            it { expect(assigns(:contact).errors.size).to eql(1) }
            it { expect(assigns(:contact).errors.full_messages.first).to eql('Account value can\'t be blank') }
            it { expect(response).to render_template('alternative_contacts/create') }
          end
        end
      end
    end
  end

  describe "PUT update" do
    context 'when user is logged in' do
      context 'as student' do
        before { sign_in user_student }

        context 'success' do
          context 'when account type is set' do
            before do
              put :update, profile_id: user_student.id, id: student_alternative_contact.id, alternative_contact: { account_type: 'wechat' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('wechat') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end

          context 'when account type is set' do
            before do
              put :update, profile_id: user_student.id, id: student_alternative_contact.id, alternative_contact: { account_value: 'testuser2' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_value).to eql('testuser2') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end

          context 'when account type and account value are set' do
            before do
              put :update, profile_id: user_student.id, id: student_alternative_contact.id, alternative_contact: { account_type: 'wechat', account_value: 'testuser2' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('wechat') }
            it { expect(assigns(:contact).account_value).to eql('testuser2') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end
        end

        context 'error' do
          context 'when contact_type is present and contact_value is empty' do
            before do
              alternative_contact_params[:account_value] = ''
              put :update, profile_id: user_student.id, id: student_alternative_contact.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).errors.empty?).to eql(false) }
            it { expect(assigns(:contact).errors.size).to eql(1) }
            it { expect(assigns(:contact).errors.full_messages.first).to eql('Account value can\'t be blank') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end
        end
      end

      context 'as mentor' do
        before { sign_in user_mentor }

        context 'success' do
          context 'when account type is set' do
            before do
              put :update, profile_id: user_mentor.id, id: mentor_alternative_contact.id, alternative_contact: { account_type: 'wechat' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('wechat') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end

          context 'when account type is set' do
            before do
              put :update, profile_id: user_mentor.id, id: mentor_alternative_contact.id, alternative_contact: { account_value: 'testuser2' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_value).to eql('testuser2') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end

          context 'when account type and account value are set' do
            before do
              put :update, profile_id: user_mentor.id, id: mentor_alternative_contact.id, alternative_contact: { account_type: 'wechat', account_value: 'testuser2' }, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).account_type).to eql('wechat') }
            it { expect(assigns(:contact).account_value).to eql('testuser2') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end
        end

        context 'error' do
          context 'when contact_type is present and contact_value is empty' do
            before do
              alternative_contact_params[:account_value] = ''
              put :update, profile_id: user_mentor.id, id: mentor_alternative_contact.id, alternative_contact: alternative_contact_params, format: :js
            end

            it { expect(response).to have_http_status 200 }
            it { expect(assigns(:contact)).to be_a(AlternativeContact) }
            it { expect(assigns(:contact).id).not_to be_nil }
            it { expect(assigns(:contact).errors.empty?).to eql(false) }
            it { expect(assigns(:contact).errors.size).to eql(1) }
            it { expect(assigns(:contact).errors.full_messages.first).to eql('Account value can\'t be blank') }
            it { expect(response).to render_template('alternative_contacts/update') }
          end
        end
      end
    end
  end

  describe "DELETE destroy" do
    context 'when user is logged in' do
      context 'as student' do
        before { sign_in user_student }

        before do
          delete :destroy, profile_id: user_student.id, id: student_alternative_contact.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:contact)).to be_a(AlternativeContact) }
        it { expect(response).to render_template('alternative_contacts/destroy') }
      end

      context 'as mentor' do
        before { sign_in user_mentor }

        context 'success' do
          before do
            delete :destroy, profile_id: user_mentor.id, id: mentor_alternative_contact.id, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(assigns(:contact)).to be_a(AlternativeContact) }
          it { expect(response).to render_template('alternative_contacts/destroy') }
        end
      end
    end
  end
end
