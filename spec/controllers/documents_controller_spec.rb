# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text(65535)
#  original_filename :text(65535)
#  filename          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'
include FileUploadHelper
RSpec.describe DocumentsController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:mentor_document) { create(:document, documentable: user_mentor.mentor) }
  let(:files) { tempfile_access(fixture_file_upload('image.png', 'image/png')) }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST create' do
      before do
        post :create, profile_id: user_mentor.id, document: {}
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'DELETE destroy' do
      before do
        delete :destroy, profile_id: user_mentor.id, id: mentor_document.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        before do
          get :index, profile_id: user_mentor.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('documents/index') }
        it { expect(assigns(:documents)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:documents).to_a).to be_a(Array) }
        it { expect(assigns(:documents).size).to eql(1) }
        it { expect(assigns(:profile)).to eql(user_mentor) }
      end

      context 'POST create' do
        before do
          xhr :post, :create, profile_id: user_mentor.id, files: files, document: { title: 'Test Document title', description: 'Test Document description' }, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('documents/create') }
        it { expect(assigns(:documents)).to be_a(ActiveRecord::Relation) }
        it { expect(assigns(:documents).size).to eql(2) }
      end

      context 'DELETE destroy' do
        before do
          xhr :delete, :destroy, profile_id: user_mentor.id, id: mentor_document.id
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('documents/destroy') }
        it { expect(assigns(:document).destroyed?).to eql(true) }
        it { expect(Document.exists?(mentor_document.id)).to eql(false) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET index' do
        it { expect { get :index, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST create' do
        it { expect { post :create, profile_id: user_student.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'DELETE destroy' do
        it { expect { delete :destroy, profile_id: user_student.id, id: mentor_document }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
