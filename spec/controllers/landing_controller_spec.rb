require 'rails_helper'

RSpec.describe LandingController, :type => :controller do
  render_views
  describe 'GET index' do
    context 'when not user logged in' do
      context 'when theres a list of mentors' do
        let!(:consult_service) { create(:consult_service) }
        let!(:user_mentor) { create(:user_mentor, services: [consult_service]) }
        let!(:mentor_school) { create(:school, scholastic: user_mentor.mentor) }

        before { get :index }

        it { expect(response).to render_template('landing/index') }
        it { expect(response).to render_template(layout: 'branding') }
        it { expect(response).to have_http_status :ok }
        it { expect(response.body).to include('preview-banner') }
        it { expect(response.body).to include('preview-mentors') }
        it { expect(response.body).to include('preview-branding') }
        it { expect(response.body).to include('preview-traveler') }
        it { expect(response.body).to include('preview-map') }
        it { expect(response.body).to include('preview-the-mentors') }
        it { expect(response.body).to include('preview-mobile') }
        it { expect(response.body).to include('preview-quick-link') }
        it { expect(response.body).to include('FIND A FRIEND ABROAD') }
        it { expect(response.body).to include('branding.png') }
        it { expect(response.body).to include('SUPPORT') }
        it { expect(response.body).to include('CAREERS') }
        it { expect(response.body).to include('FAQ') }
        it { expect(response.body).to include('CONTACT US') }
        it { expect(response.body).to include('Home') }
        it { expect(response.body).to include('Services') }
        it { expect(response.body).to include('Mentors') }
        it { expect(response.body).to include('Contact') }
        it { expect(response.body).to include('LOGIN') }
        it { expect(response.body).to include('twitter.png') }
        it { expect(response.body).to include('facebook.png') }
        it { expect(response.body).to include('login-modal') }
        it { expect(response.body).to include('new_user') }
        it { expect(response.body).to include('/login') }
        it { expect(response.body).to include('Not Signed up?') }
        it { expect(response.body).to include('Username') }
      end

      context 'when there are no list of mentors available' do
        before { get :index }

        it { expect(response).to render_template('landing/index') }
        it { expect(response).to render_template(layout: 'branding') }
        it { expect(response).to have_http_status :ok }
        it { expect(response.body).to include('preview-banner') }
        it { expect(response.body).not_to include('preview-mentors') }
        it { expect(response.body).to include('preview-branding') }
        it { expect(response.body).to include('preview-traveler') }
        it { expect(response.body).to include('preview-map') }
        it { expect(response.body).to include('preview-the-mentors') }
        it { expect(response.body).to include('preview-mobile') }
        it { expect(response.body).to include('preview-quick-link') }
        it { expect(response.body).to include('FIND A FRIEND ABROAD') }
        it { expect(response.body).to include('branding.png') }
        it { expect(response.body).to include('SUPPORT') }
        it { expect(response.body).to include('CAREERS') }
        it { expect(response.body).to include('FAQ') }
        it { expect(response.body).to include('CONTACT US') }
        it { expect(response.body).to include('Home') }
        it { expect(response.body).to include('Services') }
        it { expect(response.body).to include('Mentors') }
        it { expect(response.body).to include('Contact') }
        it { expect(response.body).to include('LOGIN') }
        it { expect(response.body).to include('twitter.png') }
        it { expect(response.body).to include('facebook.png') }
        it { expect(response.body).to include('login-modal') }
        it { expect(response.body).to include('new_user') }
        it { expect(response.body).to include('/login') }
        it { expect(response.body).to include('Not Signed up?') }
        it { expect(response.body).to include('Username') }
      end
    end

    context 'when user is logged in' do
      let!(:consult_service) { create(:consult_service) }
      let!(:basic_review) { create(:basic_review) }
      let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
      let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
      let!(:mentor_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_mentor.mentor) }
      let!(:student_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_student.student) }

      context 'as mentor' do
        before { sign_in user_mentor }
        it { expect { get :index }.to raise_error(ActionController::RoutingError) }
      end

      context 'as student' do
        before { sign_in user_student }
        it { expect { get :index }.to raise_error(ActionController::RoutingError) }
      end
    end
  end
end
