# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SchedulesController, :type => :controller do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule) }
  let(:date) { DateTimeHelper.date_format(DateTime.now) }
  let(:schedule_params) {
    {
      start_date: DateTimeHelper.date_format(DateTime.now),
      start_time: '01:00 AM',
      end_date: DateTimeHelper.date_format(DateTime.now),
      end_time: '02:00 AM',
      timezone: 'Asia/Manila',
      student_id: user_student.student.id
    }
  }

  describe 'when user is not logged in' do
    context 'GET index' do
      before do
        get :index, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET checker' do
      before do
        get :checker, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET new' do
      before do
        get :new, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'POST create' do
      before do
        post :create, profile_id: user_mentor.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'GET edit' do
      before do
        get :edit, profile_id: user_mentor.id, id: schedule.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT update' do
      before do
        put :update, profile_id: user_mentor.id, id: schedule.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'DELETE destroy' do
      before do
        delete :destroy, profile_id: user_mentor.id, id: schedule.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end

    context 'PUT close' do
      before do
        put :close, profile_id: user_mentor.id, id: schedule.id
      end

      it { expect(response).to redirect_to new_user_session_path }
    end
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      before { sign_in user_mentor }

      context 'GET index' do
        it { expect { get :index, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET checker' do
        it { expect { get :checker, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET new' do
        it { expect { get :new, profile_id: user_mentor.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'POST create' do
        it { expect { post :create, profile_id: user_mentor.id, schedule: {} }.to raise_error(ActionController::RoutingError) }
      end

      context 'GET edit' do
        before do
          xhr :get, :edit, profile_id: user_mentor.id, id: schedule.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('schedules/edit') }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).id).to eql(schedule.id) }
        it { expect(assigns(:schedule)).to eql(schedule) }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(assigns(:appointment).id).to eql(appointment.id) }
        it { expect(assigns(:appointment)).to eql(appointment) }
      end

      context 'PUT update' do
        it { expect { put :update, profile_id: user_mentor.id, id: schedule.id, schedule: {} }.to raise_error(ActionController::RoutingError) }
      end

      context 'DELETE destroy' do
        it { expect { delete :destroy, profile_id: user_mentor.id, id: schedule.id }.to raise_error(ActionController::RoutingError) }
      end

      context 'PUT close' do
        it { expect { put :close, profile_id: user_mentor.id, id: schedule.id }.to raise_error(ActionController::RoutingError) }
      end
    end

    context 'as student' do
      before { sign_in user_student }

      context 'GET index' do
        context 'request as json' do
          before do
            get :index, profile_id: user_student.id, format: :json
          end

          it { expect(response).to have_http_status 200 }
          it { expect(!!JSON.parse(response.body)).to eql(true) }
          it { expect(response).not_to render_template('schedules/index') }
          it { expect(assigns(:schedules)).not_to be_a(ActiveRecord::Relation) }
          it { expect(assigns(:schedules)).to be_a(Array) }
          it { expect(assigns(:schedules).size).to eql(1) }
        end

        context 'request as html' do
          before do
            get :index, profile_id: user_student.id
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('schedules/index') }
          it { expect(assigns(:schedules)).to be_nil }
        end
      end

      context 'GET checker' do
        context 'when enough schedules' do
          before do
            20.times{|i| create(:schedule, student: user_student.student) }
            xhr :get, :checker, profile_id: user_student.id, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('schedules/checker') }
          it { expect(assigns(:count)).to eql(21) }
        end

        context 'when dont have enough schedules' do
          before do
            xhr :get, :checker, profile_id: user_student.id, format: :js
          end

          it { expect(response).to have_http_status 200 }
          it { expect(response).to render_template('schedules/checker') }
          it { expect(assigns(:count)).to eql(1) }
          it { expect(flash[:error]).not_to be_empty }
          it { expect(flash[:error]).to eql('Please add more slots on your schedules into your calendar, 10 slots in a 2 week frame. You need to add 9 more slots into your schedules calendar.') }
        end
      end

      context 'GET new' do
        before do
          xhr :get, :new, profile_id: user_student.id, format: :js, date: date
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('schedules/new') }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).id).to be_nil }
        it { expect(assigns(:schedule).start_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).end_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).start_time).to eql('2000-01-01 00:00:00 UTC'.to_time.utc) }
        it { expect(assigns(:schedule).end_time).to eql('2000-01-01 00:00:00 UTC'.to_time.utc) }
      end

      context 'POST create' do
        before do
          post :create, profile_id: user_student.id, schedule: schedule_params, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).id).not_to be_nil }
        it { expect(assigns(:schedule).student).to eql(user_student.student) }
        it { expect(assigns(:schedule).timezone).to eql(user_mentor.timezone) }
        it { expect(assigns(:schedule).start_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).end_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).start_time).to eql('2000-01-01 01:00:00 UTC'.to_time.utc) }
        it { expect(response).to render_template('schedules/create') }
      end

      context 'GET edit' do
        before do
          xhr :get, :edit, profile_id: user_student.id, id: schedule.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('schedules/edit') }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).id).to eql(schedule.id) }
        it { expect(assigns(:schedule)).to eql(schedule) }
        it { expect(assigns(:appointment)).to be_a(Appointment) }
        it { expect(assigns(:appointment).id).to eql(appointment.id) }
        it { expect(assigns(:appointment)).to eql(appointment) }
      end

      context 'PUT update' do
        before do
          xhr :put, :update, profile_id: user_student.id, id: schedule.id, schedule: schedule_params, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).id).not_to be_nil }
        it { expect(assigns(:schedule).student).to eql(user_student.student) }
        it { expect(assigns(:schedule).timezone).to eql(user_mentor.timezone) }
        it { expect(assigns(:schedule).start_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).end_date).to eql(date.to_datetime) }
        it { expect(assigns(:schedule).start_time).to eql('2000-01-01 01:00:00 UTC'.to_time.utc) }
        it { expect(response).to render_template('schedules/update') }
      end

      context 'DELETE destroy' do
        before do
          delete :destroy, profile_id: user_student.id, id: schedule.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('schedules/destroy') }
        it { expect(assigns(:schedule).destroyed?).to eql(true) }
        it { expect(Schedule.exists?(schedule.id)).to eql(false) }
      end

      context 'PUT close' do
        before do
          schedule.status = Schedule.occupied
          schedule.save
          appointment.status = Appointment.approved
          appointment.save
          xhr :put, :close, profile_id: user_student.id, id: schedule.id, format: :js
        end

        it { expect(response).to have_http_status 200 }
        it { expect(response).to render_template('schedules/close') }
        it { expect(assigns(:schedule)).to be_a(Schedule) }
        it { expect(assigns(:schedule).status).to eql(Schedule.closed) }
        it { expect(assigns(:schedule).errors.empty?).to eql(true) }
      end
    end
  end
end
