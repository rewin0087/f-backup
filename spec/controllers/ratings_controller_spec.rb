# == Schema Information
#
# Table name: ratings
#
#  id               :integer          not null, primary key
#  speed            :float(24)
#  knowledgeability :float(24)
#  overall_rating   :float(24)
#  average          :float(24)
#  feedbacks        :string(255)
#  task_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'rails_helper'

RSpec.describe RatingsController, :type => :controller do

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
