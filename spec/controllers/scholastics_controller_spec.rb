require 'rails_helper'

RSpec.describe ScholasticsController, :type => :controller do
  describe 'when user is not logged in' do
    include_examples 'should have access to all scholastics resource'
  end

  describe 'when user is logged in' do
    context 'as mentor' do
      include_examples 'should have access to all scholastics resource'
    end

    context 'as student' do
      include_examples 'should have access to all scholastics resource'
    end
  end
end
