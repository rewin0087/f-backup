# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  university      :text(65535)
#  major           :text(65535)
#  scholastic_id   :integer
#  scholastic_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :school do
    university { FFaker::Education.school }
    major "MyText"
    scholastic nil
  end
end
