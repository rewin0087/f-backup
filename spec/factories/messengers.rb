# == Schema Information
#
# Table name: messengers
#
#  id         :integer          not null, primary key
#  task_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  slug       :string(255)
#

FactoryGirl.define do
  factory :messenger do
    task nil
  end

end
