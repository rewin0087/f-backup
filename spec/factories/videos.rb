# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  url            :text(65535)
#  source         :text(65535)
#  videoable_id   :integer
#  videoable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :video do
    url 'https://www.youtube.com/watch?v=ac3HkriqdGQ'
    source 'https://www.youtube.com/embed/ac3HkriqdGQ'
    videoable nil
  end

end
