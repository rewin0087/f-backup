# == Schema Information
#
# Table name: alternative_contacts
#
#  id            :integer          not null, primary key
#  account_type  :string(255)
#  account_value :string(255)
#  contact_id    :integer
#  contact_type  :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

FactoryGirl.define do
  factory :alternative_contact do
    account_type 'Skype'
    account_value { FFaker::Internet.user_name }
    contact nil
  end
end
