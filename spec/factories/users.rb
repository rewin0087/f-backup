# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  full_name              :string(255)
#  username               :string(255)
#  role                   :string(255)
#  about_me               :text(65535)
#  additional_information :text(65535)
#  primary_photo          :string(255)
#  current_country        :string(255)
#  other_degree           :string(255)
#  phone_number           :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  timezone               :string(255)
#  slug                   :string(255)
#

FactoryGirl.define do
  factory :user do
    password { Devise.friendly_token.first(13).squeeze }
    username { "#{SecureRandom.hex(1)}#{FFaker::Internet.user_name}" }
    email { "#{SecureRandom.hex(1)}#{FFaker::Internet.email}" }
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    phone_number { FFaker::PhoneNumber.phone_number }
    current_country { FFaker::Address.country }
    primary_photo { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'image.png')) }
    timezone 'Asia/Manila'
    about_me { FFaker::Lorem.paragraph }
    additional_information { FFaker::Lorem.paragraph }
    reply_speed { '1' }
    terms_of_use { '1' }
    role ''
    other_degree { FFaker::Education.degree }

    transient do
      services nil
    end

    after(:create) do |user|
      if user.student?
        wallet = user.wallet
        wallet.amount = 1000
        wallet.save
      end
    end

    factory :user_mentor do
      role { User.mentor }
      before(:create) do |user, eval|
        eval.services.each do |service|
          user.user_services << build(:user_service, service: service)
        end

        build(:mentor, user: user)
      end
    end

    factory :user_student do
      role { User.student }
      before(:create) do |user, eval|
        eval.services.each do |service|
          user.user_services << build(:user_service, service: service)
        end

        build(:student, user: user)
      end
    end
  end
end
