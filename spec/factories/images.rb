# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  status         :string(255)
#  imageable_id   :integer
#  imageable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :image do
    source { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'image.png')) }
    status ''
    imageable nil
  end
end
