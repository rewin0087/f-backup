# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

FactoryGirl.define do
  factory :mentor do
    university_email { FFaker::Internet.email }
    home_town { FFaker::Address.country }
    degree { [FFaker::Education.degree] }
    undergraduate_universities { [FFaker::Education.school] }
    undergraduate_universities_early_acceptance 1
    graduate_universities { [FFaker::Education.school] }
    graduate_universities_early_acceptance 1
    basic_consultation_charge 5.0
    basic_consultation_charge_currency '$'
    work { FFaker::Lorem.word }
    mentor_student 1
    reason { FFaker::Lorem.paragraph }
    free_tip { FFaker::Lorem.paragraph }
    essay_services { ['0','1'] }
    experience { FFaker::Lorem.paragraph }
    awards_and_honors { FFaker::Lorem.paragraph }
    display_ratings 0
    display_reviews 0
    user
  end
end
