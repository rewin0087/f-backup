# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :appointment do
    timezone 'Asia/Manila'
    status { Appointment.pending }
    mentor
    task

    transient do
      schedule nil
    end

    before(:create) do |appointment, eval|
      if eval.schedule
        appointment.schedule = eval.schedule
      else
        schedule = create(:schedule, status: Schedule.occupied, student: appointment.task.student)
        appointment.schedule = schedule
      end
    end
  end

end
