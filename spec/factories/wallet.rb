FactoryGirl.define do
  factory :wallet do
    amount 1000
    pending_balance 0
    user
  end
end