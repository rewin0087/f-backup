# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text(65535)
#  original_filename :text(65535)
#  filename          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryGirl.define do
  factory :document do
    title { FFaker::Lorem.word }
    description { FFaker::Lorem.paragraph }
    original_filename { FFaker::Lorem.word }
    filename { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'image.png')) }
    documentable nil
  end

end
