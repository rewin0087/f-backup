# == Schema Information
#
# Table name: students
#
#  id                  :integer          not null, primary key
#  target_countries    :text(65535)
#  target_degree       :text(65535)
#  looking_for         :text(65535)
#  questions_to_mentor :text(65535)
#  remarks             :text(65535)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :student do
    target_countries { [FFaker::Address.country] }
    target_degree { [FFaker::Education.degree] }
    looking_for { FFaker::Lorem.paragraph }
    questions_to_mentor { FFaker::Lorem.paragraph }
    remarks { FFaker::Lorem.paragraph }
    user
  end
end
