# == Schema Information
#
# Table name: bookmarks
#
#  id         :integer          not null, primary key
#  student_id :integer
#  mentor_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :bookmark do
    student nil
    mentor nil
  end

end
