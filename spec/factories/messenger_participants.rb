# == Schema Information
#
# Table name: messenger_participants
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  messenger_id :integer
#  user_type    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :messenger_participant do
    user nil
    user_type "MyString"
    messenger nil
  end

end
