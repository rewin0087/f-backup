FactoryGirl.define do
  factory :base_transaction, class: 'Transaction' do
    amount 10
    wallet
    order
  end

  factory :paypal_transaction, parent: :base_transaction do
    transaction_type Transaction::TYPE[:debit]
    payment_gateway Transaction::PAYMENT_GATEWAY[:paypal]
    status Transaction::PAYPAL_STATE['approved']
  end

  factory :credit_card_transaction, parent: :base_transaction do
    transaction_type Transaction::TYPE[:debit]
    payment_gateway Transaction::PAYMENT_GATEWAY[:cc]
    status Transaction::PAYPAL_STATE['completed']
  end
end