# == Schema Information
#
# Table name: user_services
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  service_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_service do
    user nil
    association :service, factory: :service
  end
end
