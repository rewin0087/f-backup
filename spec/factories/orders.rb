# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

FactoryGirl.define do
  factory :order do
    student
    mentor
    status { Order.pending }
    price { 0}
    actual_price { 0 }
    currency_symbol ''
    currency ''

    transient do
      tasks nil
    end

    before(:create) do |order, eval|
      order.tasks = eval.tasks
      order.compute_price_and_set_currency
    end
  end
end
