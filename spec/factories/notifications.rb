# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  message         :string(255)
#  status          :string(255)      default("PENDING")
#  user_id         :integer
#  notifiable_type :string(255)
#  notifiable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  url             :string(255)
#

FactoryGirl.define do
  factory :notification do
    message 'Test Notification'
    notifiable nil
    user nil
  end
end
