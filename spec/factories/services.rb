# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text(65535)
#  price           :decimal(10, )
#  currency        :string(255)
#  currency_symbol :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_type    :string(255)
#

FactoryGirl.define do
  factory :service do
    title { FFaker::Lorem.phrase }
    description { FFaker::Lorem.sentences }
    price 5
    currency 'USD'
    currency_symbol '$'
    service_type nil

    factory :consult_service do
      title 'Consult'
      service_type { Service.appointment_service }
    end

    factory :essay_review do
      title 'Essay Review'
      service_type { Service.appointment_service }
    end

    factory :basic_review do
      title 'Basic Review'
      service_type { Service.non_appointment_service }
    end
  end
end
