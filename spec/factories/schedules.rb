# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :schedule do
    start_date { rand(4.hour).seconds.ago - 7.hours }
    start_time { rand(7.hour).seconds.ago - 2.hour }
    end_date { rand(1.hour).seconds.ago - 5.hours }
    end_time { rand(1.hour).seconds.ago - 1.hour }
    timezone 'Asia/Manila'
    status { Schedule.available }
    student
  end

end
