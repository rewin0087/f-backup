# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  description             :text(65535)
#  requested_file          :string(255)
#  revised_file            :string(255)
#  status                  :string(255)      default("CREATED")
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  task_type               :string(255)      default("PAYED")
#  service_id              :integer
#  order_id                :integer
#  mentor_id               :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

FactoryGirl.define do
  factory :task do
    description { FFaker::Lorem.paragraph }
    requested_file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'image.png')) }
    revised_file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'image.png')) }
    status { Task.created }
    additional_instructions { FFaker::Lorem.paragraph }
    additional_comments { FFaker::Lorem.paragraph }
    service nil
    order nil
    mentor nil
    appointment nil
    price 0
    actual_price 0

    after(:build) do |t|
      t.price = t.service.price
      t.actual_price = t.service.price
      t.currency = t.service.currency
      t.currency_symbol = t.service.currency_symbol
    end
  end
end
