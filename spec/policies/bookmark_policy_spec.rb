require 'rails_helper'

describe BookmarkPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:admin_user) { create(:admin_user) }
  let!(:bookmark) { create(:bookmark, student: user_student.student, mentor: user_mentor.mentor) }
  subject { described_class }

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).not_to permit(admin_user, bookmark) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, bookmark) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, bookmark) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).not_to permit(admin_user, bookmark) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, bookmark) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, bookmark) }
    end
  end
end
