require 'rails_helper'

describe RecommendationPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).not_to permit(admin_user, :recommendation) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :recommendation) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :recommendation) }
    end
  end
end