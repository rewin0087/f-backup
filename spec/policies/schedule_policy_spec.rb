require 'rails_helper'

describe SchedulePolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :index? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :schedule) }
    end
  end

  permissions :checker? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :schedule) }
    end
  end

  permissions :new? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :schedule) }
    end
  end

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :schedule) }
    end
  end

  permissions :edit? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, schedule) }
    end
  end

  permissions :update? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, schedule) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, schedule) }
    end
  end

  permissions :close? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, schedule) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, schedule) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, schedule) }
    end
  end
end
