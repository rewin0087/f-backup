require 'rails_helper'

describe ImagePolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:admin_user) { create(:admin_user) }
  let!(:student_image) { create(:image, imageable: user_student) }
  let!(:mentor_image) { create(:image, imageable: user_mentor) }

  subject { described_class }

  permissions :index? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :image) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :image) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :image) }
    end
  end

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :image) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :image) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :image) }
    end
  end

  permissions :set_as_primary_photo? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_image) }
      it { expect(subject).to permit(admin_user, mentor_image) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, student_image) }
      it { expect(subject).to permit(user_mentor, mentor_image) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, student_image) }
      it { expect(subject).not_to permit(user_student, mentor_image) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_image) }
      it { expect(subject).to permit(admin_user, mentor_image) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, student_image) }
      it { expect(subject).to permit(user_mentor, mentor_image) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, student_image) }
      it { expect(subject).not_to permit(user_student, mentor_image) }
    end
  end
end
