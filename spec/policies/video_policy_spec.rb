require 'rails_helper'

describe VideoPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:video) { create(:video, videoable: user_mentor.mentor) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :video) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :video) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, video) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, video) }
    end
  end
end
