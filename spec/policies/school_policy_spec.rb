require 'rails_helper'

describe SchoolPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:mentor_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_mentor.mentor) }
  let!(:student_school) { create(:school, major: 'COMPUTER SCIENCE', scholastic: user_student.student) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :school) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :school) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :school) }
    end
  end

  permissions :update? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_school) }
      it { expect(subject).to permit(admin_user, student_school) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, mentor_school) }
      it { expect(subject).to permit(user_student, student_school) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, mentor_school) }
      it { expect(subject).not_to permit(user_mentor, student_school) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_school) }
      it { expect(subject).to permit(admin_user, student_school) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, mentor_school) }
      it { expect(subject).to permit(user_student, student_school) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, mentor_school) }
      it { expect(subject).not_to permit(user_mentor, student_school) }
    end
  end
end
