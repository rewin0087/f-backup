require 'rails_helper'

describe AlternativeContactPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:student_alternative_contact) { create(:alternative_contact, contact: user_student.student) }
  let!(:mentor_alternative_contact) { create(:alternative_contact, contact: user_mentor.mentor) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_alternative_contact) }
      it { expect(subject).to permit(admin_user, mentor_alternative_contact) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, student_alternative_contact) }
      it { expect(subject).to permit(user_mentor, mentor_alternative_contact) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, student_alternative_contact) }
      it { expect(subject).to permit(user_student, mentor_alternative_contact) }
    end
  end

  permissions :update? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_alternative_contact) }
      it { expect(subject).to permit(admin_user, mentor_alternative_contact) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, student_alternative_contact) }
      it { expect(subject).to permit(user_mentor, mentor_alternative_contact) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, student_alternative_contact) }
      it { expect(subject).not_to permit(user_student, mentor_alternative_contact) }
    end

    context 'when user is student not included in appointment' do
      it { expect(subject).not_to permit(user_student2, student_alternative_contact) }
      it { expect(subject).not_to permit(user_student2, mentor_alternative_contact) }
    end

    context 'when user is mentor not included in appointment' do
      it { expect(subject).not_to permit(user_mentor2, student_alternative_contact) }
      it { expect(subject).not_to permit(user_mentor2, mentor_alternative_contact) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, student_alternative_contact) }
      it { expect(subject).to permit(admin_user, mentor_alternative_contact) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, student_alternative_contact) }
      it { expect(subject).to permit(user_mentor, mentor_alternative_contact) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, student_alternative_contact) }
      it { expect(subject).not_to permit(user_student, mentor_alternative_contact) }
    end

    context 'when user is student not included in appointment' do
      it { expect(subject).not_to permit(user_student2, student_alternative_contact) }
      it { expect(subject).not_to permit(user_student2, mentor_alternative_contact) }
    end

    context 'when user is mentor not included in appointment' do
      it { expect(subject).not_to permit(user_mentor2, student_alternative_contact) }
      it { expect(subject).not_to permit(user_mentor2, mentor_alternative_contact) }
    end
  end
end
