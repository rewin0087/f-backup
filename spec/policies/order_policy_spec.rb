require 'rails_helper'

describe OrderPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:admin_user) { create(:admin_user) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }

  subject { described_class }

  permissions :index? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :order) }
    end
  end

  permissions :new? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :order) }
    end
  end

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, :order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, :order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, :order) }
    end
  end

  permissions :show? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end


  permissions :instruction? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :comment? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :accept? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :confirm? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :in_progress? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :completed? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :decline? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :cancel? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end

  permissions :close? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, order) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, order) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, order) }
    end

    context 'when user is mentor but not included in the order' do
      it { expect(subject).not_to permit(user_mentor2, order) }
    end

    context 'when user is student but not included in the order' do
      it { expect(subject).not_to permit(user_student2, order) }
    end
  end
end
