require 'rails_helper'

describe DocumentPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:admin_user) { create(:admin_user) }
  let!(:document) { create(:document, documentable: user_mentor.mentor) }
  subject { described_class }


  permissions :index? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, document) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, document) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, document) }
    end
  end

  permissions :create? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, document) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, document) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, document) }
    end
  end

  permissions :destroy? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, document) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, document) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, document) }
    end
  end
end
