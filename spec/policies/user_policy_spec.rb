require 'rails_helper'

describe UserPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :show? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, user_student) }
      it { expect(subject).to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).to permit(user_student, user_mentor) }
      it { expect(subject).to permit(user_student, user_mentor2) }
    end
  end

  permissions :edit? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :update? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :primary_photo? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :students? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :mentors? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).not_to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :account_settings? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end

  permissions :private_message? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, user_student) }
      it { expect(subject).to permit(admin_user, user_student2) }
      it { expect(subject).to permit(admin_user, user_mentor) }
      it { expect(subject).to permit(admin_user, user_mentor2) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, user_student) }
      it { expect(subject).not_to permit(user_mentor, user_student2) }
      it { expect(subject).to permit(user_mentor, user_mentor) }
      it { expect(subject).not_to permit(user_mentor, user_mentor2) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, user_student) }
      it { expect(subject).not_to permit(user_student, user_student2) }
      it { expect(subject).not_to permit(user_student, user_mentor) }
      it { expect(subject).not_to permit(user_student, user_mentor2) }
    end
  end
end
