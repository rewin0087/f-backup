require 'rails_helper'

describe MessengerPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.approved, task: tasks[0], schedule: schedule) }
  let!(:admin_user) { create(:admin_user) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:messenger) {
    users = [appointment.mentor.user, appointment.student.user]
    TaskMessenger.new(appointment.task, users).create
  }

  subject { described_class }

  permissions :video_call? do
    context 'when user is admin' do
      it { expect(subject).not_to permit(admin_user, messenger) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, messenger) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, messenger) }
    end

    context 'when user is student but not included in the messenger' do
      it { expect(subject).not_to permit(user_student2, messenger) }
    end

    context 'when user is mentor but not included in the messenger' do
      it { expect(subject).not_to permit(user_mentor2, messenger) }
    end
  end

  permissions :decline_video_call? do
    context 'when user is admin' do
      it { expect(subject).not_to permit(admin_user, messenger) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, messenger) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, messenger) }
    end

    context 'when user is student but not included in the messenger' do
      it { expect(subject).not_to permit(user_student2, messenger) }
    end

    context 'when user is mentor but not included in the messenger' do
      it { expect(subject).not_to permit(user_mentor2, messenger) }
    end
  end

  permissions :show? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, messenger) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, messenger) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, messenger) }
    end

    context 'when user is student but not included in the messenger' do
      it { expect(subject).not_to permit(user_student2, messenger) }
    end

    context 'when user is mentor but not included in the messenger' do
      it { expect(subject).not_to permit(user_mentor2, messenger) }
    end
  end

  permissions :message? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, messenger) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, messenger) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, messenger) }
    end

    context 'when user is student but not included in the messenger' do
      it { expect(subject).not_to permit(user_student2, messenger) }
    end

    context 'when user is mentor but not included in the messenger' do
      it { expect(subject).not_to permit(user_mentor2, messenger) }
    end
  end
end