require 'rails_helper'

describe TaskPolicy do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_student2) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:user_mentor2) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let(:task) { tasks[0] }
  let!(:admin_user) { create(:admin_user) }
  subject { described_class }

  permissions :show? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :update? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :upload_request_file? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :upload_revised_file? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :in_progress? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :done? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :completed? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :instruction? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).not_to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :comment? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end

  permissions :appointment? do
    context 'when user is admin' do
      it { expect(subject).to permit(admin_user, task) }
    end

    context 'when user is mentor' do
      it { expect(subject).to permit(user_mentor, task) }
    end

    context 'when user is student' do
      it { expect(subject).not_to permit(user_student, task) }
    end

    context 'when user is other student' do
      it { expect(subject).not_to permit(user_student2, task) }
    end

    context 'when user is other mentor' do
      it { expect(subject).not_to permit(user_mentor2, task) }
    end
  end
end
