# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  url            :text(65535)
#  source         :text(65535)
#  videoable_id   :integer
#  videoable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe Video, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service]) }
  let!(:video) { create(:video, videoable: user_mentor.mentor) }

  context 'association' do
    it { should belong_to(:videoable) }
  end

  context 'validation' do
    it { should validate_presence_of(:url) }
    context 'url format' do
      it { should allow_value('https://www.youtube.com/watch?v=39UOnlRUWd0&list=PL_CE9sdULDgmJ3BUTB2cRFLVXBsbYVHRy&index=1').for(:url) }
      it { should_not allow_value('www.youtube.com').for(:url) }
      it { should_not allow_value('youtube').for(:url) }
    end
  end

  context 'methods' do
    context '#parse_url' do
      before do
        stub_request(:get, "https://www.youtube.com/watch?v=ac3HkriqdGQ").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => "", :headers => {})
        video.parse_url!
      end

      it { expect(video.url).to eql('https://www.youtube.com/watch?v=ac3HkriqdGQ') }
      it { expect(video.source).to eql('https://www.youtube.com/embed/ac3HkriqdGQ') }
      it { expect(video.videoable).to be_a(Mentor) }
    end
  end
end
