# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

require 'rails_helper'

RSpec.describe Order, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { build_list(:task, 1, mentor: user_mentor.mentor, service: consult_service) }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }

  context 'constant' do
    it { expect(Order::STATUSES_WITH_ACTION).to eql(%w(PENDING ACCEPTED CONFIRMED IN_PROGRESS COMPLETED)) }
    it { expect(Order::STATUSES).to eql(%w(DECLINED CANCELED CLOSED PENDING ACCEPTED CONFIRMED IN_PROGRESS COMPLETED)) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:PENDING) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:ACCEPTED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:CONFIRMED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:IN_PROGRESS) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:COMPLETED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:DECLINED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:CANCELED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:CLOSED) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:ADDITIONAL_INSTRUCTIONS) }
    it { expect(Order::NOTIFICATION_TEMPLATE).to have_key(:ADDITIONAL_COMMENTS) }
    it { expect(Order::NOTIFICATION_TEMPLATE[:PENDING]).to eql('You have a pending Order.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:ACCEPTED]).to eql('Your Order is accepted.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:CONFIRMED]).to eql('Order is confirmed.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:IN_PROGRESS]).to eql('Order is set to In Progress.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:COMPLETED]).to eql('Order is Completed.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:DECLINED]).to eql('You Order is declined.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:CANCELED]).to eql('Order is canceled.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:CLOSED]).to eql('Order is done and closed.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:ADDITIONAL_INSTRUCTIONS]).to eql('Your Student sent you an additional instruction regarding the order.') }
    it { expect(Order::NOTIFICATION_TEMPLATE[:ADDITIONAL_COMMENTS]).to eql('Your Mentor sent you an encouragement message regarding your order.') }
  end

  context 'association' do
    it { should belong_to(:student) }
    it { should belong_to(:mentor) }
    it { should have_many(:tasks) }
    it { should have_many(:notifications) }
    it { should have_many(:services).through(:tasks) }
  end

  context 'validation' do
    it { expect(order).to validate_presence_of(:student) }
    it { expect(order).to validate_presence_of(:mentor) }
    it { expect(order).to validate_presence_of(:status) }
    it { expect(order).to validate_presence_of(:actual_price) }
    it { expect(order).to validate_presence_of(:currency) }
    it { expect(order).to validate_presence_of(:currency_symbol) }
    it { expect(order).to validate_presence_of(:price) }
    it { expect(order).to validate_numericality_of(:actual_price) }
    it { expect(order).to validate_numericality_of(:price) }
    it { expect(order.service_tasks_presence).to be_nil }
    it { expect(order.sufficient_wallet?).to be_nil }
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :additional_instructions,
        :additional_comments,
        :student_id,
        :mentor_id,
        :status,
        :price,
        :actual_price,
        :currency,
        :currency_symbol
      ]
    }

    it { expect(Order.available_params).to eql(available_params) }
    it { expect(described_class).to respond_to(:pending) }
    it { expect(described_class).to respond_to(:accepted) }
    it { expect(described_class).to respond_to(:confirmed) }
    it { expect(described_class).to respond_to(:in_progress) }
    it { expect(described_class).to respond_to(:completed) }
    it { expect(described_class).to respond_to(:declined) }
    it { expect(described_class).to respond_to(:canceled) }
    it { expect(described_class).to respond_to(:closed) }
    it { expect(Order.pending).to eql('PENDING') }
    it { expect(Order.accepted).to eql('ACCEPTED') }
    it { expect(Order.confirmed).to eql('CONFIRMED') }
    it { expect(Order.in_progress).to eql('IN_PROGRESS') }
    it { expect(Order.completed).to eql('COMPLETED') }
    it { expect(Order.declined).to eql('DECLINED') }
    it { expect(Order.canceled).to eql('CANCELED') }
    it { expect(Order.closed).to eql('CLOSED') }
  end

  context 'methods' do
    context 'statuses' do
      it { expect(order).to respond_to(:pending?) }
      it { expect(order).to respond_to(:accepted?) }
      it { expect(order).to respond_to(:confirmed?) }
      it { expect(order).to respond_to(:in_progress?) }
      it { expect(order).to respond_to(:completed?) }
      it { expect(order).to respond_to(:declined?) }
      it { expect(order).to respond_to(:canceled?) }
      it { expect(order).to respond_to(:closed?) }
      it { expect(order.pending?).to be_in([true, false]) }
      it { expect(order.accepted?).to be_in([true, false]) }
      it { expect(order.confirmed?).to be_in([true, false]) }
      it { expect(order.in_progress?).to be_in([true, false]) }
      it { expect(order.completed?).to be_in([true, false]) }
      it { expect(order.declined?).to be_in([true, false]) }
      it { expect(order.canceled?).to be_in([true, false]) }
      it { expect(order.closed?).to be_in([true, false]) }
    end

    context '#accept!' do
      before { order.accept! }
      it { expect(order.status).to eql(Order.accepted) }
      it { expect(order.errors.empty?).to eql(true) }
    end

    context '#confirm!' do
      before { order.confirm! }
      it { expect(order.status).to eql(Order.confirmed) }
      it { expect(order.errors.empty?).to eql(true) }
    end

    context '#in_progress!' do
      context 'success' do
        before { order.in_progress! }
        it { expect(order.status).to eql(Order.in_progress) }
        it { expect(order.tasks.map{|t| t.requested_file.file}.include?(nil)).to eql(false) }
        it { expect(order.errors.empty?).to eql(true) }
      end

      context 'error' do
        before do
          order.tasks.first.remove_requested_file!
          order.in_progress!
        end

        it { expect(order.errors.empty?).to eql(false) }
        it { expect(order.errors.full_messages.first).to eql(I18n.t('models.order.errors.missed_requested_file')) }
        it { expect(I18n.t('models.order.errors.missed_requested_file')).to eql('All requested files must be uploaded for all the tasks before moving to IN PROGRESS.') }
      end
    end

    context '#completed!' do
      context 'success' do
        before do
          order.tasks.map {|t| create(:appointment, mentor: order.mentor, task: t, status: Appointment.completed) if t.service.service_type == Service.appointment_service }
          order.tasks.map(&:completed!)
          order.completed!
        end

        it { expect(order.status).to eql(Order.completed) }
        it { expect(order.errors.empty?).to eql(true) }
        it { expect(order.tasks.map(&:completed?).uniq.include?(false)).to eql(false) }
        it { expect(order.tasks.map{|t| t.revised_file.file}.include?(false)).to eql(false) }
      end

      context 'error' do
        before do
           order.tasks.first.remove_revised_file!
          order.completed!
        end
        it { expect(order.errors.empty?).to eql(false) }
        it { expect(order.errors.size).to eql(2) }
        it { expect(order.errors.full_messages.first).to eql(I18n.t('models.order.errors.missed_revised_file')) }
        it { expect(order.errors.full_messages.second).to eql(I18n.t('models.order.errors.incomplete_tasks.completed')) }
        it { expect(I18n.t('models.order.errors.incomplete_tasks.completed')).to eql('Please wait for the mentor to complete all the tasks before moving the order to COMPLETED. If there are some tasks that are already done, you should set those tasks to COMPLETED first before moving your order to COMPLETED.')}
        it { expect(I18n.t('models.order.errors.missed_revised_file')).to eql('Please wait for the mentor to finish all the tasks and wait for the mentor to upload all the revised files for all the tasks before moving to COMPLETED.') }
      end
    end

    context '#decline!' do
      before { order.decline! }
      it { expect(order.status).to eql(Order.declined) }
      it { expect(order.errors.empty?).to eql(true) }
      it { expect(order.errors.empty?).to eql(true) }
    end

    context '#cancel!' do
      before { order.cancel! }
      it { expect(order.status).to eql(Order.canceled) }
      it { expect(order.errors.empty?).to eql(true) }
      it { expect(order.tasks.first.status).to eql(Task.canceled) }
    end

    context '#close!' do
      context 'success' do
        before do
          order.tasks.map {|t| create(:appointment, mentor: order.mentor, task: t, status: Appointment.completed) if t.service.service_type == Service.appointment_service }
          order.tasks.map(&:completed!)
          order.close!
        end

        it { expect(order.status).to eql(Order.closed) }
        it { expect(order.errors.empty?).to eql(true) }
      end

      context 'error' do
        before { order.close! }

        it { expect(order.errors.empty?).to eql(false) }
        it { expect(order.errors.full_messages.first).to eql(I18n.t('models.order.errors.incomplete_tasks.close')) }
        it { expect(I18n.t('models.order.errors.incomplete_tasks.close')).to eql('Please wait for the mentor to complete all the tasks before closing the order.') }
      end
    end
  end
end
