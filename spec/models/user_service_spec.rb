# == Schema Information
#
# Table name: user_services
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  service_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe UserService, :type => :model do

  context 'association' do
    it { should belong_to(:user) }
    it { should belong_to(:service) }
  end

end
