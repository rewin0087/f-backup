# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  full_name              :string(255)
#  username               :string(255)
#  timezone               :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'rails_helper'

RSpec.describe AdminUser, :type => :model do
  let(:admin_user) { create(:admin_user) }

  context 'methods' do
    it { expect(admin_user).to respond_to(:administrator?) }
    it { expect(admin_user).to respond_to(:mentor?) }
    it { expect(admin_user).to respond_to(:student?) }
    it { expect(admin_user.administrator?).to eql(true) }
    it { expect(admin_user.mentor?).to eql(false) }
    it { expect(admin_user.student?).to eql(false) }
  end
end
