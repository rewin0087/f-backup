# == Schema Information
#
# Table name: students
#
#  id                  :integer          not null, primary key
#  target_countries    :text(65535)
#  target_degree       :text(65535)
#  looking_for         :text(65535)
#  questions_to_mentor :text(65535)
#  remarks             :text(65535)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

RSpec.describe Student, :type => :model do
  let!(:basic_review) { create(:basic_review) }
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review], current_country: 'Philippines') }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review], current_country: 'Philippines') }

  before do
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: "COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY", scholastic: user_mentor.mentor)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: 'COMPUTER AND INFORMATION SYSTEMS', scholastic: user_mentor.mentor)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: "COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY", scholastic: user_student.student)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: 'COMPUTER AND INFORMATION SYSTEMS', scholastic: user_student.student)
  end

  context 'association' do
    it { should belong_to(:user) }
    it { should have_many(:bookmarks) }
    it { should have_many(:schools) }
    it { should have_many(:alternative_contacts) }
    it { should have_many(:favorite_mentors).through(:bookmarks).source(:mentor) }
    it { should have_many(:schedules) }
    it { should have_many(:appointments).through(:schedules) }
    it { should have_many(:orders) }
    it { should have_many(:tasks).through(:orders) }
    it { should accept_nested_attributes_for :schools }
    it { should accept_nested_attributes_for :alternative_contacts }
  end

  context 'serializer' do
    it { should serialize(:target_countries).as(Array) }
    it { should serialize(:target_degree).as(Array) }
  end

  context 'class_methods' do
    context '.matches_for_mentor' do
      context 'when match found' do
        let(:keywords) {
        {
          majors: user_mentor.mentor.schools.map(&:major),
          services: [basic_review.id],
          degrees: user_mentor.mentor.degree,
          countries: [user_mentor.current_country],
          universities: user_mentor.mentor.schools.map(&:university),
          other_degree: user_mentor.other_degree
        }
      }

      it { expect(Student.matches_for_mentor(keywords).to_a).to be_a(Array) }
      it { expect(Student.matches_for_mentor(keywords).size).to eql(1) }
      end

      context 'when not match found' do
        let(:keywords) {
        {
          majors: ['COMPUTER SCIENCE'],
          services: [basic_review.id],
          degrees: ['LLM'],
          countries: ['Japan'],
          universities: ['University of Baguio'],
          other_degree: ''
        }
      }

      it { expect(Student.matches_for_mentor(keywords).to_a).to be_a(Array) }
      it { expect(Student.matches_for_mentor(keywords).size).to eql(0) }
      end
    end

    context '.available_params' do
      it { expect(Student.available_params).to eql([:questions_to_mentor, :remarks, :looking_for, target_countries: [], target_degree: []]) }
    end
  end
end
