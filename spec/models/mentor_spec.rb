# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

require 'rails_helper'

RSpec.describe Mentor, :type => :model do
  let!(:basic_review) { create(:basic_review) }
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review], current_country: 'Philippines') }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review], current_country: 'Philippines') }

  before do
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: "COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY", scholastic: user_mentor.mentor)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: 'COMPUTER AND INFORMATION SYSTEMS', scholastic: user_mentor.mentor)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: "COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY", scholastic: user_student.student)
    create(:school, university: user_mentor.mentor.graduate_universities.first, major: 'COMPUTER AND INFORMATION SYSTEMS', scholastic: user_student.student)
  end

  context 'constants' do
    let(:essay_services) {
      [
        'Basic essay review services (providing comments and editing errors) at $25 per 500 words (48 hr response time)',
        'Advanced essay review: logic review and two rounds of editing with feedback at 500 words for $75 with 30 minute video chat (96 hr response time for first contact; 48 hr response time for subsequent requests)',
        'No, I do not want to offer essay review services.'
      ]
    }

    let(:essay_services_with_index) {
      essay_services.map.with_index { |v,i| [v, i] }
    }

    it { expect(Mentor::ESSAY_SERVICES).to eql(essay_services) }
    it { expect(Mentor::ESSAY_SERVICES_WITH_INDEX).to eql(essay_services_with_index) }
  end

  context 'serializer' do
    it { should serialize(:degree).as(Array) }
    it { should serialize(:essay_services).as(Array) }
    it { should serialize(:undergraduate_universities).as(Array) }
    it { should serialize(:graduate_universities).as(Array) }
  end

  context 'associations' do
    it { should belong_to(:user) }
    it { should have_many(:bookmarks) }
    it { should have_many(:schools) }
    it { should have_many(:videos) }
    it { should have_many(:alternative_contacts) }
    it { should have_many(:documents) }
    it { should have_many(:tasks) }
    it { should have_many(:appointments) }
    it { should have_many(:schedules).through(:appointments) }
    it { should have_many(:orders) }
    it { should have_many(:ratings).through(:tasks) }
    it { should accept_nested_attributes_for :schools }
    it { should accept_nested_attributes_for :alternative_contacts }
  end

  context 'class_methods' do
    context '.available_params' do
      let(:available_params) {
        [
          :university_email,
          :home_town,
          :work,
          :mentor_student,
          :reason,
          :experience,
          :awards_and_honors,
          :display_reviews,
          :display_ratings,
          :free_tip,
          :terms_of_use,
          :reply_speed,
          :undergraduate_universities_early_acceptance,
          :graduate_universities_early_acceptance,
          :basic_consultation_charge,
          :essay_services => [],
          :degree => [],
          :undergraduate_universities => [],
          :graduate_universities => []
        ]
      }

      it { expect(Mentor.available_params).to eql(available_params) }
    end

    context '.top_natchers' do
      it { expect(Mentor.top_nachers.to_a).to be_a(Array) }
      it { expect(Mentor.top_nachers.size).to eql(1) }
    end

    context '.filter_search' do
      context 'when match found' do
        let(:keyword) { [user_mentor.first_name] }

        context 'with advance filter' do
          let(:advance_filter) {
            {
              services: [basic_review.id],
              majors: ['COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY', 'COMPUTER AND INFORMATION SYSTEMS'],
              country: 'Philippines',
              degrees: user_mentor.degree,
              ratings: ''
            }
          }

          it { expect(Mentor.filter_search(keyword, 1, advance_filter).to_a).to be_a(Array) }
          it { expect(Mentor.filter_search(keyword, 1, advance_filter).size).to eql(1) }
        end

        context 'without advance filter' do
          it { expect(Mentor.filter_search(keyword).to_a).to be_a(Array) }
          it { expect(Mentor.filter_search(keyword).size).to eql(1) }
        end
      end

      context 'when no match found' do
        let(:keyword) { ['test'] }

        context 'with advance filter' do
          let(:advance_filter) {
            {
              services: [basic_review.id],
              majors: ['COMPUTER ADMINISTRATION MANAGEMENT AND SECURITY', 'COMPUTER AND INFORMATION SYSTEMS'],
              country: 'Philippines',
              degrees: user_mentor.degree,
              ratings: '0'
            }
          }

          it { expect(Mentor.filter_search(keyword, 1, advance_filter).to_a).to be_a(Array) }
          it { expect(Mentor.filter_search(keyword, 1, advance_filter).size).to eql(0) }
        end

        context 'without advance filter' do
          it { expect(Mentor.filter_search(keyword).to_a).to be_a(Array) }
          it { expect(Mentor.filter_search(keyword).size).to eql(0) }
        end
      end
    end
  end

  context '.matches_for_student' do
    context 'when match found' do
      let(:keywords) {
        {
          majors: user_student.student.schools.map(&:major),
          services: [basic_review.id],
          degrees: user_student.student.target_degree,
          countries: [user_student.current_country],
          universities: user_student.student.schools.map(&:university),
          other_degree: user_student.other_degree
        }
      }

      it { expect(Mentor.matches_for_student(keywords).to_a).to be_a(Array) }
      it { expect(Mentor.matches_for_student(keywords).size).to eql(1) }
    end

    context 'when no match found' do
      let(:keywords) {
        {
          majors: ['COMPUTER SCIENCE'],
          services: [basic_review.id],
          degrees: ['LLM'],
          countries: ['Japan'],
          universities: ['University of Baguio'],
          other_degree: ''
        }
      }

      it { expect(Mentor.matches_for_student(keywords).to_a).to be_a(Array) }
      it { expect(Mentor.matches_for_student(keywords).size).to eql(0) }
    end
  end

  context 'methods' do
  end
end
