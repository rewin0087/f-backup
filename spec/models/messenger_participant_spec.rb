# == Schema Information
#
# Table name: messenger_participants
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  messenger_id :integer
#  user_type    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe MessengerParticipant, :type => :model do
  context 'association' do
    it { should belong_to(:user) }
    it { should belong_to(:messenger) }
  end

  context 'validation' do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:messenger) }
    it { should validate_presence_of(:user_type) }
  end
end
