# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  university      :text(65535)
#  major           :text(65535)
#  scholastic_id   :integer
#  scholastic_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe School, :type => :model do

  context 'association' do
    it { should belong_to(:scholastic) }
  end

  context 'validation' do
    before { allow(subject).to receive(:mentor?).and_return(true) }
    it { should validate_presence_of(:university) }
    it { should validate_presence_of(:major) }
  end

  context 'class_methods' do
    let(:available_params) { [:university, :major] }

    it { expect(School.available_params).to eql(available_params) }
  end
end
