# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  text         :text(65535)
#  messenger_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Message, :type => :model do
  context 'association' do
    it { should belong_to(:messenger) }
    it { should belong_to(:user) }
    it { should have_one(:task).through(:messenger) }
    it { should have_one(:appointment).through(:messenger) }
  end

  context 'validation' do
    it { should validate_presence_of(:text).with_message('message must not be empty.') }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:messenger_id) }
  end
end
