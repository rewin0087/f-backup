# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text(65535)
#  original_filename :text(65535)
#  filename          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe Document, :type => :model do
  context 'association' do
    it { should belong_to(:documentable) }
  end

  context 'scope' do
    it { expect(described_class).to respond_to(:recent_files) }
  end

  context '.available_params' do
    it { expect(Document.available_params).to eql([:title, :description]) }
  end
end
