# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  message         :string(255)
#  status          :string(255)      default("PENDING")
#  user_id         :integer
#  notifiable_type :string(255)
#  notifiable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  url             :string(255)
#

require 'rails_helper'

RSpec.describe Notification, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:user_student) { create(:user_student, services: [consult_service]) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:notification) { create(:notification, user: user_student, notifiable: schedule) }

  context 'constant' do
    it { expect(Notification::STATUSES).to eql(%w(PENDING SEEN)) }
  end

  context 'association' do
    it { should belong_to(:notifiable) }
    it { should belong_to(:user) }
  end

  context 'class_methods' do
    it { expect(described_class).to respond_to(:pending) }
    it { expect(described_class).to respond_to(:seen) }
    it { expect(Notification.pending).to eql('PENDING') }
    it { expect(Notification.seen).to eql('SEEN') }
  end

  context 'methods' do
    context '#seen!' do
      before { notification.seen! }
      it { expect(notification.status).to eql(Notification.seen) }
      it { expect(notification.user).to eql(user_student) }
      it { expect(notification.notifiable).to be_a(Schedule) }
      it { expect(notification.notifiable).to eql(schedule) }
    end
  end
end
