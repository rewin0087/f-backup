# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  description             :text(65535)
#  requested_file          :string(255)
#  revised_file            :string(255)
#  status                  :string(255)      default("CREATED")
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  task_type               :string(255)      default("PAYED")
#  service_id              :integer
#  order_id                :integer
#  mentor_id               :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

require 'rails_helper'

RSpec.describe Task, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let(:task) { tasks[0] }
  let!(:task_non_appointment) { tasks[1] }

  context 'constant' do
    it { expect(Task::STATUSES_WITH_ACTION).to eql(%w(ACCEPTED IN_PROGRESS DONE COMPLETED)) }
    it { expect(Task::STATUSES).to eql(%w(CREATED PENDING CANCELED CLOSED ACCEPTED IN_PROGRESS DONE COMPLETED)) }
    it { expect(Task::TYPES).to eql(%w(PAYED FREE DISCOUNTED)) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:IN_PROGRESS) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:BACK_TO_IN_PROGRESS) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:COMPLETED) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:DONE) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:CLOSED) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:ADDITIONAL_INSTRUCTIONS) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:ADDITIONAL_COMMENTS) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:REQUEST_FILE_UPLOADED) }
    it { expect(Task::NOTIFICATION_TEMPLATE).to have_key(:REVISED_FILE_UPLOADED) }
    it { expect(Task::NOTIFICATION_TEMPLATE[:IN_PROGRESS]).to eql('Your Mentor set your task to In Progress.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:BACK_TO_IN_PROGRESS]).to eql('Your Student reverted the task to In Progress.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:DONE]).to eql('Your Mentor is done with your task.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:COMPLETED]).to eql('Your Student set the task to Completed.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:CLOSED]).to eql('Your Student closed the task.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:ADDITIONAL_INSTRUCTIONS]).to eql('Your Student sent you an additional instruction regarding the task.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:ADDITIONAL_COMMENTS]).to eql('Your Mentor sent you an encouragement message regarding your task.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:REQUEST_FILE_UPLOADED]).to eql('Your Student is done uploading the request file to be mentored.') }
    it { expect(Task::NOTIFICATION_TEMPLATE[:REVISED_FILE_UPLOADED]).to eql('Your Mentor is done uploading the revised file.') }
  end

  context 'association' do
    it { should belong_to(:service) }
    it { should belong_to(:order) }
    it { should belong_to(:mentor) }
    it { should have_many(:notifications) }
    it { should have_one(:student).through(:order) }
    it { should have_one(:appointment) }
    it { should have_one(:messenger) }
    it { should have_one(:rating) }
  end

  context 'validation' do
    it { should validate_presence_of(:mentor) }
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:order) }
    it { should validate_presence_of(:service) }
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :description,
        :requested_file,
        :revised_file,
        :status,
        :additional_instructions,
        :additional_comments,
        :price,
        :actual_price,
        :currency,
        :currency_symbol,
        :task_type,
        :service_id,
        :order_id,
        :mentor_id
      ]
    }

    it { expect(Task.available_params).to eql(available_params) }
    it { expect(described_class).to respond_to(:accepted) }
    it { expect(described_class).to respond_to(:in_progress) }
    it { expect(described_class).to respond_to(:done) }
    it { expect(described_class).to respond_to(:completed) }
    it { expect(described_class).to respond_to(:created) }
    it { expect(described_class).to respond_to(:pending) }
    it { expect(described_class).to respond_to(:canceled) }
    it { expect(described_class).to respond_to(:closed) }
    it { expect(Task.accepted).to eq('ACCEPTED') }
    it { expect(Task.in_progress).to eq('IN_PROGRESS') }
    it { expect(Task.done).to eq('DONE') }
    it { expect(Task.completed).to eq('COMPLETED') }
    it { expect(Task.created).to eq('CREATED') }
    it { expect(Task.pending).to eq('PENDING') }
    it { expect(Task.canceled).to eq('CANCELED') }
    it { expect(Task.closed).to eq('CLOSED') }
    it { expect(Task.payed).to eq('PAYED') }
    it { expect(Task.free).to eq('FREE') }
    it { expect(Task.discounted).to eq('DISCOUNTED') }
  end

  context 'methods' do
    context 'statuses' do
      it { expect(task).to respond_to(:accepted?) }
      it { expect(task).to respond_to(:in_progress?) }
      it { expect(task).to respond_to(:done?) }
      it { expect(task).to respond_to(:completed?) }
      it { expect(task).to respond_to(:created?) }
      it { expect(task).to respond_to(:pending?) }
      it { expect(task).to respond_to(:canceled?) }
      it { expect(task).to respond_to(:closed?) }
      it { expect(task.accepted?).to be_in([true, false]) }
      it { expect(task.in_progress?).to be_in([true, false]) }
      it { expect(task.done?).to be_in([true, false]) }
      it { expect(task.completed?).to be_in([true, false]) }
      it { expect(task.created?).to be_in([true, false]) }
      it { expect(task.pending?).to be_in([true, false]) }
      it { expect(task.canceled?).to be_in([true, false]) }
      it { expect(task.closed?).to be_in([true, false]) }
    end

    context '#clear_all_notifications' do
      pending 'add test'
    end

    context '#pending!' do
      before { task.pending! }
      it { expect(task.status).to eql(Task.pending) }
    end

    context '#accept!' do
      before { task.accept! }
      it { expect(task.status).to eql(Task.accepted) }
    end

    context '#in_progress!' do
      context 'success' do
        before { task.order.in_progress! }

        context 'non appointment task' do
          before { task_non_appointment.in_progress! }
          it { expect(task_non_appointment.status).to eql(Task.in_progress) }
          it { expect(task_non_appointment.errors.empty?).to eql(true) }
        end

        context 'task with appointment' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.completed, task: task)
            task.in_progress!
          end

          it { expect(task.status).to eql(Task.in_progress) }
          it { expect(task.errors.empty?).to eql(true) }
        end
      end

      context 'error' do
        context 'non appointment task' do
          context 'order is not in progress' do
            before { task_non_appointment.in_progress! }
            it { expect(task_non_appointment.errors.empty?).to eql(false) }
            it { expect(task_non_appointment.errors.size).to eql(1) }
            it { expect(task_non_appointment.errors.full_messages.first).to eql(I18n.t('models.task.errors.order_in_progress')) }
            it { expect(I18n.t('models.task.errors.order_in_progress')).to eql('Order is not yet in IN PROGRESS state. Please wait for the student to set the Order to IN PROGRESS.') }
          end

          context 'no requested file' do
            before do
              task_non_appointment.remove_requested_file!
              order.in_progress!
              task_non_appointment.in_progress!
            end

            it { expect(task_non_appointment.errors.empty?).to eql(false) }
            it { expect(task_non_appointment.errors.size).to eql(1) }
            it { expect(task_non_appointment.errors.full_messages.first).to eql(I18n.t('models.task.errors.requested_file')) }
            it { expect(I18n.t('models.task.errors.requested_file')).to eql('Please wait for the student to upload the request file to be mentored.') }
          end
        end

        context 'task with appointment' do
          context 'order is not in progress and no appointment' do
            before { task.in_progress! }
            it { expect(task.errors.empty?).to eql(false) }
            it { expect(task.errors.size).to eql(2) }
            it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.order_in_progress')) }
            it { expect(task.errors.full_messages.second).to eql(I18n.t('models.task.errors.no_appointment')) }
            it { expect(I18n.t('models.task.errors.order_in_progress')).to eql('Order is not yet in IN PROGRESS state. Please wait for the student to set the Order to IN PROGRESS.') }
            it { expect(I18n.t('models.task.errors.no_appointment')).to eql('Please schedule first an appointment with the student.') }
          end

          context 'order in progress and appointment is approved' do
            before do
              create(:appointment, mentor: user_mentor.mentor, status: Appointment.approved, task: task)
              order.in_progress!
            end

            context 'no requested file' do
              before do
                task.appointment.completed!
                task.remove_requested_file!
                order.in_progress!
                task.in_progress!
              end

              it { expect(task.errors.empty?).to eql(false) }
              it { expect(task.errors.size).to eql(1) }
              it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.requested_file')) }
              it { expect(I18n.t('models.task.errors.requested_file')).to eql('Please wait for the student to upload the request file to be mentored.') }
            end

            context 'appointment not completed' do
              before { task.in_progress! }

              it { expect(task.errors.empty?).to eql(false) }
              it { expect(task.errors.size).to eql(1) }
              it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.appointment_not_completed.in_progress')) }
              it { expect(I18n.t('models.task.errors.appointment_not_completed.in_progress')).to eql('Please wait for your appointment schedule to be COMPLETED.') }
            end
          end
        end
      end
    end

    context '#done!' do
      before { order.in_progress! }
      context 'success' do
        context 'non appointment task' do
          before do
            task_non_appointment.done!
          end

          it { expect(task_non_appointment.status).to eql(Task.done) }
          it { expect(task_non_appointment.errors.empty?).to eql(true) }
        end

        context 'task with appointment' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.completed, task: task)
            task.done!
          end

          it { expect(task.status).to eql(Task.done) }
          it { expect(task.errors.empty?).to eql(true) }
        end
      end

      context 'error' do
        context 'no revised file uploaded' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.completed, task: task)
            task.remove_revised_file!
            task.done!
          end

          it { expect(task.errors.empty?).to eql(false) }
          it { expect(task.errors.size).to eql(1) }
          it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.revised_file')) }
          it { expect(I18n.t('models.task.errors.revised_file')).to eql('Please upload the revision file first before setting the task to DONE.') }
        end

        context 'appointment not yet completed' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.approved, task: task)
            task.done!
          end

          it { expect(task.errors.empty?).to eql(false) }
          it { expect(task.errors.size).to eql(1) }
          it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.appointment_not_completed.done')) }
          it { expect(I18n.t('models.task.errors.appointment_not_completed.done')).to eql('Please finish first your appointment with the student.') }
        end
      end
    end

    context '#completed!' do
      before { order.in_progress! }

      context 'success' do
        context 'non appointment task' do
          before { task_non_appointment.completed! }

          it { expect(task_non_appointment.status).to eql(Task.completed) }
          it { expect(task_non_appointment.errors.empty?).to eql(true) }
        end

        context 'task with appointment' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.completed, task: task)
            task.completed!
          end

          it { expect(task.status).to eql(Task.completed) }
          it { expect(task.errors.empty?).to eql(true) }
        end
      end

      context 'error' do
        context 'appointment not completed' do
          before do
            create(:appointment, mentor: user_mentor.mentor, status: Appointment.approved, task: task)
            task.completed!
          end

          it { expect(task.errors.empty?).to eql(false) }
          it { expect(task.errors.full_messages.first).to eql(I18n.t('models.task.errors.appointment_not_completed.completed')) }
          it { expect(I18n.t('models.task.errors.appointment_not_completed.completed')).to eql('Please finish first your appointment with the mentor or set appointment to COMPLETED if appointment is DONE.') }
        end
      end
    end

    context '#cancel!' do
      before do
        order.in_progress!
        task.cancel!
      end

      it { expect(task.status).to eql(Task.canceled) }
      it { expect(task.errors.empty?).to eql(true) }
    end

    context '#close!' do
      before do
        order.in_progress!
        task.close!
      end

      it { expect(task.status).to eql(Task.closed) }
      it { expect(task.errors.empty?).to eql(true) }
    end
  end
end
