# == Schema Information
#
# Table name: messengers
#
#  id         :integer          not null, primary key
#  task_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  slug       :string(255)
#

require 'rails_helper'

RSpec.describe Messenger, :type => :model do
  context 'association' do
    it { should belong_to(:task) }
    it { should have_many(:messages).dependent(:destroy) }
    it { should have_many(:messenger_participants).dependent(:destroy) }
    it { should have_one(:appointment).through(:task) }
  end

  context 'validation' do
    it { should validate_presence_of(:task) }
  end
end
