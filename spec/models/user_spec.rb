# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  full_name              :string(255)
#  username               :string(255)
#  role                   :string(255)
#  about_me               :text(65535)
#  additional_information :text(65535)
#  primary_photo          :string(255)
#  current_country        :string(255)
#  other_degree           :string(255)
#  phone_number           :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  timezone               :string(255)
#  slug                   :string(255)
#

require 'rails_helper'

RSpec.describe User, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }

  context 'constant' do
    it { expect(User::MAX_FILESIZE).to eql(3.megabytes.to_i) }
    it { expect(User::MAX_FILESIZE).to eql(3145728) }
    it { expect(User::ROLES).to eql(%w(ADMINISTRATOR STUDENT MENTOR)) }
    it { expect(User::SITE_ROLES).to eql(%w(STUDENT MENTOR)) }
    it { expect(User::DEGREES).to have_key(:bachelor) }
    it { expect(User::DEGREES).to have_key(:llm) }
    it { expect(User::DEGREES).to have_key(:jd) }
    it { expect(User::DEGREES).to have_key(:master) }
    it { expect(User::DEGREES).to have_key(:mba) }
    it { expect(User::DEGREES).to have_key(:md) }
    it { expect(User::DEGREES).to have_key(:phd) }
    it { expect(User::DEGREES).to have_key(:other) }
    it { expect(User::DEGREES[:bachelor]).to eql("Bachelor's") }
    it { expect(User::DEGREES[:llm]).to eql('LLM') }
    it { expect(User::DEGREES[:jd]).to eql('JD') }
    it { expect(User::DEGREES[:master]).to eql("Master's NON-MBA") }
    it { expect(User::DEGREES[:mba]).to eql('MBA') }
    it { expect(User::DEGREES[:md]).to eql('MD') }
    it { expect(User::DEGREES[:phd]).to eql('PhD') }
    it { expect(User::DEGREES[:other]).to eql('Other') }
    it { expect(User::MENTOR).to eql('Mentor') }
    it { expect(User::STUDENT).to eql('Student') }
    it { expect(User::DEGREE_NAMES).to eql(User::DEGREES.values) }
    it { expect(User::TIMEZONES).to eql(Timezone::Zone.list.map{|t| [t[:title].gsub('/', ' ').gsub('_', ' '), t[:title]]}.sort) }
  end

  context 'association' do
    it { should have_one(:student) }
    it { should have_one(:mentor) }
    it { should have_many(:user_services) }
    it { should have_many(:services).through(:user_services) }
    it { should have_many(:images).dependent(:destroy) }
    it { should have_many(:alternative_contacts) }
    it { should have_many(:recent_images).class_name('Image') }
    it { should have_many(:notifications).class_name('Notification').with_foreign_key('user_id') }
    it { should have_many(:pending_notifications).class_name('Notification').with_foreign_key('user_id') }
    it { should have_one(:wallet).dependent(:destroy) }
    it { should have_many(:transactions).through(:wallet).dependent(:destroy) }
    it { should have_many(:mentor_orders).through(:mentor).source(:orders) }
    it { should have_many(:student_orders).through(:student).source(:orders) }
    it { should accept_nested_attributes_for(:student).allow_destroy(true) }
    it { should accept_nested_attributes_for(:mentor).allow_destroy(true) }
  end

  context 'validation' do
    it { should validate_presence_of(:username) }
    it { should validate_uniqueness_of(:username) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:current_country) }
    it { should validate_presence_of(:role) }
    it { should validate_presence_of(:timezone) }

    context 'mentor?' do
      subject { user_mentor }
      before { allow(subject).to receive(:mentor?).and_return(true) }

      it { should validate_presence_of(:phone_number) }
      it { should validate_acceptance_of(:reply_speed).on(:create) }
      it { should validate_acceptance_of(:terms_of_use).on(:create) }
    end
  end

  context 'scope' do
    context '.mentors' do
      it { expect(described_class).to respond_to(:mentors) }
      it { expect(User.mentors.to_a).to be_a(Array) }
    end

    context '.students' do
      it { expect(described_class).to respond_to(:students) }
      it { expect(User.students.to_a).to be_a(Array) }
    end
  end

  context 'class_methods' do
    it { expect(described_class).to respond_to(:administrator) }
    it { expect(described_class).to respond_to(:student) }
    it { expect(described_class).to respond_to(:mentor) }
    it { expect(described_class).to respond_to(:bachelor) }
    it { expect(described_class).to respond_to(:llm) }
    it { expect(described_class).to respond_to(:jd) }
    it { expect(described_class).to respond_to(:master) }
    it { expect(described_class).to respond_to(:mba) }
    it { expect(described_class).to respond_to(:md) }
    it { expect(described_class).to respond_to(:phd) }
    it { expect(described_class).to respond_to(:other) }
    it { expect(User.administrator).to eql('ADMINISTRATOR') }
    it { expect(User.mentor).to eql('MENTOR') }
    it { expect(User.student).to eql('STUDENT') }
    it { expect(User.bachelor).to eql('Bachelor\'s') }
    it { expect(User.llm).to eql('LLM') }
    it { expect(User.jd).to eql('JD') }
    it { expect(User.master).to eql('Master\'s NON-MBA') }
    it { expect(User.mba).to eql('MBA') }
    it { expect(User.md).to eql('MD') }
    it { expect(User.phd).to eql('PhD') }
    it { expect(User.other).to eql('Other') }

    context '.available_params' do
      let(:available_params) {
        [
          :username,
          :password,
          :password_confirmation,
          :email,
          :first_name,
          :last_name,
          :phone_number,
          :current_country,
          :about_me,
          :additional_information,
          :primary_photo,
          :timezone,
          :other_degree,
          :service_ids => []
        ]
      }

      it { expect(User.available_params).to eql(available_params) }
    end
  end

  context 'methods' do
    context '#set_full_name' do
      before { user_student.set_full_name }
      it { expect(user_student.full_name).to eql("#{user_student.first_name} #{user_student.last_name}") }
    end

    context '#minimum_length_of_services' do
      before { user_student.minimum_length_of_services }

      it { expect(user_student.errors.empty?).to eql(true) }
      it { expect(user_student.errors.size).to eql(0) }
    end

    context '#my_students' do
      context 'when there are orders' do
        let!(:tasks) { build_list(:task, 1, mentor: user_mentor.mentor, service: consult_service) }
        let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }

        it { expect(user_mentor.my_students.to_a).to be_a(Array) }
        it { expect(user_mentor.my_students.size).to eql(1) }
      end

      context 'when there are no orders' do
        it { expect(user_mentor.my_students.to_a).to be_a(Array) }
        it { expect(user_mentor.my_students.size).to eql(0) }
      end
    end

    context '#my_mentors' do
      context 'when there are orders' do
        let!(:tasks) { build_list(:task, 1, mentor: user_mentor.mentor, service: consult_service) }
        let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }

        it { expect(user_student.my_mentors.to_a).to be_a(Array) }
        it { expect(user_student.my_mentors.size).to eql(1) }
      end

      context 'when there are no orders' do
        it { expect(user_student.my_mentors.to_a).to be_a(Array) }
        it { expect(user_student.my_mentors.size).to eql(0) }
      end
    end

    context '#generate_wallet' do
      it { expect(user_student.wallet).not_to be_nil }
      it { expect(user_student.wallet).to be_a(Wallet) }
      it { expect(user_mentor.wallet).not_to be_nil }
      it { expect(user_mentor.wallet).to be_a(Wallet) }
    end
  end
end
