# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Appointment, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }
  let!(:appointment) { create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule) }

  context 'association' do
    it { should belong_to(:schedule) }
    it { should belong_to(:mentor) }
    it { should belong_to(:task) }
    it { should have_one(:messenger).through(:task) }
    it { should have_one(:student).through(:schedule) }
    it { should have_many(:notifications) }
  end

  context 'validation' do
    it { should validate_presence_of(:schedule) }
    it { should validate_presence_of(:mentor) }
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:task) }
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :timezone,
        :status,
        :schedule_id,
        :mentor_id,
        :task_id
      ]
    }

    it { expect(Appointment.available_params).to eql(available_params) }
    it { expect(described_class).to respond_to(:pending) }
    it { expect(described_class).to respond_to(:approved) }
    it { expect(described_class).to respond_to(:completed) }
    it { expect(described_class).to respond_to(:rescheduled) }
    it { expect(described_class).to respond_to(:canceled) }
    it { expect(described_class).to respond_to(:closed) }
    it { expect(Appointment.pending).to eql('PENDING') }
    it { expect(Appointment.approved).to eql('APPROVED') }
    it { expect(Appointment.completed).to eql('COMPLETED') }
    it { expect(Appointment.rescheduled).to eql('RESCHEDULED') }
    it { expect(Appointment.canceled).to eql('CANCELED') }
    it { expect(Appointment.closed).to eql('CLOSED') }
  end

  context 'methods' do
    context 'statuses' do
      it { expect(appointment).to respond_to(:pending?) }
      it { expect(appointment).to respond_to(:approved?) }
      it { expect(appointment).to respond_to(:completed?) }
      it { expect(appointment).to respond_to(:rescheduled?) }
      it { expect(appointment).to respond_to(:canceled?) }
      it { expect(appointment).to respond_to(:closed?) }
      it { expect(appointment.pending?).to be_in([true, false]) }
      it { expect(appointment.approved?).to be_in([true, false]) }
      it { expect(appointment.completed?).to be_in([true, false]) }
      it { expect(appointment.rescheduled?).to be_in([true, false]) }
      it { expect(appointment.canceled?).to be_in([true, false]) }
      it { expect(appointment.closed?).to be_in([true, false]) }
    end

    context '#presenter' do
      it { expect(appointment.presenter).to be_a(AppointmentPresenter) }
      it { expect(appointment.presenter).to be_an_instance_of(AppointmentPresenter) }
    end

    context '#decline!' do
      context 'success' do
        context 'when schedule is created by student' do
          before { appointment.decline! }

          it { expect(appointment.schedule.status).to eql(Schedule.available) }
          it { expect(Appointment.exists?(appointment.id)).to eql(false) }
          it { expect(appointment.errors.empty?).to eql(true) }
        end

        context 'when schedule is created by a mentor | resquest schedule' do
          before do
            request_schedule = create(:schedule, student: user_student.student, status: Schedule.request_schedule)
            appointment.schedule = request_schedule
            appointment.decline!
          end

          it { expect(Appointment.exists?(appointment.id)).to eql(false) }
          it { expect(appointment.errors.empty?).to eql(true) }
        end

      end
    end

    context '#confirm!' do
      context 'success' do
        before { appointment.confirm! }

        it { expect(appointment.status).to eql(Appointment.approved) }
        it { expect(appointment.schedule.status).to eql(Schedule.occupied) }
        it { expect(appointment.task.messenger).not_to be_nil }
        it { expect(appointment.task.messenger).to be_a(Messenger) }
        it { expect(appointment.task.messenger.messenger_participants.size).to eql(2) }
        it { expect(appointment.errors.empty?).to eql(true) }
      end
    end

    context '#cancel!' do
      context 'success' do
        before do
          users = [appointment.mentor.user, appointment.student.user]
          TaskMessenger.new(appointment.task, users).create
          appointment.cancel!
        end

        it { expect(appointment.errors.empty?).to eql(true) }
        it { expect(appointment.schedule.status).to eql(Schedule.available) }
        it { expect(Messenger.exists?(appointment.messenger.id)).to eql(false) }
      end
    end

    context '#close!' do
      context 'success' do
        before { appointment.close! }

        it { expect(appointment.status).to eql(Appointment.closed) }
      end
    end

    context '#completed!' do
      context 'success' do
        before do
          schedule.status = Schedule.occupied
          appointment.status = Appointment.approved
          appointment.completed!
        end

        it { expect(appointment.status).to eql(Appointment.completed) }
        it { expect(appointment.errors.empty?).to eql(true) }
      end

      context 'error' do
        context 'when schedule is not occupied' do
          before do
            appointment.status = Appointment.approved
            appointment.completed!
          end

          it { expect(appointment.errors.empty?).to eql(false) }
          it { expect(appointment.errors.full_messages.first).to eql(I18n.t('models.appointment.errors.completed')) }
          it { expect(I18n.t('models.appointment.errors.completed')).to eql('You can\'t set an appointment to completed if not approved.') }
        end

        context 'when appointment is not approved' do
          before do
            schedule.status = Schedule.occupied
            appointment.completed!
          end

          it { expect(appointment.errors.empty?).to eql(false) }
          it { expect(appointment.errors.full_messages.first).to eql(I18n.t('models.appointment.errors.completed')) }
          it { expect(I18n.t('models.appointment.errors.completed')).to eql('You can\'t set an appointment to completed if not approved.') }
        end

        context 'when schedule is not occupied and appointment is not approved' do
          before { appointment.completed! }

          it { expect(appointment.errors.empty?).to eql(false) }
          it { expect(appointment.errors.full_messages.first).to eql(I18n.t('models.appointment.errors.completed')) }
          it { expect(I18n.t('models.appointment.errors.completed')).to eql('You can\'t set an appointment to completed if not approved.') }
        end
      end
    end
  end
end
