# == Schema Information
#
# Table name: alternative_contacts
#
#  id            :integer          not null, primary key
#  account_type  :string(255)
#  account_value :string(255)
#  contact_id    :integer
#  contact_type  :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

require 'rails_helper'

RSpec.describe AlternativeContact, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:alternative_contact) { create(:alternative_contact, contact: user_student.student) }

  context 'association' do
    it { should belong_to(:contact) }
  end

  context 'validation' do
    context 'if account_type is present?' do
      before { allow(subject).to receive(:account_type?).and_return(true) }
      it { should validate_presence_of(:account_value) }
    end

    context 'if account_type is not present?' do
      before { allow(subject).to receive(:account_type?).and_return(false) }
      it { should_not validate_presence_of(:account_value) }
    end
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :account_type,
        :account_value
      ]
    }

    it { expect(AlternativeContact.available_params).to eql(available_params) }
  end

  context 'methods' do
    context '#account_type?' do
      it { expect(alternative_contact.account_type?).not_to be_nil }
      it { expect(alternative_contact.account_type?).to be_in([true, false]) }
    end

    context '#attach_to_user' do
      it { expect(alternative_contact.attach_to_user.user).not_to be_nil }
      it { expect(alternative_contact.attach_to_user.user).to be_a(User) }
      it { expect(alternative_contact.attach_to_user.user).to eql(alternative_contact.user) }
    end
  end
end
