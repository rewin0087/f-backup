# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text(65535)
#  price           :decimal(10, )
#  currency        :string(255)
#  currency_symbol :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_type    :string(255)
#

require 'rails_helper'

RSpec.describe Service, :type => :model do
  let!(:consult_service) { create(:consult_service) }

  context 'constant' do
    let(:services) do
      [
        { title: 'Basic Essay Review', description: ' (including scholarship app review) Basic essay review services (providing comments and editing errors) at $25 per 500 words (48 hr response time)', price: 25, currency: 'USD', currency_symbol: '$', service_type: Service.non_appointment_service },
        { title: 'Advanced essay review', description: 'logic review and two rounds of editing with feedback at 500 words for $75 with 30 minute video chat (96 hr response time for first contact, 48 hr response time for subsequent requests)', price: 75, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Consulting', description: 'School selection; CV review; detailed application timeline; student life; etc', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Scholarships', description: 'Finding scholarships and application review', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Recommendation', description: 'Recommendation letter questions', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Mock Interviews', description: '$20 per hour (includes preparation work by mentor)', price: 20, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'College Visit', description: 'Personal Tour Guide', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Find a job', description: 'How to find a job after college', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
        { title: 'Other consultation services', description: 'CV/Resume review; Creating an application timeline; Assistance for Scholarships and Financial Aid; Assistance on Housing, Residency, Student Visa, etc.;Personal campus tour guide', price: 20, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service }
      ]
    end

    it { expect(Service::TYPES).to eql(%w(NON_APPOINTMENT_SERVICE APPOINTMENT_SERVICE)) }
    it { expect(Service::ALL).to eql(services) }

    it { expect(described_class).to respond_to(:non_appointment_service) }
    it { expect(described_class).to respond_to(:appointment_service) }
    it { expect(Service.non_appointment_service).to eql('NON_APPOINTMENT_SERVICE') }
    it { expect(Service.appointment_service).to eql('APPOINTMENT_SERVICE') }
  end

  context 'association' do
    it { should have_many(:user_services) }
    it { should have_many(:users).through(:user_services) }
    it { should have_many(:tasks) }
    it { should have_many(:orders).through(:tasks) }
  end

  context 'methods' do
    it { expect(consult_service).to respond_to(:non_appointment_service?) }
    it { expect(consult_service).to respond_to(:appointment_service?) }
    it { expect(consult_service.non_appointment_service?).to be_in([true, false]) }
    it { expect(consult_service.appointment_service?).to be_in([true, false]) }
  end
end
