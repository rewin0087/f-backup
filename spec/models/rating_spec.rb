# == Schema Information
#
# Table name: ratings
#
#  id               :integer          not null, primary key
#  speed            :float(24)
#  knowledgeability :float(24)
#  overall_rating   :float(24)
#  average          :float(24)
#  feedbacks        :string(255)
#  task_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'rails_helper'

RSpec.describe Rating, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
