# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Schedule, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { [build(:task, mentor: user_mentor.mentor, service: consult_service), build(:task, mentor: user_mentor.mentor, service: basic_review)] }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:schedule) { create(:schedule, student: user_student.student) }

  context 'constant' do
    it { expect(Schedule::STATUSES).to eql(%w(AVAILABLE OCCUPIED CLOSED WAITING_FOR_CONFIRMATION REQUEST_SCHEDULE)) }
    it { expect(Schedule::NOTIFICATION_TEMPLATE).to have_key(:WAITING_FOR_CONFIRMATION) }
    it { expect(Schedule::NOTIFICATION_TEMPLATE).to have_key(:REQUEST_SCHEDULE) }
    it { expect(Schedule::NOTIFICATION_TEMPLATE[:WAITING_FOR_CONFIRMATION]).to eql('Your Mentor is waiting for your confirmation for your appointment.') }
    it { expect(Schedule::NOTIFICATION_TEMPLATE[:REQUEST_SCHEDULE]).to eql('Your Mentor is waiting for your confirmation for a request appointment schedule.') }
    it { expect(Schedule::MAX_TIME_CHECKING).to eql(2.weeks) }
    it { expect(Schedule::MAX_SCHEDULES_PER_TWO_WEEKS).to eql(10) }
    it { expect(Schedule::APPOINTMENT_CANCELLATION_OFFSET).to eql(4.hours) }
  end

  context 'association' do
    it { should belong_to(:student) }
    it { should have_one(:appointment).dependent(:destroy) }
    it { should have_one(:mentor).through(:appointment) }
    it { should have_many(:notifications).dependent(:destroy) }
    it { should have_one(:task).through(:appointment) }
  end

  context 'validation' do
    it { should validate_presence_of(:timezone) }
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:start_time) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:end_time) }
    it { should validate_presence_of(:student) }
    it { should validate_uniqueness_of(:start_time).scoped_to(:start_date, :student_id) }
    it { should validate_uniqueness_of(:end_time).scoped_to(:start_date, :student_id) }
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :start_date,
        :start_time,
        :end_date,
        :end_time,
        :timezone,
        :student_id,
        :status,
      ]
    }

    it { expect(Schedule.available_params).to eql(available_params) }
    it { expect(described_class).to respond_to(:available) }
    it { expect(described_class).to respond_to(:occupied) }
    it { expect(described_class).to respond_to(:closed) }
    it { expect(described_class).to respond_to(:waiting_for_confirmation) }
    it { expect(described_class).to respond_to(:request_schedule) }
    it { expect(Schedule.available).to eql('AVAILABLE') }
    it { expect(Schedule.occupied).to eql('OCCUPIED') }
    it { expect(Schedule.closed).to eql('CLOSED') }
    it { expect(Schedule.waiting_for_confirmation).to eql('WAITING_FOR_CONFIRMATION') }
    it { expect(Schedule.request_schedule).to eql('REQUEST_SCHEDULE') }
  end

  context 'methods' do
    context 'statuses' do
      it { expect(schedule).to respond_to(:available?) }
      it { expect(schedule).to respond_to(:occupied?) }
      it { expect(schedule).to respond_to(:closed?) }
      it { expect(schedule).to respond_to(:waiting_for_confirmation?) }
      it { expect(schedule).to respond_to(:request_schedule?) }
      it { expect(schedule.available?).to be_in([true, false]) }
      it { expect(schedule.occupied?).to be_in([true, false]) }
      it { expect(schedule.closed?).to be_in([true, false]) }
      it { expect(schedule.waiting_for_confirmation?).to be_in([true, false]) }
      it { expect(schedule.request_schedule?).to be_in([true, false]) }
    end

    context '#start_datetime' do
      it { expect(schedule.start_datetime).to eql("#{schedule.start_date} #{schedule.start_time.strftime('%I:%M %p')}") }
    end

    context '#end_datetime' do
      it { expect(schedule.end_datetime).to eql("#{schedule.end_date} #{schedule.end_time.strftime('%I:%M %p')}") }
    end

    context '#formatted_start_time' do
      it { expect(schedule.formatted_start_time).to eql(schedule.start_time.strftime('%I:%M %p')) }
    end

    context '#formatted_end_time' do
      it { expect(schedule.formatted_end_time).to eql(schedule.end_time.strftime('%I:%M %p')) }
    end

    context '#waiting_for_confirmation!' do
      before do
        create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule)
        schedule.waiting_for_confirmation!
      end

      it { expect(schedule.status).to eql(Schedule.waiting_for_confirmation) }
    end

    context '#allow_cancellation?' do
      context 'when allowed to cancel because time is behind the apppointment time and behind the cancellation offset time' do
        before do
          schedule.start_date = DateTime.now.utc + 2.days
          schedule.end_date = DateTime.now.utc + 2.days
          schedule.status = Schedule.occupied
        end

        it { expect(schedule.allow_cancellation?).to eq(true) }
      end

      context 'when not allowed to cancel because already ahead the cancellation offset time and its time for the appointment' do
        before do
          schedule.status = Schedule.occupied
        end

        it { expect(schedule.allow_cancellation?).to eq(false) }
      end
    end

    context '#start_appointment?' do
      before { schedule.status = Schedule.occupied }

      context 'can start appointment' do
        it { expect(schedule.start_appointment?).to eql(true) }
      end

      context 'cant start appointment' do
        before do
          schedule.start_date = DateTime.now.utc + 3.days
        end

        it { expect(schedule.start_appointment?).to eql(false) }
      end
    end

    context '#converted_start_datetime' do
      it { expect(schedule.converted_start_datetime).to have_key(:now) }
      it { expect(schedule.converted_start_datetime).to have_key(:start_datetime) }
      it { expect(schedule.converted_start_datetime[:now]).to eql(DateTimeHelper.new(Time.now, schedule.timezone).convert_with_timezone.datetime) }
      it { expect(schedule.converted_start_datetime[:start_datetime]).to eql(DateTimeHelper.new(schedule.start_datetime, schedule.timezone).convert_with_timezone.datetime) }
    end

    context '#start_datetime_remaining' do
      let(:remaining) {
        datetime = schedule.converted_start_datetime
        time_remaining = (datetime[:start_datetime] - datetime[:now]).round
        time_remaining > 0 ? time_remaining : 0
      }

      it { expect(schedule.start_datetime_remaining).to eql(remaining) }
    end

    context '#end_appointment?' do
      context 'when appointment is ahead the apoointment time' do
        before do
          schedule.status = Schedule.occupied
          schedule.start_date = DateTime.now.utc + 2.hours
        end

        it { expect(schedule.end_appointment?).to eql(true) }
      end

      context 'when appointment is not yet started' do
        before do
          schedule.status = Schedule.occupied
          schedule.end_date = DateTime.now.utc + 2.days
        end

        it { expect(schedule.end_appointment?).to eql(false) }
      end
    end

    context '#close!' do
      before do
        create(:appointment, mentor: user_mentor.mentor, status: Appointment.pending, task: tasks[0], schedule: schedule)
      end

      context 'when schedule is occupied and appointment is approved' do
        before do
          schedule.status = Schedule.occupied
          schedule.appointment.status = Appointment.approved
          schedule.close!
        end

        it { expect(schedule.status).to eql(Schedule.closed) }
        it { expect(schedule.errors.empty?).to eql(true) }
      end

      context 'when schedule is not occupied and approved' do
        before do
          schedule.status = Schedule.occupied
          schedule.close!
        end

        it { expect(schedule.errors.empty?).to eql(false) }
        it { expect(schedule.errors.full_messages.first).to eql('You can\'t set an appointment to completed if not approved.') }
      end
    end
  end
end
