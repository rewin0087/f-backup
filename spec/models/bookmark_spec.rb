# == Schema Information
#
# Table name: bookmarks
#
#  id         :integer          not null, primary key
#  student_id :integer
#  mentor_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Bookmark, :type => :model do
  context 'association' do
    it { should belong_to(:student) }
    it { should belong_to(:mentor) }
  end
end
