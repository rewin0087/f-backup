require 'rails_helper'

RSpec.describe Transaction, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review]) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:tasks) { build_list(:task, 1, mentor: user_mentor.mentor, service: consult_service) }
  let!(:order) { create(:order, student: user_student.student, mentor: user_mentor.mentor, tasks: tasks) }
  let!(:paypal_transaction) { create(:paypal_transaction, wallet: user_student.wallet, order: nil) }
  let!(:credit_card_transaction) { create(:credit_card_transaction, wallet: user_student.wallet, order: nil) }

  context 'association' do
    it { should belong_to(:wallet) }
    it { should have_one(:user).through(:wallet) }
    it { should belong_to(:order) }
  end

  context 'class_methods' do
    let(:available_params) {
      [
        :amount,
        :another_amount,
        :card_number,
        :expire_month,
        :expire_year,
        :cvv2,
        :payment_gateway
      ]
    }

    it { expect(Transaction.available_params).to eql(available_params) }
    it { expect(Transaction::ITEM_NAME).to eql('a friend abroad load wallet') }
    it { expect(Transaction::TYPE).to include(:debit, :credit) }
    it { expect(Transaction::PAYMENT_GATEWAY).to include(:paypal, :cc) }
    it { expect(Transaction::PAYPAL_STATE).to include(:completed, :created, :approved) }
  end

  context 'methods' do

    context '#update_wallet' do
      before do
        order.create_new_transaction
      end

      context 'when order status is pending' do
        it { expect(order.transactions.last.status).to eql(Order.pending) }
        it { expect(order.student.user.wallet.pending_balance).to eql(order.price) }
        it { expect(order.mentor.user.wallet.pending_balance).to eql(order.price) }
        it { expect(user_student.wallet.current_balance).to eql(995) }
        it { expect(user_mentor.wallet.current_balance).to eql(5) }
      end

      context 'when order status is canceled' do
        before do
          order.cancel!
        end

        it { expect(order.transactions.last.status).to eql(Order.canceled) }
        it { expect(order.student.user.wallet.pending_balance).to eql(0) }
        it { expect(order.mentor.user.wallet.pending_balance).to eql(0) }
        it { expect(user_student.wallet.current_balance).to eql(1000) }
        it { expect(user_mentor.wallet.current_balance).to eql(0) }
      end

      context 'when order status is declined' do
        before do
          order.decline!
        end

        it { expect(order.transactions.last.status).to eql(Order.declined) }
        it { expect(order.student.user.wallet.pending_balance).to eql(0) }
        it { expect(order.mentor.user.wallet.pending_balance).to eql(0) }
        it { expect(user_student.wallet.current_balance).to eql(1000) }
        it { expect(user_mentor.wallet.current_balance).to eql(0) }
      end

      context 'when order status is closed' do
        before do
          order.tasks.first.update_attributes(status: 'COMPLETED')
          order.close!
        end

        it { expect(order.transactions.last.status).to eql(Order.closed) }
        it { expect(order.student.user.wallet.pending_balance).to eql(0) }
        it { expect(order.mentor.user.wallet.pending_balance).to eql(0) }
        it { expect(order.student.user.wallet.amount).to eql(995) }
        it { expect(order.mentor.user.wallet.amount).to eql(5) }
        it { expect(user_student.wallet.current_balance).to eql(995) }
        it { expect(user_mentor.wallet.current_balance).to eql(5) }
      end
    end

    context '#paypal?' do
      it { expect(paypal_transaction.paypal?).to be_truthy }
    end

    context '#credit_card?' do
      it { expect(credit_card_transaction.credit_card?).to be_truthy }
    end
  end
end