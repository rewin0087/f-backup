# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  status         :string(255)
#  imageable_id   :integer
#  imageable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

include FileUploadHelper

RSpec.describe Image, :type => :model do
  let!(:consult_service) { create(:consult_service) }
  let!(:basic_review) { create(:basic_review) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review]) }
  let!(:image) { create(:image, status: Image.public, imageable: user_mentor) }

  context 'constant' do
    it { expect(Image::STATUSES).to eql(%w(PUBLIC PRIVATE)) }
  end

  context 'association' do
    it { should belong_to(:imageable) }
  end

  context 'scope' do
    it { expect(described_class).to respond_to(:recent_images) }
  end

  context 'class_methods' do
    it { expect(described_class).to respond_to(:public) }
    it { expect(described_class).to respond_to(:private) }
    it { expect(Image.public).to eql('PUBLIC') }
    it { expect(Image.private).to eql('PRIVATE') }
  end

  context 'methods' do
    it { expect(image).to respond_to(:public?) }
    it { expect(image).to respond_to(:private?) }
    it { expect(image.public?).to be_in([true, false]) }
    it { expect(image.private?).to be_in([true, false]) }
    it { expect(image.public?).to eql(true) }
    it { expect(image.private?).to eql(false) }
  end
end
