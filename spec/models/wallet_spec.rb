require 'rails_helper'

RSpec.describe Wallet, :type => :model do
  let!(:basic_review) { create(:basic_review) }
  let!(:consult_service) { create(:consult_service) }
  let!(:user_mentor) { create(:user_mentor, services: [consult_service, basic_review], current_country: 'Philippines') }
  let!(:user_student) { create(:user_student, services: [consult_service, basic_review], current_country: 'Philippines') }

  context 'association' do
    it { should belong_to(:user) }
    it { should have_many(:transactions) }
  end

  context '#current_balance' do
    before do
      user_mentor.wallet.update_attributes(amount: 200, pending_balance: 50)
      user_student.wallet.update_attributes(amount: 200, pending_balance: 50)
    end

    it { expect(user_mentor.wallet.current_balance).to eql(250) }
    it { expect(user_student.wallet.current_balance).to eql(150) }
  end
end