# == Schema Information
#
# Table name: recommendations
#
#  id         :integer          not null, primary key
#  message    :text(65535)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Recommendation, :type => :model do
  context 'association' do
    it { should belong_to(:user) }
  end

  context 'validation' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:message) }
  end

  context 'class_methods' do
    context '.available_params' do
      it { expect(Recommendation.available_params).to eql([:user_id, :message]) }
    end
  end
end
