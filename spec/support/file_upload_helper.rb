module FileUploadHelper
  def tempfile_access(file)
    class << file
      attr_reader :tempfile
    end

    file
  end
end