
include FileUploadHelper
RSpec.shared_examples_for 'should have access to all images resource' do
  let(:file) { tempfile_access(fixture_file_upload('image.png', 'image/png')) }
  before { sign_in user }

  context 'GET index' do
    before do
      get :index, profile_id: user.id
    end

    it { expect(response).to have_http_status 200 }
    it { expect(response).to render_template('images/index') }
    it { expect(assigns(:images)).to be_a(ActiveRecord::Relation) }
    it { expect(assigns(:images)).not_to be_empty }
    it { expect(assigns(:images).size).to eql(1) }
    it { expect(assigns(:images).to_a).to be_a(Array) }
    it { expect(assigns(:profile)).to eql(user) }
  end

  context 'POST create' do
    before do
      xhr :post, :create, profile_id: user.id, images: { '0': file }, format: :js
    end

    it { expect(response).to have_http_status 200 }
    it { expect(response).to render_template('images/create') }
    it { expect(assigns(:images).size).to eql(2) }
    it { expect(assigns(:images)).to be_a(ActiveRecord::Relation) }
    it { expect(assigns(:images).to_a).to be_a(Array) }
    it { expect(assigns(:profile)).to eql(user) }
  end

  context 'DELETE destroy' do
    before do
      xhr :delete, :destroy, profile_id: user.id, id: image.id, format: :js
    end

    it { expect(response).to have_http_status 200 }
    it { expect(response).to render_template('images/destroy') }
    it { expect(assigns(:image).destroyed?).to eql(true) }
    it { expect(Image.exists?(image.id)).to eql(false) }
  end

  context 'PUT set_as_primary_photo' do
    before do
      xhr :put, :set_as_primary_photo, profile_id: user.id, id: image.id, format: :js
    end

    it { expect(response).to have_http_status 200 }
    it { expect(response).to render_template('images/set_as_primary_photo') }
    it { expect(assigns(:profile).primary_photo).not_to eql(user.primary_photo) }
    it { expect(assigns(:image)).to eql(image) }
  end
end