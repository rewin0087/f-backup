RSpec.shared_examples_for 'should have access to all scholastics resource' do
  describe 'GET universities' do
    context 'when q is present' do
      let(:q) { 'baguio' }
      let(:universities) { YUniversity::NAMES.select { |u| u.downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u, text: u }} }

      before do
        get :universities, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:universities)).to eql(universities) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :universities, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:universities)).to be_nil }
    end
  end

  describe 'GET languages' do
    context 'when q is present' do
      let(:q) { 'english' }
      let(:languages) { LanguageList::ALL_LANGUAGES.map(&:name).sort.select{ |u| u.downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u, text: u } } }

      before do
        get :languages, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:languages)).to eql(languages) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :languages, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:languages)).to be_nil }
    end
  end

  describe 'GET countries' do
    context 'when q is present' do
      let(:q) { 'japan' }
      let(:countries) { YUniversity::COUNTRY_NAMES.sort.select{ |u| u.downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u, text: u } } }

      before do
        get :countries, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:countries)).to eql(countries) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :countries, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:countries)).to be_nil }
    end
  end

  describe 'GET majors' do
    context 'when q is present' do
      let(:q) { 'computer' }
      let(:majors) { UniversityMajorSubjects::MAJORS.map{ |subject| subject[:major] }.sort.select{ |u| u.downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u, text: u } } }

      before do
        get :majors, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:majors)).to eql(majors) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :majors, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:majors)).to be_nil }
    end
  end

  describe 'GET timezones' do
    context 'when q is present' do
      let(:q) { 'manila' }
      let(:timezones) { User::TIMEZONES.sort.select{ |u| u[0].downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u[1], text: u[0] } } }

      before do
        get :timezones, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:timezones)).to eql(timezones) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :timezones, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:timezones)).to be_nil }
    end
  end

  describe 'GET search_tags' do
    let(:tags) {
      majors ||= UniversityMajorSubjects::MAJORS.map{ |subject| subject[:major] }
      degrees ||= User::DEGREE_NAMES
      services ||= Service.all.map(&:title)
      countries ||= YUniversity::COUNTRY_NAMES
      universities = YUniversity::NAMES
      (majors + degrees + services + countries + universities).sort
    }

    context 'when q is present' do
      let(:q) { 'computer' }
      let(:result_tags) { tags.select{ |u| u.downcase.include?(q.downcase) || u.include?(q) }.take(30).map{|u| { id: u, text: u } } }

      before do
        result_tags << q
        get :search_tags, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:tags)).to eql(result_tags) }
    end

    context 'when q is not present' do
      let(:q) { '' }

      before do
        get :search_tags, q: q, format: :json
      end

      it { expect(response).to have_http_status 200 }
      it { expect(assigns(:tags)).to be_nil }
    end
  end
end