class SchedulePresenter < BaseDecorator
  def schedule
    __getobj__
  end

  def start_datetime(user)
    viewer_timezone = user.timezone
    schedule_datetime = DateTimeHelper.new(schedule.start_datetime, schedule.timezone, viewer_timezone).convert_with_timezone
    @start_datetime ||= DateTimeHelper.presentable_datetime(schedule_datetime.datetime)
  end

  def end_datetime(user)
    viewer_timezone = user.timezone
    schedule_datetime = DateTimeHelper.new(schedule.end_datetime, schedule.timezone, viewer_timezone).convert_with_timezone
    @end_datetime ||= DateTimeHelper.presentable_datetime(schedule_datetime.datetime)
  end
end