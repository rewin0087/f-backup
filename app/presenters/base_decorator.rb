class BaseDecorator < SimpleDelegator
  def self.wrap(collection)
    collection.map do |o|
      new o
    end
  end
end
