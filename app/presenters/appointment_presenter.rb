class AppointmentPresenter < BaseDecorator
  def appointment
    __getobj__
  end

  def schedule
    @schedule ||= SchedulePresenter.new(appointment.schedule)
  end
end