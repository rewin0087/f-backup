class NotifyMailer < ApplicationMailer
  def notification_for_student(user_id, mentor_user_id)
    @student = User.find(user_id)
    @mentor_user = User.find(mentor_user_id)
    subject = "Email from our mentor"
    recipients = @student.email
    from = 'afriendabroad.com <support@afriendabroad.com>'
    mail(to: recipients, from: from, subject: subject)
  end

  def private_message(from, to, message)
    @from = User.find(from)
    @to = User.find(to)
    @message = message
    subject = "Private Message from #{@from.full_name}"
    recipients = @to.email
    from = 'afriendabroad.com <support@afriendabroad.com>'
    mail(to: recipients, from: from, subject: subject)
  end
end
