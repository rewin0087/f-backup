class RecommendationMailer < ApplicationMailer
  def new_recommendation_notification_to_admins(recommendation_id)
    @recommendation = Recommendation.find(recommendation_id)
    @admins = AdminUser.all
    @emails = @admins.map{|a| a.email}
    subject = 'Notification: User Recommendations'
    recipients = @emails
    from = @recommendation.user.email
    mail(to: recipients, from: from, subject: subject)
  end
end
