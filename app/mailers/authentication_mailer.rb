class AuthenticationMailer < ApplicationMailer
  def template_for_student(user_id)
    @student = User.find(user_id)
    subject = "Thank you for registering to AfriendAbroad"
    recipients = @student.email
    from = 'afriendabroad.com <support@afriendabroad.com>'
    mail(to: recipients, from: from, subject: subject)
  end

  def template_for_mentor(user_id)
    @mentor = User.find(user_id)
    subject = "Thank you for registering to AfriendAbroad"
    recipients = @mentor.email
    from = 'afriendabroad.com <support@afriendabroad.com>'
    mail(to: recipients, from: from, subject: subject)
  end
end
