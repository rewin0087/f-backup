class AppointmentMailer < ApplicationMailer
  def appointment_request_notification_to_mentor(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Request for Appointment"
    recipients = @appointment.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

   def appointment_request_notification_to_student(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Request for Appointment"
    recipients = @appointment.schedule.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def decline_appointment_notification_to_mentor(appointment)
    @appointment = appointment
    subject = "Notification: Declined appointment"
    recipients = @appointment.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def decline_request_appointment_schedule_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = "Notification: Declined request appointment schedule"
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def approved_notification_to_student(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Accepted the appointment"
    recipients = @appointment.schedule.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def approved_notification_to_mentor(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Accepted the appointment"
    recipients = @appointment.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def cancel_notification_to_mentor(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Cancellation of appointment"
    recipients = @appointment.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def cancel_notification_to_student(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Cancellation of appointment"
    recipients = @appointment.schedule.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def completed_notification_to_mentor(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Appointment Completed"
    recipients = @appointment.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def completed_notification_to_student(appointment_id)
    @appointment = Appointment.find(appointment_id)
    subject = "Notification: Appointment Completed"
    recipients = @appointment.schedule.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end
end
