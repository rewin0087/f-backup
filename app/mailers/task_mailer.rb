class TaskMailer < ApplicationMailer
  def upload_request_file_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Request file to be mentored uploaded'
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def upload_revised_file_notification_to_student(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Revised file to be was uploaded'
    recipients = @task.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def in_progress_notification_to_student(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task is In Progress'
    recipients = @task.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def back_to_in_progress_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task is Reverted to In Progress'
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def done_notification_to_student(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Mentor is done with your task'
    recipients = @task.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def completed_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task Completed'
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def closed_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task is closed'
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def additional_instruction_notification_to_mentor(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task Additional Instruction from Student'
    recipients = @task.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def encouragement_message_notification_to_student(task_id)
    @task = Task.find(task_id)
    subject = 'Notification: Task Encouragement message from Mentor'
    recipients = @task.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end
end
