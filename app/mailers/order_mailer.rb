class OrderMailer < ApplicationMailer
  def pending_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Pending Order'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def accepted_notification_to_student(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Accepted Order'
    recipients = @order.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def confirmed_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Comfirmed Order'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def in_progress_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: In Progress Order'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def completed_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Order Complete'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def declined_notification_to_student(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Declined Order'
    recipients = @order.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def canceled_notification_to_student(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Canceled Order'
    recipients = @order.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def canceled_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Canceled Order'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def closed_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Order closed'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def closed_notification_to_student(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Order closed'
    recipients = @order.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def additional_instruction_notification_to_mentor(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Order Additional Instruction from Student'
    recipients = @order.mentor.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end

  def encouragement_message_notification_to_student(order_id)
    @order = Order.find(order_id)
    subject = 'Notification: Order Encouragement message from Mentor'
    recipients = @order.student.user.email
    from = "afriendabroad.com <#{ENV['DEFAULT_SENDER']}>"
    mail(to: recipients, from: from, subject: subject)
  end
end
