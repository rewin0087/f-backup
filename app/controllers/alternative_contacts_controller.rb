# == Schema Information
#
# Table name: alternative_contacts
#
#  id            :integer          not null, primary key
#  account_type  :string(255)
#  account_value :string(255)
#  contact_id    :integer
#  contact_type  :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

class AlternativeContactsController < ApplicationController
  before_filter :set_profile
  before_filter :set_alternative_contact, only: [:update, :destroy]
  before_filter :authorize_resource!

  def create
    fail Pundit::NotAuthorizedError if not allowed_user?
    contactable = @profile.mentor? ? @profile.mentor : @profile.student

    @contact = contactable.alternative_contacts.new(sanitize_params)
    @contact.valid?
    @contact.save
  end

  def update
    @contact.assign_attributes(sanitize_params)
    @contact.valid?
    @contact.save
  end

  def destroy
    @contact.destroy
  end

  private

  def authorize_resource!
    authorize @contact.present? ? @contact : :alternative_contact
  end

  def set_alternative_contact
    @contact = AlternativeContact.find params[:id]
  end

  def sanitize_params
    params.require(:alternative_contact).permit(alternative_contact_params)
  end

  def alternative_contact_params
    AlternativeContact.available_params
  end
end
