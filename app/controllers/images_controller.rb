# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  status         :string(255)
#  imageable_id   :integer
#  imageable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ImagesController < ApplicationController
  before_filter :set_profile
  before_filter :set_image, only: [:destroy, :set_as_primary_photo]
  before_filter :authorize_resource!

  def index
    fail Pundit::NotAuthorizedError if not allowed_user?

    @images = @profile.images.page(params[:page])
  end

  def create
    fail Pundit::NotAuthorizedError if not allowed_user?

    if params[:images].size > 0
      total = params[:images].values.size
      @profile.images << params[:images].values.map { |image| Image.new(source: image, status: Image.public) }
      @profile.save
      @images = params[:scheme].present? && params[:scheme] == 'true' ? @profile.images.recent_images(total) : @profile.recent_images(nil)
    end
  end

  def destroy
    @image.destroy
  end

  def set_as_primary_photo
    @profile.primary_photo = @image.source
    @profile.save
  end

  protected

  def authorize_resource!
    authorize @image.present? ? @image : :image
  end

  def set_image
    @image = Image.find params[:id]
  end
end
