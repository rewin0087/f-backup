# == Schema Information
#
# Table name: ratings
#
#  id               :integer          not null, primary key
#  speed            :float(24)
#  knowledgeability :float(24)
#  overall_rating   :float(24)
#  average          :float(24)
#  feedbacks        :string(255)
#  task_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class RatingsController < ApplicationController
  before_filter :set_profile
  before_filter :set_task
  before_filter :authorize_resource!

  def index
  end

  def create
    @rating = @task.create_rating(sanitize_params)
  end

  private

  def sanitize_params
    params.require(:rating).permit(rating_params)
  end

  def rating_params
    Rating.available_params
  end

  def set_task
    @task = Task.find params[:task_id]
  end
end
