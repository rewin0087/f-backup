# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SchedulesController < ApplicationController
  before_filter :set_profile
  before_filter :set_schedule, only: [:show, :edit, :destroy, :update, :close]
  before_filter :authorize_resource!

  def index
    respond_to do |format|
      format.json do
        @schedules = UserSchedule.new(@profile, params[:timezone], { start: params[:start], end: params[:end] }, params[:status]).for_student
        render json: @schedules
      end
      format.html { fail Pundit::NotAuthorizedError if not allowed_student? }
    end
  end

  def checker
    fail Pundit::NotAuthorizedError if not allowed_student?

    if @profile.student?
      @count = UserSchedule.new(@profile, params[:timezone], { start: params[:start] }, params[:status]).for_student({ count: true })

      if Schedule::MAX_SCHEDULES_PER_TWO_WEEKS > @count
        @error_message = t('models.schedule.errors.insuficient_schedule', insuficient_schedule_total: (Schedule::MAX_SCHEDULES_PER_TWO_WEEKS - @count).to_s )
        flash[:error] = @error_message
      end
    end
  end

  def new
    fail Pundit::NotAuthorizedError if not allowed_student?

    datetime = DateTimeHelper.parse_from_string(params[:date])
    @schedule = Schedule.new
    @schedule.start_date = datetime[:date]
    @schedule.end_date = datetime[:date]
    @schedule.start_time = datetime[:time]
    @schedule.end_time = datetime[:time]
  end

  def create
    fail Pundit::NotAuthorizedError if not allowed_student?

    @schedule = Schedule.new(schedule_params)
    @schedule.student = @profile.student
    if @schedule.valid?
      @schedule.save
    else
      flash.now[:error] = @schedule.errors.full_messages.first
    end
  end

  def edit
    @appointment = @schedule.appointment
  end

  def update
    @schedule.assign_attributes(schedule_params)
    if @schedule.valid?
      @schedule.save
    else
      flash.now[:error] = @schedule.errors.full_messages.first
    end
  end

  def destroy
    flash.now[:error] = @schedule.errors.full_messages.first unless @schedule.destroy
  end

  def close
    @schedule.close!
    if @schedule.errors.any?
      flash.now[:error] = @schedule.errors.full_messages.first
    end
  end

  private

  def authorize_resource!
    authorize @schedule.present? ? @schedule : :schedule
  end

  def schedule_params
    params.require(:schedule).permit(Schedule.available_params)
  end

  def set_schedule
    @schedule = Schedule.find(params[:id])
  end
end
