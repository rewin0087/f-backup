# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text(65535)
#  original_filename :text(65535)
#  filename          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class DocumentsController < ApplicationController
  before_filter :set_profile, only: [:create, :index]
  before_filter :set_document, only: [:destroy]
  before_filter :authorize_resource!

  def index
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    @documents = @profile.mentor.documents.page(params[:page])
  end

  def create
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    mentor = @profile.mentor
    mentor.documents.new(sanitize_params.merge(filename: document, original_filename: document.original_filename))
    mentor.valid?
    mentor.save
    @documents = params[:scheme].present? && params[:scheme] == 'true' ? mentor.documents.recent_files(1) : mentor.documents.recent_files(nil)
  end

  def destroy
    @document.destroy
  end

  private

  def set_document
    @document = Document.find params[:id]
  end

  def authorize_resource!
    authorize @document.present? ? @document : :document
  end

  def sanitize_params
    params.require(:document).permit(document_params)
  end

  def document_params
    Document.available_params
  end

  def document
    params.require(:files)
  end
end
