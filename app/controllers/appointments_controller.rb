# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AppointmentsController < ApplicationController
  before_filter :set_profile
  before_filter :set_appointment, only: [:show, :edit, :destroy, :decline, :confirm, :cancel]
  before_filter :authorize_resource!

  def index
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    respond_to do |format|
      format.json do
        @appointments = UserSchedule.new(@profile, params[:timezone], { start: params[:start], end: params[:end] }).for_mentor
        render json: @appointments
      end
      format.html
    end
  end

  def new
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    @task = Task.find(params[:task])
    @mentor = Mentor.find(params[:mentor])

    if params[:status] == Schedule.available
      @schedule = Schedule.find(params[:schedule])
      @appointment = Appointment.new
      @appointment.schedule = @schedule
      @appointment.task = @task
      @appointment.mentor = @mentor
      @appointment.timezone = @mentor.user.timezone
    elsif params[:request_schedule]
      datetime = DateTimeHelper.parse_from_string(params[:date])
      @student = Student.find(params[:student])

      @schedule = Schedule.new
      @schedule.student = @student
      @schedule.timezone = @mentor.user.timezone
      @schedule.status = Schedule.request_schedule
      @schedule.start_date = datetime[:date]
      @schedule.end_date = datetime[:date]
      @schedule.start_time = datetime[:time]

      @appointment = Appointment.new
      @appointment.schedule = @schedule
      @appointment.task = @task
      @appointment.mentor = @mentor
      @appointment.timezone = @mentor.user.timezone
    else
      @appointment = Appointment.find(params[:schedule])
      @schedule = @appointment.schedule
    end
  end

  def create
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    @appointment = Appointment.new(appointment_params)

    if @appointment.valid?
      @appointment.save
      @appointment.schedule.waiting_for_confirmation!
    else
      flash.now[:error] = @appointment.errors.full_messages.first
    end
  end

  def request_schedule
    fail Pundit::NotAuthorizedError if not allowed_mentor?

    @appointment = Appointment.new(appointment_params)
    @appointment.build_schedule(schedule_params)

    if @appointment.valid?
      @appointment.schedule.save
      @appointment.save
      # send email here and notifcation
      AppointmentMailer.delay.appointment_request_notification_to_mentor(@appointment.id)
      AppointmentMailer.delay.appointment_request_notification_to_student(@appointment.id)
      WebClientNotification.deliver(@appointment, Schedule::NOTIFICATION_TEMPLATE[:REQUEST_SCHEDULE], @appointment.student.user, t('views.tasks.view_task_label'), profile_task_path(@appointment.student.user, @appointment.task))
    else
      flash.now[:error] = @appointment.errors.full_messages.first
    end
  end

  def show
    @schedule = @appointment.schedule
  end

  def decline
    @appointment.decline!
    if @appointment.errors.any?
      flash.now[:error] = @appointment.errors.full_messages.first
    end
  end

  def confirm
    @appointment.confirm!
    if @appointment.errors.any?
      flash.now[:error] = @appointment.errors.full_messages.first
    end
  end

  def cancel
    @appointment.cancel!
    if @appointment.errors.any?
      flash.now[:error] = @appointment.errors.full_messages.first
    end
  end

  private

  def authorize_resource!
    authorize @appointment.present? ? @appointment : :appointment
  end

  def appointment_params
    params.require(:appointment).permit(Appointment.available_params)
  end

  def schedule_params
    params.require(:schedule).permit(Schedule.available_params)
  end

  def set_appointment
    @appointment = Appointment.find(params[:id])
  end
end
