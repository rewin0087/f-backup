class DeviseExtentions::StudentController < Devise::RegistrationsController
  include ScholasticVariables
  include StudentParams

  before_filter :set_degrees, only: [:new, :create]
  before_filter :set_services, only: [:new, :create]

  def new
    student = Student.new
    student.schools  << School.new
    student.alternative_contacts << AlternativeContact.new
    build_resource({ student: student })
    set_minimum_password_length
    yield resource if block_given?
    respond_with self.resource
  end

  def create
    build_resource(sanitize_params)
    resource.role = User.student
    resource.student = Student.new(student_permitted_params(params))
    resource.student.schools.build(school_permitted_params(params))
    resource.student.alternative_contacts.build(alternative_contact_permitted_params(params))
    resource.valid?
    resource.save

    if resource.persisted?
      # sends email to the student
      AuthenticationMailer.delay.template_for_student(resource.id)
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up_as_a_student if is_flashing_format?
        sign_up(resource_name, resource)
        redirect_to after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        redirect_to after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      resource.set_student_virtual_attributes
      render :new
    end
  end

  private

  def sanitize_params
    permitted = params.require(:user).permit(user_params + student_params + school_params + alternative_contact_params)
    permitted[:service_ids] = permitted[:service_ids].reject(&:empty?)
    permitted
  end

  def user_params
    User.available_params
  end

  def student_params
    Student.available_params
  end

  def school_params
    School.available_params
  end

  def alternative_contact_params
    AlternativeContact.available_params
  end
end