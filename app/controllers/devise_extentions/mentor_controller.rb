class DeviseExtentions::MentorController < Devise::RegistrationsController
  include ScholasticVariables
  include MentorParams
  before_filter :set_degrees, only: [:new, :create]
  before_filter :set_services, only: [:new, :create]

  def new
    mentor = Mentor.new
    mentor.schools  << School.new
    mentor.alternative_contacts << AlternativeContact.new
    build_resource({ mentor: mentor })
    set_minimum_password_length
    yield resource if block_given?
    respond_with self.resource
  end

  def create
    build_resource(sanitize_params)
    resource.role = User.mentor
    resource.mentor = Mentor.new(mentor_permitted_params(params))
    resource.mentor.schools.build(school_permitted_params(params))
    resource.mentor.alternative_contacts.build(alternative_contact_permitted_params(params))
    resource.valid?
    resource.save

    if resource.persisted?
      # sends email to the mentor
      AuthenticationMailer.delay.template_for_mentor(resource.id)
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up_as_a_mentor if is_flashing_format?
        sign_up(resource_name, resource)
        redirect_to after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        redirect_to after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      resource.set_mentor_virtual_attributes
      render :new
    end
  end

  private

  def sanitize_params
    permitted = params.require(:user).permit(user_params + mentor_params + school_params + alternative_contact_params)
    permitted[:service_ids] = permitted[:service_ids].reject(&:empty?)
    permitted
  end

  def user_params
    User.available_params
  end

  def mentor_params
    Mentor.available_params
  end

  def school_params
    School.available_params
  end

  def alternative_contact_params
    AlternativeContact.available_params
  end
end
