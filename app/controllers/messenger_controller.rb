class MessengerController < ApplicationController
  before_filter :set_profile
  before_filter :set_messenger
  before_filter :authorize_resource!

  def video_call
    load_messages
    close_concurrent_opened_browser
    notify_video_call_participants
  end

  def decline_video_call
    decline_video_call_notification

    respond_to do |format|
      format.js
    end
  end

  def show
    if params[:video] == '1'
      flash[:info] = 'Only one browser | browser tab can have a video call for each task messenger session.'
    end

    load_messages
  end

  def message
    @message = Message.new(sanitize_params)
    @message.valid?
    @message.save

    if @message.errors.empty?
      Fiber.new do
        WebsocketRails[:messenger].trigger 'new', {
          message: {
            id: @message.id,
            content: render_to_string(@message),
            object: @message.attributes,
            messenger_id: @message.messenger.slug,
            user_id: @message.user.id
          }
        }
      end.resume
    end
  end

  private

  def authorize_resource!
    authorize @messenger.present? ? @messenger : :messenger
  end

  def close_concurrent_opened_browser
    Fiber.new do
      WebsocketRails[:messenger].trigger 'initialize_video_call', {
        messenger: {
          id: @messenger.id,
          messenger_id: @messenger.slug,
          user_id: @profile.id
        }
      }
    end.resume
  end

  def decline_video_call_notification
    if params[:from_user]
      from_user = User.find params[:from_user]
      Fiber.new do
        WebsocketRails[:messenger].trigger 'decline_video_call', {
          messenger: {
            id: @messenger.id,
            content: render_to_string('_request_video_call_modal', locals: { user: from_user, messenger: @messenger, from_user: @profile }, layout: false),
            object: @messenger.attributes,
            messenger_id: @messenger.slug,
            user_id: from_user.id
          }
        }
      end.resume
    end
  end

  def notify_video_call_participants
    user_participants = @messenger.messenger_participants.map(&:user)
    user_participants.reject!{ |user| user.id == @profile.id }

    user_participants.each do |user|
      Fiber.new do
        WebsocketRails[:messenger].trigger 'video_call', {
          messenger: {
            id: @messenger.id,
            content: render_to_string('_video_call_notification_modal', locals: { user: user, messenger: @messenger, from_user: @profile }, layout: false),
            object: @messenger.attributes,
            messenger_id: @messenger.slug,
            user_id: user.id
          }
        }
      end.resume
    end
  end

  def set_messenger
    @messenger = Messenger.find(params[:messenger_id] || params[:id])
  end

  def sanitize_params
    params.require(:message).permit([:messenger_id, :user_id, :text])
  end

  def load_messages
    @page = params[:page].nil? ? 1 : params[:page].to_i
    @messages = @messenger.messages.page(@page)
    @messages = @messages.reverse

    if params[:auto] == '1'
      flash[:info] = 'Your session has started. You can start the chat session.'
    end

    respond_to do |format|
      format.html do
        @message = @messenger.messages.new
        @message.user = @profile
      end
      format.js
    end
  end
end
