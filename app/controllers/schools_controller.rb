# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  university      :text(65535)
#  major           :text(65535)
#  scholastic_id   :integer
#  scholastic_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class SchoolsController < ApplicationController
  before_filter :set_profile
  before_filter :set_school, only: [:update, :destroy]
  before_filter :authorize_resource!

  def create
    fail Pundit::NotAuthorizedError if not allowed_user?

    @school = School.new(sanitize_params.merge(params_current_user))
    @school.valid?
    @school.save
  end

  def update
    @school.assign_attributes(sanitize_params)
    @school.valid?
    @school.save
  end

  def destroy
    @school.destroy
  end

  private

  def authorize_resource!
    authorize @school.present? ? @school : :school
  end

  def set_school
    @school = School.find params[:id]
  end

  def sanitize_params
    params.require(:school).permit(school_params)
  end

  def school_params
    School.available_params
  end

  def params_current_user
    scholastic_id = @profile.mentor? ? @profile.mentor.id : @profile.student.id
    {
      scholastic_id: scholastic_id,
      scholastic_type: @profile.role
    }
  end
end
