module PaypalConfigurations
  extend ActiveSupport::Concern
  require 'paypal-sdk-rest'
  include PayPal::SDK::REST
end