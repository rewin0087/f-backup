module StudentParams
  def student_permitted_params(params)
    permitted = params.require(:user).permit(Student.available_params)
    permitted[:target_countries].reject!(&:empty?) if permitted[:target_countries]
    permitted[:target_degree].reject!(&:empty?) if permitted[:target_degree]
    permitted
  end

  def school_permitted_params(params)
    permitted = params.require(:user).permit(:student_attributes => { :schools_attributes => School.available_params })
    permitted = permitted[:student_attributes][:schools_attributes]
    permitted
  end

  def alternative_contact_permitted_params(params)
    permitted = params.require(:user).permit(:student_attributes => { :alternative_contacts_attributes => AlternativeContact.available_params })
    permitted = permitted[:student_attributes][:alternative_contacts_attributes]
    permitted
  end
end