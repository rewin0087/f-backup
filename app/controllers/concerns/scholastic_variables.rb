module ScholasticVariables
  extend ActiveSupport::Concern

  def set_countries
    @countries ||= Wuniversity::Data::COUNTRIES.sort
  end

  def set_majors
    @majors ||= UniversityMajorSubjects::MAJORS.map{ |subject| subject[:major] }
  end

  def set_degrees
    @degrees ||= User::DEGREE_NAMES
  end

  def set_services
    @services ||= Service.all.map{ |e| ["#{e.title}: #{e.description}", e.id] }
  end

  def set_search_tags
    @search_tags ||= (@majors + @degrees + @services.map{ |s| s.first } + @countries).sort
  end

  def set_languages
    @languages ||= LanguageList::ALL_LANGUAGES.map(&:name).sort
  end

  def send_to_js
    gon.degrees = set_degrees
    gon.services = set_services
  end
end