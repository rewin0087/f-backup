module MentorParams
  def mentor_permitted_params(params)
    valid_mentor_params = Mentor.available_params - [:terms_of_use, :reply_speed]
    permitted = params.require(:user).permit(valid_mentor_params)
    permitted[:degree].reject!(&:empty?) if permitted[:degree]
    permitted[:essay_services].reject!(&:empty?) if permitted[:essay_services]
    permitted[:undergraduate_universities].reject!(&:empty?) if permitted[:undergraduate_universities]
    permitted[:graduate_universities].reject!(&:empty?) if permitted[:graduate_universities]
    permitted
  end

  def school_permitted_params(params)
    permitted = params.require(:user).permit(:mentor_attributes => { :schools_attributes => School.available_params })
    permitted = permitted[:mentor_attributes][:schools_attributes]
    permitted
  end

  def alternative_contact_permitted_params(params)
    permitted = params.require(:user).permit(:mentor_attributes => { :alternative_contacts_attributes => AlternativeContact.available_params })
    permitted = permitted[:mentor_attributes][:alternative_contacts_attributes]
    permitted
  end
end