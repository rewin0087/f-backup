class ScholasticsController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:universities, :languages, :countries, :majors, :timezones, :search_tags]
  respond_to :js, :json

  def index
  end

  def universities
    keyword = params[:q].to_s
    @universities = Wuniversity::Data::ALL.select { |u| u.downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u, text: u }} unless keyword.blank?
    respond_with @universities || []
  end

  def languages
    keyword = params[:q].to_s
    @languages = LanguageList::ALL_LANGUAGES.map(&:name).sort.select{ |u| u.downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u, text: u } } unless keyword.blank?
    respond_with @languages || []
  end

  def countries
    keyword = params[:q].to_s
    @countries ||= Wuniversity::Data::COUNTRIES.sort.select{ |u| u.downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u, text: u } } unless keyword.blank?
    respond_with @countries || []
  end

  def majors
    keyword = params[:q].to_s
    @majors ||= UniversityMajorSubjects::MAJORS.map{ |subject| subject[:major] }.sort.select{ |u| u.downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u, text: u } } unless keyword.blank?
    respond_with @majors || []
  end

  def timezones
    keyword = params[:q].to_s
    @timezones ||= User::TIMEZONES.sort.select{ |u| u[0].downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u[1], text: u[0] } } unless keyword.blank?
    respond_with @timezones || []
  end

  def search_tags
    keyword = params[:q].to_s
    @tags ||= tags.select{ |u| u.downcase.include?(keyword.downcase) || u.include?(keyword) }.take(30).map{|u| { id: u, text: u } } unless keyword.blank?
    respond_with (@tags || []) << keyword
  end

  def tags
    @majors ||= UniversityMajorSubjects::MAJORS.map{ |subject| subject[:major] }
    @degrees ||= User::DEGREE_NAMES
    @services ||= Service.all.map(&:title)
    @countries ||= Wuniversity::Data::COUNTRIES
    @universities = Wuniversity::Data::ALL
    @tags ||= (@majors + @degrees + @services + @countries + @universities).sort
  end
end
