# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

class MentorsController < ApplicationController
  include ScholasticVariables
  include MentorParams

  skip_before_filter :authenticate_user!, only: [:index, :search]
  before_filter :send_to_js, only: [:search, :index]
  before_filter :send_params_to_js, only: [:search, :index]

  before_filter :set_degrees, only: [:account_settings]
  before_filter :set_services, only: [:account_settings]
  before_filter :set_profile, only: [:update, :account_settings]

  def index
    fail Pundit::NotAuthorizedError if user_signed_in? && !student_user_only?

    @top_mentors = Mentor.top_nachers
    @top_suggested = current_user.present? && current_user.student? ? ::MatchMentors.find_for(current_user).matches : []
    @search_tags = []
    set_layout
  end

  def search
    fail Pundit::NotAuthorizedError if user_signed_in? && !student_user_only?

    @keyword = params[:keyword] || []
    @mentors = []
    @search_tags = @keyword || []
    @country = params[:country] || []
    @majors = params[:majors] || []

    if @keyword.blank?
      @mentors = Mentor.top_nachers(params[:page])
    else
      @mentors = Mentor.filter_search(@search_tags, params[:page], advance_filter_params)
    end

    gon.services = @services.map{ |s| { id: s[1], text: s[0] } }
    set_layout
  end

  def update
    fail Pundit::NotAuthorizedError if not allowed_mentor?
    update_mentor
    @documents = @profile.mentor.documents
  end

  def account_settings
    fail Pundit::NotAuthorizedError if not allowed_mentor?
    update_mentor
  end

  def notify_student
    fail Pundit::NotAuthorizedError if not mentor_user_only?
    NotifyMailer.delay.notification_for_student(params[:id], current_user.id)
  end

  protected

  def update_mentor
    @profile.assign_attributes(sanitize_params)
    @profile.mentor.assign_attributes(mentor_permitted_params(params))
    @profile.valid?
    @profile.slug = nil
    @profile.save
    @profile.set_mentor_virtual_attributes
  end

  def set_layout
    if current_user.blank? || current_user.administrator?
      render :layout => 'branding'
    end
  end

  def send_params_to_js
    query = params
    query.delete(:controller)
    query.delete(:action)
    gon.params = query || {}
  end

  def advance_filter_params
    {
      majors: params[:majors],
      degrees: params[:degrees],
      current_country: params[:country],
      services: params[:services],
      ratings: params[:ratings]
    }
  end

  def sanitize_params
    params.require(:user).permit(user_params + mentor_params)
  end

  def user_params
    User.available_params
  end

  def mentor_params
    Mentor.available_params
  end
end
