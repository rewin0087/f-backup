# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  description             :text(65535)
#  requested_file          :string(255)
#  revised_file            :string(255)
#  status                  :string(255)      default("CREATED")
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  task_type               :string(255)      default("PAYED")
#  service_id              :integer
#  order_id                :integer
#  mentor_id               :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

class TasksController < ApplicationController
  before_filter :set_profile
  before_filter :set_task
  before_filter :authorize_resource!

  def show
    @task.clear_all_notifications(current_user)
  end

  def update
    @task.assign_attributes(task_params)
    @task.save
  end

  def upload_request_file
    unless params[:modal]
      @task.assign_attributes(requested_file_params)
      @task.save
      # send email here and notification
      TaskMailer.delay.upload_request_file_notification_to_mentor(@task.id)
      WebClientNotification.deliver(@task, Task::NOTIFICATION_TEMPLATE[:REQUEST_FILE_UPLOADED], @task.mentor.user, t('views.tasks.view_task_label'), profile_task_path(@task.mentor.user, @task))
    end
  end

  def upload_revised_file
    unless params[:modal]
      @task.assign_attributes(revised_file_params)
      @task.save
      # send email here and notification
      TaskMailer.delay.upload_revised_file_notification_to_student(@task.id)
      WebClientNotification.deliver(@task, Task::NOTIFICATION_TEMPLATE[:REVISED_FILE_UPLOADED], @task.student.user, t('views.tasks.view_task_label'), profile_task_path(@task.student.user, @task))
    end
  end

  def in_progress
    @task.in_progress!(params[:revert])
  end

  def done
    @task.done!
  end

  def completed
    @task.completed!
  end

  def instruction
    @task.assign_attributes(task_params)
    @task.save
    # send email here and notification
    TaskMailer.delay.additional_instruction_notification_to_mentor(@task.id)
    WebClientNotification.deliver(@task, Task::NOTIFICATION_TEMPLATE[:ADDITIONAL_INSTRUCTIONS], @task.mentor.user, t('views.tasks.view_task_label'), profile_task_path(@task.mentor.user, @task))
  end

  def comment
    @task.assign_attributes(task_params)
    @task.save
    # send email here and notification
    TaskMailer.delay.encouragement_message_notification_to_student(@task.id)
    WebClientNotification.deliver(@task, Task::NOTIFICATION_TEMPLATE[:ADDITIONAL_COMMENTS], @task.student.user, t('views.tasks.view_task_label'), profile_task_path(@task.student.user, @task))
  end

  def appointment
  end

  protected

  def authorize_resource!
    authorize @task.present? ? @task : :task
  end

  def task_params
    params.require(:task).permit(Task.available_params)
  end

  def set_task
    @task = Task.find(params[:id])
  end

  def requested_file_params
    params.require(:task).permit(:requested_file)
  end

  def revised_file_params
    params.require(:task).permit(:revised_file)
  end
end
