# == Schema Information
#
# Table name: recommendations
#
#  id         :integer          not null, primary key
#  message    :text(65535)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RecommendationsController < ApplicationController
  before_filter :authorize_resource!

  def create
    @recommendation = Recommendation.new(sanitize_params)
    @recommendation.valid?
    if @recommendation.save
      RecommendationMailer.delay.new_recommendation_notification_to_admins(@recommendation)
    end
  end

  private

  def authorize_resource!
    authorize :recommendation
  end

  def sanitize_params
    params.require(:recommendation).permit(recommendation_params)
  end

  def recommendation_params
    Recommendation.available_params
  end
end
