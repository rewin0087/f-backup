class LandingController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]
  before_filter :render_404_if_user_signed_in
  layout 'branding'

  def index
    @top_mentors = Mentor.top_nachers(1, 6)
  end

  private

  def render_404_if_user_signed_in
    if user_signed_in?
      render_404
    end
  end
end
