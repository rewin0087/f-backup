class ApplicationController < ActionController::Base
  include ::Pundit
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :register_not_allowed!
  before_filter :set_admin_notice
  before_filter :authenticate_user!, unless: :user_signed_in?
  before_filter :redirect_to_dashboard_if_user_signed_in
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from Pundit::NotAuthorizedError, with: :render_404

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username, :password, :remember_me) }
  end

  def after_sign_up_path_for(resource)
    dashboard_index_path
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def current_user
    user = super
    user ? user : current_admin_user
  end

  protected

  def set_admin_notice
    @notice = 'You are logged in as an Administrator. Changing any information is not recommended to do.' if current_user && current_user.administrator?
  end

  def set_profile
    @profile = User.find(params[:profile_id]) if params[:profile_id]
  end

  def authorize_resource!
  end

  def redirect_to_dashboard_if_user_signed_in
    return redirect_to dashboard_index_path if user_signed_in? and request.path == root_path
  end

  def register_not_allowed!
    return render_404 if request.path == new_user_registration_path
  end

  def set_timezones
    @timezones ||= User::TIMEZONES
  end

  def render_404
    fail(ActionController::RoutingError.new('Not Found'))
  end

  def student_user_only?
    (current_user.administrator? || current_user.student?)
  end

  def mentor_user_only?
    (current_user.administrator? || current_user.mentor?)
  end

  def allowed_mentor?
    current_user.administrator? || (current_user == @profile && current_user.mentor?)
  end

  def allowed_student?
    current_user.administrator? || (current_user == @profile && current_user.student?)
  end

  def allowed_user?
    current_user.administrator? || (current_user == @profile)
  end
end
