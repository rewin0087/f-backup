class DashboardController < ApplicationController
  before_filter :set_profile
  before_filter :check_current_user, only: [:index]

  def index
    if current_user.administrator? && @profile.nil?
      @top_mentors = Mentor.top_nachers(1, 6)
      render 'landing/index', layout: 'branding'
    end
  end

  private

  def check_current_user
    if (@profile.present? && @profile.student?) || current_user.student?
      @mentors = ::MatchMentors.find_for(@profile || current_user).matches(params[:page])
    elsif (@profile.present? && @profile.mentor?) || current_user.mentor?
      @students = ::MatchStudents.find_for(@profile || current_user).matches(params[:page])
    end
  end
end
