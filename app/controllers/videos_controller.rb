# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  url            :text(65535)
#  source         :text(65535)
#  videoable_id   :integer
#  videoable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class VideosController < ApplicationController
  before_filter :set_profile
  before_filter :set_video, only: [:destroy]
  before_filter :authorize_resource!

  def create
    fail Pundit::NotAuthorizedError if not allowed_mentor?
    @video = Video.new(video_params)

    if @video.valid? && @video.parse_url!
      @video.save
    end
  end

  def destroy
    @video.destroy
  end

  protected

  def authorize_resource!
    authorize @video.present? ? @video : :video
  end

  def set_video
    @video = Video.find(params[:id])
  end

  def video_params
    params.require(:video).permit([:url, :videoable_id, :videoable_type])
  end
end
