class ProfileController < ApplicationController
  include ScholasticVariables
  before_filter :set_profile
  before_filter :profile_assign_attributes, only: [:edit, :account_settings]
  before_filter :set_countries, only: [:account_settings]
  before_filter :set_majors, only: [:account_settings]
  before_filter :set_degrees, only: [:account_settings]
  before_filter :set_services, only: [:account_settings]
  before_filter :authorize_resource!
  before_filter :get_documents, only: [:edit, :show]

  def show
    student_bookmark!
  end

  def edit
    @images = @profile.recent_images
  end

  # Update Password Only
  def update
    if @profile.valid_password? security_params[:current_password]
      if security_params[:password].include?(security_params[:current_password])
        @profile.errors[:base] << t('models.user.errors.new_password.unique')
      else
        @profile.reset_password!(security_params[:password], security_params[:password_confirmation])
        sign_in(@profile, bypass: true) if @profile.errors.empty?
      end
    else
      @profile.errors[:base] << t('models.user.errors.password.invalid')
    end
  end

  def primary_photo
    @profile.assign_attributes(primary_photo_upload_params)
    @profile.add_primary_photo_to_images
    @profile.save
    @images = @profile.recent_images
  end

  def students
    @students = @profile.my_students(params[:page])
  end

  def mentors
    @mentors = @profile.my_mentors(params[:page])
  end

  def account_settings
  end

  def private_message

    if params[:recipient]
      @recipient = params[:recipient]
    else
      if params[:private_message]
        NotifyMailer.delay.private_message(@profile.id, params[:to], params[:private_message])
      end
    end
  end

  private

  def authorize_resource!
    authorize @profile.present? ? @profile : :user
  end

  def security_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end

  def primary_photo_upload_params
    params.require(:user).permit(:primary_photo)
  end

  def profile_assign_attributes
    @profile.assign_virtual_attributes
  end

  def set_profile
    @profile = User.find params[:id]
    set_student_favorites_profile if @profile.student?
  end

  def student_bookmark!
    if @profile.mentor? && current_user.student?
      @bookmark = current_user.student.bookmarks.find_by(mentor: @profile.mentor)
    end
  end

  def set_student_favorites_profile
    @favorites = @profile.student.favorite_mentors.page
  end

  def get_documents
    @documents = @profile.mentor.recent_documents if @profile.mentor?
  end
end
