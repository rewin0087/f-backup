# == Schema Information
#
# Table name: transactions
#
#  id                  :integer          not null, primary key
#  transaction_type    :string(255)
#  payment_gateway     :string(255)
#  amount              :decimal(10, )
#  notification_params :text(65535)
#  status              :string(255)
#  transaction_id      :string(255)
#  purchased_at        :datetime
#  wallet_id           :integer
#

class TransactionsController < ApplicationController
  protect_from_forgery except: [:hook]
  before_filter :sanitize_params, only: [:create]
  before_filter :set_profile, only: [:index, :create, :hook]
  skip_before_filter :authenticate_user!, only: [:hook]

  def index
    fail Pundit::NotAuthorizedError if not allowed_user?

    @transactions = if @profile.student?
      ::Transaction.student_payment_history(@profile, params[:page])
    elsif @profile.mentor?
      ::Transaction.mentor_payment_history(@profile, params[:page])
    else
      []
    end

    @presenter_transactions = @transactions.map { | transaction | Presenter::Transaction.new(transaction, @profile).data }
  end

  def create
    @payment = if sanitize_params[:payment_gateway] == ::Transaction::PAYMENT_GATEWAY[:cc] || sanitize_params[:payment_gateway] == ::Transaction::PAYMENT_GATEWAY[:paypal]
      Paypal.new.pay_by_paypal(initialize_transaction)
    elsif sanitize_params[:payment_gateway] == ::Transaction::PAYMENT_GATEWAY[:alipay]
      AlipayPayment.pay_by_alipay(initialize_transaction)
    end

    render json: @payment.is_a?(String) ? { url: @payment } : @payment
  end

  def initialize_transaction
    ::Transaction.new(sanitize_params.merge({
      wallet_id: @profile.wallet.id,
      transaction_type: ::Transaction::TYPE[:debit]
    }))
  end

  def hook
    # notification_url
    params.permit! # Permit all Paypal input params
    status = params[:payment_status] || params[:trade_status]
    if status == ::Transaction::PAYPAL_STATE[:completed]
      transaction  = ::Transaction.find params[:id]
      transaction.update_attributes notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now
      render nothing: true
    elsif status == ::Transaction::ALIPAY_STATE[:trade_finished]
      transaction  = ::Transaction.find params[:id]
      transaction.update_attributes notification_params: params, status: status, transaction_id: params[:notify_id], purchased_at: Time.now
      redirect_to profile_wallets_path(@profile)
    end
  end

  private

  def sanitize_params
    params[:transaction][:amount] ||= params[:transaction][:another_amount]
    params.require(:transaction).permit(transaction_params)
  end

  def transaction_params
    ::Transaction.available_params
  end
end
