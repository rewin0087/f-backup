class ServicesController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]
  layout 'branding'

  def index

  end
end
