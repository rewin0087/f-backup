class BookmarkController < ApplicationController
  before_filter :set_mentor, only: [:create, :destroy]
  before_filter :set_student, only: [:create, :destroy]
  before_filter :set_bookmark, only: [:destroy]
  before_filter :authorize_resource!

  def create
    if @student
      @bookmark = Bookmark.new(mentor: @mentor, student: @student)
      @bookmark.save
    end
  end

  def destroy
    @bookmark.destroy
  end

  private

  def authorize_resource!
    authorize @bookmark.present? ? @bookmark : :bookmark
  end

  def set_bookmark
    @bookmark = Bookmark.find_by(mentor: @mentor, student: @student, id: params[:id])
  end

  def set_mentor
    @mentor = Mentor.find params[:mentor_id]
  end

  def set_student
    @student = current_user.student
  end
end