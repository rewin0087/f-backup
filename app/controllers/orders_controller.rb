# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

class OrdersController < ApplicationController
  before_filter :set_profile
  before_filter :set_order, except: [:index, :new, :create]
  before_filter :authorize_resoure!

  def index
    fail Pundit::NotAuthorizedError if not allowed_user?

    if @profile.student?
      @orders = @profile.student.orders.page(params[:page]).per(10)
    elsif @profile.mentor?
      @orders = @profile.mentor.orders.page(params[:page]).per(10)
    else
      @orders = []
    end
  end

  def new
    fail Pundit::NotAuthorizedError if not allowed_student?

    mentor_id = params[:mentor]
    @mentor_user = User.find(mentor_id)
    @order = Order.new
    @order.mentor = @mentor_user.mentor
    @order.student = @profile.student
  end

  def create
    fail Pundit::NotAuthorizedError if not allowed_student?

    @order = Order.new(order_params)
    @order.tasks = build_tasks(@order.mentor)
    @order.compute_price_and_set_currency
    @order.valid?

    if @order.save
      @order.transactions.create(
        transaction_type: Transaction::TYPE[:credit],
        amount: @order.actual_price,
        status: @order.status
      )
      OrderMailer.delay.pending_notification_to_mentor(@order.id)
      WebClientNotification.deliver(@order, Order::NOTIFICATION_TEMPLATE[:PENDING], @order.mentor.user, t('views.orders.view_order_label'), profile_order_path(@order.mentor.user, @order))
      flash[:success] = t('views.orders.create.flash.success', mentor_full_name: @order.mentor.user.full_name)
    else
      @mentor_user = @order.mentor.user
      flash.now[:error] = @order.errors.full_messages.first
    end
  end

  def show
    @order = Order.find(params[:id])
    @order.clear_notifications(current_user)
  end

  def instruction
    @order.assign_attributes(order_params)
    @order.save
    # send email here and notification
    OrderMailer.delay.additional_instruction_notification_to_mentor(@order.id)
    WebClientNotification.deliver(@order, Order::NOTIFICATION_TEMPLATE[:ADDITIONAL_INSTRUCTIONS], @order.mentor.user, t('views.orders.view_order_label'), profile_order_path(@order.mentor.user, @order))
  end

  def comment
    @order.assign_attributes(order_params)
    @order.save
    # send email here and notification
    OrderMailer.delay.encouragement_message_notification_to_student(@order.id)
    WebClientNotification.deliver(@order, Order::NOTIFICATION_TEMPLATE[:ADDITIONAL_COMMENTS], @order.student.user, t('views.orders.view_order_label'), profile_order_path(@order.student.user, @order))
  end

  def accept
    @order.accept!
  end

  def confirm
    @order.confirm!
  end

  def in_progress
    @order.in_progress!
  end

  def completed
    @order.completed!
  end

  def decline
    @order.decline!
  end

  def cancel
    @order.cancel!
  end

  def close
    @order.close!
  end

  private

  def authorize_resoure!
    authorize @order.present? ? @order : :order
  end

  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.require(:order).permit(Order.available_params)
  end

  def build_tasks(mentor)
    services = Service.find task_params[:tasks][:service_id]
    tasks = services.map do |service|
      Task.new(
        price: service.price,
        actual_price: service.price,
        currency: service.currency,
        currency_symbol: service.currency_symbol,
        service_id: service.id,
        mentor_id: mentor.id
      )
    end

    tasks
  end

  def task_params
    permitted = params.require(:order).permit(:tasks => { :service_id => [] })
    permitted[:tasks][:service_id] = permitted[:tasks][:service_id].reject(&:empty?).uniq
    permitted
  end
end
