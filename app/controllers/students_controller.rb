# == Schema Information
#
# Table name: students
#
#  id                  :integer          not null, primary key
#  target_countries    :text(65535)
#  target_degree       :text(65535)
#  looking_for         :text(65535)
#  questions_to_mentor :text(65535)
#  remarks             :text(65535)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class StudentsController < ApplicationController
  include ScholasticVariables
  include StudentParams

  skip_before_filter :authenticate_user!, only: [:index, :search]
  before_filter :set_degrees, only: [:account_settings]
  before_filter :set_services, only: [:account_settings]
  before_filter :set_profile, only: [:update, :account_settings]

  def index
    fail Pundit::NotAuthorizedError if not mentor_user_only? if user_signed_in?

    @students = Student.all.page(params[:page])
    set_layout
  end

  def search
    fail Pundit::NotAuthorizedError if not mentor_user_only? if user_signed_in?

    set_layout
  end

  def update
    fail Pundit::NotAuthorizedError if not allowed_student?

    update_student
  end

  def account_settings
    fail Pundit::NotAuthorizedError if not allowed_student?

    update_student
  end

  protected

  def set_layout
    if current_user.blank? || current_user.administrator?
      render :layout => 'branding'
    end
  end

  def update_student
    @profile.assign_attributes(sanitize_params)
    @profile.student.assign_attributes(student_permitted_params(params))
    @profile.valid?
    @profile.slug = nil
    @profile.save
    @profile.set_student_virtual_attributes
  end

  def sanitize_params
    params.require(:user).permit(user_params + student_params)
  end

  def user_params
    User.available_params
  end

  def student_params
    Student.available_params
  end
end
