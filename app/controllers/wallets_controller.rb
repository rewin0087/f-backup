# == Schema Information
#
# Table name: wallets
#
#  id      :integer          not null, primary key
#  amount  :decimal(10, )
#  user_id :integer
#

class WalletsController < ApplicationController
  before_filter :set_profile, only: [:index]

  def index
    fail Pundit::NotAuthorizedError if not allowed_user?

    @transaction = Transaction.new

    if @profile.mentor?
      render 'mentor'
    end
  end
end
