window.taskUploadRequestFileDropzone = () ->
  taskUploadRequestFileModal = $('#task-upload-request-file-modal')

  if taskUploadRequestFileModal.length == 1
    taskUploadRequestFileModalForm = taskUploadRequestFileModal.find('form#task-upload-request-file-form').dropzone
      paramName: 'task[requested_file]'
      maxFilesize: 40
      addRemoveLinks: true
      maxFiles: 1
      uploadMultiple: false
      init: () ->
        @.on 'success', (file) ->
          @.removeFile(file)
          taskUploadRequestFileModal.modal('hide')
          eval(file.xhr.response)
        @.on 'error', (file) ->
          _m.render_error(MESSAGES.ERRORS.GENERAL)

window.taskUploadRevisedFileDropzone = () ->
  taskUploadRevisedFileModal = $('#task-upload-revised-file-modal')

  if taskUploadRevisedFileModal.length == 1
    taskUploadRevisedFileModalForm = taskUploadRevisedFileModal.find('form#task-upload-revised-file-form').dropzone
      paramName: 'task[revised_file]'
      maxFilesize: 40
      addRemoveLinks: true
      maxFiles: 1
      uploadMultiple: false
      init: () ->
        @.on 'success', (file) ->
          @.removeFile(file)
          taskUploadRevisedFileModal.modal('hide')
          eval(file.xhr.response)
        @.on 'error', (file) ->
          _m.render_error(MESSAGES.ERRORS.GENERAL)

window.renderTaskAppointmentCalendar = () ->
  taskAppointmentCalendar = $('#task-appointment-calendar')
  if taskAppointmentCalendar.length > 0
    loader(true)
    taskAppointmentCalendar.fullCalendar('destroy')
    taskAppointmentCalendar.fullCalendar
      dayClick: (date, jsEvent, view) ->
        now = new Date()
        if (+date.toDate() >= +now)
          loader(true)
          url = taskAppointmentCalendar.attr('data-mentor-appointments').replace('.json', '')
          url += '/new.js?request_schedule=1'
          url += '&task=' + taskAppointmentCalendar.attr('data-task')
          url += '&mentor=' + taskAppointmentCalendar.attr('data-mentor')
          url += '&student=' + taskAppointmentCalendar.attr('data-student')
          url += '&date=' + date.format()
          $.get url
        else
          _n.renderErrorPopup('The appointment slot is in the past. Please select slots occurring tomorrow or later.')
      eventClick: (calEvent, jsEvent, view) ->
        now = new Date()
        date = new Date(calEvent.start_date)
        if calEvent.status != 'AVAILABLE' || (date >= now)
          loader(true)
          url = taskAppointmentCalendar.attr('data-mentor-appointments').replace('.json', '')
          url += '/new.js?schedule=' + calEvent.id
          url += '&task=' + taskAppointmentCalendar.attr('data-task')
          url += '&mentor=' + taskAppointmentCalendar.attr('data-mentor')
          url += '&status=' + calEvent.status
          $.get url
        else
          _n.renderErrorPopup('The appointment slot is in the past. Please select slots occurring tomorrow or later.')
      eventAfterRender: (event, element, view) ->
        loader(false)
        element.css({background: window.eventColors[event.status]})
      header:
        left: 'prev, next title'
        center: ''
        right: 'month, agendaWeek, agendaDay'
      height: 500
      eventSources: [
        {
          url: taskAppointmentCalendar.attr('data-student-schedules') + '?status=AVAILABLE'
        },
        {
          url: taskAppointmentCalendar.attr('data-mentor-appointments')
        }
      ]
      timezone: $('select#timezone').val()
      displayEventEnd: true
      timeFormat: 'h:mma'
      eventAfterAllRender: () ->
        loader(false)
    calendarOverride()

$(document).on 'ready page:load', () ->
  if $('body').hasClass('tasks') && $('body').hasClass('appointment')
    $('.tasks select#timezone').on 'change', (e) ->
      renderTaskAppointmentCalendar()

    renderTaskAppointmentCalendar()
    $('.tasks select#timezone').select2()
