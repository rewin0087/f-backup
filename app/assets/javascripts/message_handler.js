var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.Message_Handler_Error = (function() {
  function Message_Handler_Error() {
    this.render = __bind(this.render, this);
  }

  Message_Handler_Error.prototype.template = JST['message-handler/error_box'];

  Message_Handler_Error.prototype.render = function(message) {
    return this.template({message: message});
  };

  return Message_Handler_Error;

})();

window.Message_Handler_Success = (function() {
  function Message_Handler_Success() {
    this.render = __bind(this.render, this);
  }

  Message_Handler_Success.prototype.template = JST['message-handler/success_box'];

  Message_Handler_Success.prototype.render = function(message, url) {
    return this.template({message: message, url: url});
  };

  return Message_Handler_Success;

})();

window.Message_Handler_Notification = (function() {
  function Message_Handler_Notification() {
    this.render = __bind(this.render, this);
  }

  Message_Handler_Notification.prototype.template = JST['message-handler/notification_box'];

  Message_Handler_Notification.prototype.render = function(data) {
    return this.template({message: data.message, url: data.url, title: data.title});
  };

  return Message_Handler_Notification;

})();

window.Message_Handler = (function() {
  function Message_Handler() {
    this.checkMessageCount = __bind(this.checkMessageCount, this);
    this.render_success = __bind(this.render_success, this);
    this.render_error = __bind(this.render_error, this);
    this.render = __bind(this.render, this);
  }

  Message_Handler.prototype.template = JST['message-handler/main'];

  Message_Handler.prototype.$el = null;

  Message_Handler.prototype.render = function() {
    var template = this.template();
    this.$el = $(template);
    return template;
  };

  Message_Handler.prototype.init_notification_close = function() {
    $('li.message-box .ion-close').unbind();

    $('html').on('click', 'li.message-box .ion-close', function(e) {
      e.preventDefault();

      parent = $(this).parent().parent().parent();
      parent.fadeOut();

      setTimeout(function() {
        parent.remove();
      }, 300);
    });
  };

  Message_Handler.prototype.render_error = function(message) {
    var error = new window.Message_Handler_Error();
    return this.checkMessageCount(error.render(message));
  };

  Message_Handler.prototype.render_success = function(message, url) {
    var success = new window.Message_Handler_Success();
    return this.checkMessageCount(success.render(message, url));
  };

  Message_Handler.prototype.render_notification = function(data) {
    var notification = new window.Message_Handler_Notification();
    return this.checkMessageCount(notification.render(data));
  };

  Message_Handler.prototype.checkMessageCount = function(notification) {
    var parent = '.' + this.$el.attr('class');
    var messages = $(parent + ' ul li');
    var length = messages.length;

    $(parent + ' .row ul').prepend(notification);
    this.init_notification_close();

    if (length > 5) {
      return messages[0].remove();
    }

    setTimeout(function() {
      var messages = $(parent + ' ul li')
      messages.last().remove();
    }, 100000);
  };

  return Message_Handler;

})();

window._m = new window.Message_Handler();

$(document).on('page:load ready', function() {
  if(!_m) {
    window._m = new window.Message_Handler();
  }
  $('body').append(_m.render());
});
