class window.Notifications
  errorPopupTemplate: JST['notification/error_popup']
  successPopupTemplate: JST['notification/success_popup']

  constructor: () ->
    container = $('<div id="notification-popup"></div>')
    $('body').append(container)

  renderErrorPopup: (message) ->
    popup = @errorPopupTemplate({ message: message })
    $('body').find('div#notification-popup').html(popup)
    $('div#error_popup_notification').modal('show')

  renderSuccessPopup: (message) ->
    popup = @successPopupTemplate({ message: message })
    $('body').find('div#notification-popup').html(popup)
    $('div#success_popup_notification').modal('show')

  initializeWebClientNotification: () ->
    ws.unsubscribe('notifications')
    notificationsChannel = ws.subscribe('notifications')
    notificationsChannel.bind 'new', (data) ->
      userId = parseInt $('body').attr('data-user')
      notificationCountHolder = $('#notification-count-container #count')
      if userId == data.user_id
        notificationCountHolder.html(data.pending_notification)
        _m.render_notification(data)


window._n = new Notifications()
_n.initializeWebClientNotification()

$(document).on 'page:load ready', () ->
  window._n = new Notifications()