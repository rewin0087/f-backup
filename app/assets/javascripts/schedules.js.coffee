window.eventColors = { AVAILABLE: '#8cc2cd', OCCUPIED: '#67b16b', CLOSED: '#814736', WAITING_FOR_CONFIRMATION: '#f0ad4e' }
window.calendarOverride = () ->
  $('button.fc-month-button').html('MONTH')
  $('button.fc-agendaWeek-button').html('WEEK')
  $('button.fc-agendaDay-button').html('AGENDA')

window.renderScheduleCalendar = () ->
  loader(true)
  $('#schedule-calendar').fullCalendar('destroy')
  $('#schedule-calendar').fullCalendar
    eventClick: (calEvent, jsEvent, view) ->
      now = new Date()
      date = new Date(calEvent.start_date)
      if calEvent.status != 'AVAILABLE' || (+date >= +now)
        loader(true)
        $.get '/profile/' + userId() + '/schedules/' + calEvent.id + '/edit.js?date=' + calEvent.start_date
      else
        _n.renderErrorPopup('You can\'t edit this schedule because it\'s already behind the current time.')
    dayClick: (date, jsEvent, view) ->
      now = new Date()
      if (+date.toDate() >= +now)
        loader(true)
        $.get '/profile/' + userId() + '/schedules/new?date=' + date.format()
    eventAfterRender: (event, element, view) ->
      element.css({background: window.eventColors[event.status]})
    header:
      left: 'prev, next title'
      center: ''
      right: 'month, agendaWeek, agendaDay'
    height: 500
    events: '/profile/' + userId() + '/schedules.json'
    timezone: $('select#timezone').val()
    displayEventEnd: true
    timeFormat: 'h:mma'
    eventAfterAllRender: () ->
      loader(false)
  calendarOverride()

$(document).on 'page:load ready', ->
  if $('body').hasClass('schedules')
    $('.schedules select#timezone').on 'change', (e) ->
      renderScheduleCalendar()

    renderScheduleCalendar()
  $('.schedules select#timezone').select2()