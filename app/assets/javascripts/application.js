// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require turbolinks
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap-sprockets
//= require rails-timeago
//= require websocket_rails/main
//= require moment
//= require fullcalendar
//= require select2
//= require underscore
//= require pretty_social
//= require dropzone
//= require jquery.timepicker.js
//= require simplewebrtc_v2
//= require flipclock.min
//= require_tree ../templates
//= require_tree .
Turbolinks.enableProgressBar()
Dropzone.autoDiscover = false