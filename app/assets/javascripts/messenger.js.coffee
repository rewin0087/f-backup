class window.MessengerNotification
  container: null
  containerId: null
  messengerChannel: null
  videoCall: null
  voiceCall: null

  constructor: () ->
    @container = $('.messages-container')
    @containerId = @container.attr('id')
    ws.unsubscribe('messenger')
    @messengerChannel = ws.subscribe('messenger')

  listenToMessages: () ->
    @messengerChannel.bind 'new', (data) =>
      if _this.containerId == data.message.messenger_id
        _this.container.append(data.message.content)
        setTimeout () ->
          _this.scrollToButtom()
          $('time').timeago()
        , 300

      userId = parseInt $('body').attr('data-user')
      if userId == data.message.user_id
        $('form#new_message')[0].reset()

  listenToVideoCall: () ->
    # receive video call
    @messengerChannel.bind 'video_call', (data) =>
      userId = parseInt $('body').attr('data-user')
      videoHolder = $('.video-holder').attr('id')
      @removeModal()
      if !videoHolder && userId == data.messenger.user_id
        $('.modal-backdrop').remove()
        $('.modal-holder').html(data.messenger.content)
        $('.modal-holder #video-call-notification-modal').modal('show')

    # decline video call
    @messengerChannel.bind 'decline_video_call', (data) =>
      connectionStateHolder = $('.peer-connection-state')
      userId = parseInt $('body').attr('data-user')
      videoHolder = $('.video-holder').attr('id')
      @removeModal()
      if !!videoHolder && userId == data.messenger.user_id
        $('.modal-backdrop').remove()
        connectionStateHolder.html('Call was declined.')
        $('.modal-holder').html(data.messenger.content)
        $('.modal-holder #request-video-call-notification-modal').modal('show')

    # detect | close concurrent browser
    @messengerChannel.bind 'initialize_video_call', (data) =>
      videoHolder = $('.video-holder')
      userId = parseInt $('body').attr('data-user')
      if videoHolder.length == 1 && userId == data.messenger.user_id
        videoHolderId = videoHolder.attr('id')
        if videoHolderId == "video-call-#{data.messenger.messenger_id}"
          setTimeout () ->
            window.location.href = "#{videoHolder.attr('data-referer-path')}?video=1"
          , 2000

  removeModal: () ->
    modal = $('.modal-holder .modal')
    if !!modal
      modal.modal('hide')
      setTimeout () ->
        modal.remove()
        $('.modal-backdrop').remove()
      , 800

  initializeVideoCall: () ->
    @videoCall = new VideoCall()
    @videoCall.initiate()
    @videoCall

  scrollToButtom: () ->
    if @container.length == 1
      scrollTo = $('.scroll_end').offset().top + 100000000
      $('.messages-scroll').animate({ scrollTop: scrollTo + 'px' })

  countdownTimer: () ->
    countdownTimerHolder = $('.messenger-countdown-timer')
    if countdownTimerHolder.length == 1
      remainingTime = parseInt countdownTimerHolder.attr('data-remaining-time')
      countdownTimer = countdownTimerHolder.FlipClock
        countdown: true
        autoStart: true
        callbacks:
          interval: () ->
            remaining = countdownTimer.getTime()
            if remaining < 3
              window.location.href = "#{countdownTimerHolder.attr('data-current-path')}?auto=1"

      countdownTimer.setTime(remainingTime)
      countdownTimer.start()

$(document).on 'page:load ready', () ->
  window.messenger = new MessengerNotification()
  messenger.listenToVideoCall()

  if $('body').hasClass('messenger')
    messenger.listenToMessages()
    messenger.scrollToButtom()
    messenger.listenToVideoCall()
    messenger.countdownTimer()
    $('body').attr('data-no-turbolink', '')

    if $('body').hasClass('video_call') && $('div').hasClass('video-holder')
      messenger.initializeVideoCall()