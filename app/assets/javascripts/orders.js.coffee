window.orderFormMentorSelectionHandler = () ->
  selectedMentorServices = $('.mentor_service_selection:checked')
  mentorServiceTotalPrice = $('div#total_order_price')
  orderPriceField = $('input#order_price')
  orderActualPriceField = $('input#order_actual_price')
  orderCurrencyField = $('input#order_currency')
  orderCurrencySymbolField = $('input#order_currency_symbol')
  total = 0
  currency = []
  currency_symbol = []

  selectedMentorServices.each (i,v) ->
    total += parseFloat $(v).attr('data-price')
    currency.push $(v).attr('data-currency')
    currency_symbol.push $(v).attr('data-currency-symbol')

  currency = $.unique(currency).toString()
  currency_symbol = $.unique(currency_symbol).toString()

  mentorServiceTotalPrice.html("#{currency} #{currency_symbol}#{total.toFixed(2)}")
  orderPriceField.val(total)
  orderActualPriceField.val(total)
  orderCurrencyField.val(currency)
  orderCurrencySymbolField.val(currency_symbol)

window.initializePopOver = () ->
  popover = $('[data-toggle="popover"]')
  popover.unbind()
  popover.popover()
  popover.on 'click', (e) ->
    e.preventDefault()

window.userScheduleChecking = () ->
  if $('body').attr('data-student') == 'true'
    timezone = $('body').attr('data-timezone')
    date = moment().format('YYYY-MM-DD');
    $.get '/profile/' + userId() + '/schedules/checker.js?start=' + date + '&timezone=' + timezone

$(document).on 'ready page:load', () ->
  initializePopOver()
  if $('body').hasClass('orders') && $('body').hasClass('new')
    userScheduleChecking()
    orderFormMentorSelectionHandler()
    $('body').on 'click', '.mentor_service_selection', (e) ->
      orderFormMentorSelectionHandler()