window.renderAppointmentSchedulesCalendar = () ->
  loader(true)
  $('#appointment-schedules-calendar').fullCalendar('destroy')
  $('#appointment-schedules-calendar').fullCalendar
    eventClick: (calEvent, jsEvent, view) ->
      loader(true)
      $.get '/profile/' + userId() + '/appointments/' + calEvent.id + '.js?date=' + calEvent.start_date
    eventAfterRender: (event, element, view) ->
      element.css({background: window.eventColors[event.status]})
    header:
      left: 'prev, next title'
      center: ''
      right: 'month, agendaWeek, agendaDay'
    height: 500
    events: '/profile/' + userId() + '/appointments.json'
    timezone: $('select#timezone').val()
    displayEventEnd: true
    timeFormat: 'h:mma'
    eventAfterAllRender: () ->
      loader(false)
  calendarOverride()



$(document).on 'page:load ready', ->
  if $('body').hasClass('appointments')
    $('.appointments select#timezone').on 'change', (e) ->
      renderAppointmentSchedulesCalendar()

    renderAppointmentSchedulesCalendar()
  $('.appointments select#timezone').select2()