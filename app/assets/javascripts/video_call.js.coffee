class window.VideoCall
  webrtc: null
  room: null

  constructor: () ->
    @room = $('.video-holder').attr('id')
    @webrtc = new SimpleWebRTC
      localVideoEl: 'requester_video'
      remoteVideosEl: 'responser_video'
      autoRequestMedia: true
      debug: false
      detectSpeakingEvents: true
      autoAdjustMic: true

  showVolume: (el, volume) ->
    if !el
      return
    if volume < -45
      volume = -45 # -45 to -20 is
    if volume > -20
      volume = -20 # a good range
    el.val(volume)

  readyToCall: () ->
    @webrtc.on 'readyToCall', () =>
      if @room
        @webrtc.joinRoom(@room)

  localStream: () ->
    @webrtc.on 'localStream', (stream) ->
      $('.requester_volume').show()

  peerVideoAdded: () ->
    @webrtc.on 'videoAdded', (video, peer) ->
      connectionStateHolder = $('.peer-connection-state')
      remoteVideos = $('#responser_video')

      video.oncontextmenu = () ->
        return false
      # connection state
      if peer && peer.pc
        peer.pc.on 'iceConnectionStateChange', (event) =>
          switch peer.pc.iceConnectionState
            when 'checking'
              connectionStateHolder.html('Connecting to peer...')
            when 'completed', 'connected'
              connectionStateHolder.html('Connection established.')
            when 'disconnected'
              connectionStateHolder.html('Disconnected.')
            when 'failed'
              connectionStateHolder.html('Connection failed.')
            when 'closed'
              connectionStateHolder.html('Connection closed.')

      remoteVideos.html(video)

  volumeChange: () ->
    @webrtc.on 'volumeChange', (volume, treshold) =>
      _this.showVolume($('.requester_volume'), volume)

  connectionFailed: () ->
    @webrtc.on 'connectivityError', (peer) ->
      connectionStateHolder = $('.peer-connection-state')
      connectionStateHolder.html('Connection failed.')
      remoteVideos = $('#responser_video')
      remoteVideos.html('')

  peerWasRemoved: () ->
    @webrtc.on 'videoRemoved', (video, peer) ->
      remoteVideos = $('#responser_video')
      remoteVideos.html('')

  initiate: () ->
    @readyToCall()
    @localStream()
    @peerVideoAdded()
    @volumeChange()
    @peerWasRemoved()
    @