class WebClientNotification
  attr_accessor :notifiable, :message, :user, :title, :url, :notification

  def initialize(notifiable, message, user_to_notify, title = nil, url = nil)
    @notifiable, @message, @user, @title, @url = notifiable, message, user_to_notify, title, url
  end

  def deliver
    @notification = Notification.create(notifiable: @notifiable, message: @message, user: @user, url: @url)
    total = @user.pending_notifications.size
    Fiber.new do
      WebsocketRails[:notifications].trigger 'new', {
        message: @notification.message,
        title: @title,
        url: @url,
        pending_notification: total > 0 ? total : nil,
        user_id: @user.id
      }
    end.resume

    self
  end

  def destroy
    @notification.destroy
  end

  def self.deliver(notifiable, message, user_to_notify, title = nil, url = nil)
    new(notifiable, message, user_to_notify, title, url).deliver
  end
end