class MatchStudents
  attr_accessor :user
  attr_accessor :students

  def initialize(user)
    @user = user
  end

  def self.find_for(user)
    self.new(user)
  end

  def matches(page = nil)
    @students = @user.mentor.present? ? Student.matches_for_mentor(match_params, page) : []
  end

  def match_params
    mentor = @user.mentor
    major = mentor.schools.map(&:major)
    degrees = mentor.degree
    services = @user.services.ids
    universities = mentor.schools.map(&:university)
    other_degree = @user.other_degree
    countries = [@user.current_country, mentor.home_town].uniq

    {
      majors: major,
      degrees: degrees,
      countries: countries,
      services: services,
      universities: universities,
      other_degree: other_degree
    }
  end
end