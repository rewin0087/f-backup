class MatchMentors
  attr_accessor :user
  attr_accessor :mentors

  def initialize(user)
    @user = user
  end

  def self.find_for(user)
    new(user)
  end

  def matches(page = nil)
    @mentors = @user.student.present? ? Mentor.matches_for_student(match_params, page) : []
  end

  def match_params
    student = @user.student
    major = student.schools.map(&:major)
    degrees = student.target_degree
    country = @user.current_country
    prefered_countries = student.target_countries
    services = @user.services.ids
    universities = student.schools.map(&:university).reject(&:empty?)
    other_degree = @user.other_degree
    countries = prefered_countries << country
    countries.uniq!

    {
      majors: major,
      degrees: degrees,
      countries: countries,
      services: services,
      universities: universities.empty? ?  countries : universities,
      other_degree: other_degree,
    }
  end
end