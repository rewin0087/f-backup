class AlipayPayment
  def self.pay_by_alipay(transaction)
    if transaction.valid? and transaction.save
      redirect_to_alipay_url(transaction)
    end
  end

  def self.redirect_to_alipay_url(params)
    profile = params.wallet.user
    url = Rails.application.routes.url_helpers.hook_profile_transaction_url(profile, params[:id], host: ENV['HOST'])

    alipay_uri = Alipay::Service.create_forex_trade_url(
      out_trade_no: Time.current.strftime('%Y%m%d%H%M%S'),
      subject: ::Transaction::ITEM_NAME,
      currency: 'USD',
      total_fee: params[:amount],
      notify_url: url
    )
    Rails.logger.info "ALIPAY -- #{url}"
    "ALIPAY -- #{url}"
    alipay_uri
  end
end