class DateTimeHelper
  attr_accessor :original_datetime, :datetime, :viewer_timezone, :timezone

  def initialize(original_datetime, timezone = nil, viewer_timezone = nil)
    self.original_datetime = original_datetime
    self.timezone = timezone
    self.viewer_timezone = viewer_timezone
  end

  def convert_with_timezone
    default_timezone = Timezone::Zone.new zone: @timezone
    @datetime = default_timezone.time_with_offset(@original_datetime.in_time_zone(@timezone))

    if @viewer_timezone.present?
      viewer_timezone = Timezone::Zone.new zone: @viewer_timezone
      @datetime = viewer_timezone.time_with_offset(@datetime)
    end

    self
  end

  def time
    DateTimeHelper.time_format(@datetime)
  end

  def date
    DateTimeHelper.date_format(@datetime)
  end

  def self.time_format(time = nil)
    format = '%I:%M %p'
    time.present? ? time.strftime(format) : format
  end

  def self.date_format(date = nil)
    format = '%F'
    date.present? ? date.strftime(format) : format
  end

  def self.parse_from_string(datetime)
    date = datetime.to_datetime
    { date: DateTimeHelper.date_format(date), time: DateTimeHelper.time_format(date) }
  end

  def self.presentable_datetime(datetime = nil)
    format = '%B %d, %Y, %I:%M:%S %p'
    datetime.present? ? datetime.strftime(format) : format
  end

  def self.presentable_date(datetime = nil)
    format = '%B %d, %Y'
    datetime.present? ? datetime.strftime(format) : format
  end
end