class TaskMessenger
  attr_accessor :task, :users, :messenger

  def initialize(task, users)
    @task, @users = task, users
    @messenger = Messenger.new(task: @task)
  end

  def build_participants
    @users.map {|user|  @messenger.messenger_participants.new(user: user, user_type: user.role) }
  end

  def create
    build_participants
    @messenger.save
    @messenger
  end
end