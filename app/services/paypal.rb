class Paypal
  include PaypalConfigurations

  PERCENTAGE = 2.90 / 100
  FIXED_RATE = 0.30

  def pay_by_paypal(transaction)
    if transaction.valid? and transaction.save
      redirect_to_paypal_url(transaction)
    end
  end

  def redirect_to_paypal_url(params)
    landing_page = params[:payment_gateway] == ::Transaction::PAYMENT_GATEWAY[:paypal] ? 'login' : 'billing'

    profile = params.wallet.user
    url = Rails.application.routes.url_helpers.hook_profile_transaction_url(profile, params.id, host: ENV['HOST'])
    profile_wallet_url = Rails.application.routes.url_helpers.profile_wallets_path(profile, host: ENV['HOST'])
    values = {
      business: ENV['PAYPAL_EMAIL'],
      cmd: '_xclick',
      return: profile_wallet_url,
      amount: compute_amount(params[:amount], params[:payment_gateway]),
      item_name: ::Transaction::ITEM_NAME,
      notify_url: url,
      landing_page: landing_page
    }
    Rails.logger.info "#{url}"
    Rails.logger.info "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end

  def compute_amount(amount, type)
    total = if type == ::Transaction::PAYMENT_GATEWAY[:cc]
      (amount * ::Paypal::PERCENTAGE) + amount + FIXED_RATE
    else
      amount
    end

    total
  end
end