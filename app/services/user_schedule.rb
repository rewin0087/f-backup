class UserSchedule
  attr_accessor :user, :timezone, :date_range, :schedule_status

  def initialize(user, timezone, date_range = {}, status = nil)
    @user = user
    @timezone = timezone
    @date_range = date_range
    @schedule_status = status
  end

  def for_student(options = {})
    return Schedule::MAX_TIME_CHECKING if options[:count] && !@user.student?
    return [] unless @user.student?

    schedules = Schedule.includes(:appointment).where(student: @user.student)
    schedules = schedules.where(start_date: @date_range[:start]..(@date_range[:end] || (@date_range[:start].to_date + Schedule::MAX_TIME_CHECKING).to_s )) if @date_range[:start].present? && @date_range[:end].present?
    schedules = schedules.where(status: @schedule_status) if @schedule_status.present?

    return schedules.size if options[:count] == true

    schedules.map do |schedule|
      UserSchedule.decorator(schedule, @timezone)
    end
  end

  def for_mentor
    return [] unless @user.mentor?

    appointments = Appointment.joins(:schedule).where(mentor: @user.mentor)
    appointments = appointments.where(schedules: { start_date: @date_range[:start]..@date_range[:end] }) if @date_range[:start].present? && @date_range[:end].present?
    appointments = appointments.where(schedules: { status: @schedule_status }) if @schedule_status.present?
    appointments.map do |appointment|
      UserSchedule.decorator(appointment.schedule, @timezone, true)
    end
  end

  def self.datetime_schedule_conversion(schedule, viewer_timezone = nil)
    start_datetime = DateTimeHelper.new(schedule.start_datetime, schedule.timezone, viewer_timezone).convert_with_timezone.datetime
    end_datetime = DateTimeHelper.new(schedule.end_datetime, schedule.timezone, viewer_timezone).convert_with_timezone.datetime

    { start: start_datetime, end: end_datetime }
  end

  def self.decorator(schedule, timezone, use_appointment_id = false)
    {
      id: use_appointment_id ? schedule.appointment.id : schedule.id,
      title: schedule.appointment.present? ? "#{schedule.appointment.task.service.title} - #{schedule.status.gsub('_', ' ')}" : schedule.status,
      status: schedule.status,
      start_date: schedule.start_date,
      end_date: schedule.end_date,
      start_time: schedule.formatted_start_time,
      end_time: schedule.formatted_end_time,
      timezone: schedule.timezone
    }.merge(UserSchedule.datetime_schedule_conversion(schedule, timezone))
  end
end