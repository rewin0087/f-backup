ActiveAdmin.register AdminUser do
  permit_params :username, :first_name, :last_name, :email, :password, :password_confirmation
  menu parent: 'Administration'

  index do
    selectable_column
    id_column
    column :username
    column :first_name
    column :last_name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :username
  filter :first_name
  filter :last_name
  filter :email
  filter :timezone, as: :select, collection: proc { User::TIMEZONES.sort.map{|u| u[1] } }
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :username
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :timezone, collection: User::TIMEZONES.sort.map{|u| u[1] }
      if f.object.new_record?
          f.input :password
          f.input :password_confirmation
      end
    end
    f.actions
  end

end
