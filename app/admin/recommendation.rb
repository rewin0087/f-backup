ActiveAdmin.register Recommendation do
  permit_params Recommendation.available_params
  actions :all
  menu parent: 'Administration'

  index do
    selectable_column
    id_column
    column :message
    column :user
    column :created_at
    column :updated_at
    actions
  end
end
