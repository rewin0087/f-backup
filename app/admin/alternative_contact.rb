ActiveAdmin.register AlternativeContact do
  permit_params AlternativeContact.available_params
  actions :all, except: [:new, :create]
  menu parent: 'User Management'

  scope :all
  scope :mentors
  scope :students

  index do
    selectable_column
    id_column
    column :account_type
    column :account_value
    column :contact_type

    column 'User' do  |a|
      link_to a.user.full_name, profile_path(a.user)
    end

    column :created_at
    column :updated_at
    actions
  end

  filter :id
  filter :account_type
  filter :account_value
  filter :contact_type
  filter :user
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs 'Contact Detail' do
      f.input :account_type
      f.input :account_value
    end
    f.actions
  end
end
