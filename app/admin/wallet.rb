ActiveAdmin.register Wallet do
  permit_params Wallet.available_params
  actions :all, except: [:new, :create, :destroy]
  menu parent: 'User Management'

  scope :all

  index do
    selectable_column
    id_column
    column :amount
    column :pending_balance

    column 'Full Name' do |wallet|
      link_to wallet.user.full_name, profile_wallets_path(wallet.user)
    end

    column :created_at
    column :updated_at
    actions
  end

  filter :amount
  filter :pending_balance
  filter :user
end
