ActiveAdmin.register Notification do
  menu parent: 'Administration'
  actions :index

  scope :all
  scope :all_pending
  scope :all_seen

  index do
    selectable_column
    id_column
    column :message
    column :status
    column :notifiable_type
    column :created_at
    column :updated_at
    column 'Link' do |n|
      link_to 'Notifiable', n.url
    end
  end

  filter :message
  filter :notifiable_type
  filter :status, as: :select, collection: proc { Notification::STATUSES }
  filter :created_at
  filter :updated_at
end
