ActiveAdmin.register User do
  actions :index
  menu parent: 'User Management'

  scope :all
  scope :mentors
  scope :students

  index do
    selectable_column
    id_column
    column :username
    column :full_name
    column :role
    column :email
    column :timezone

    column 'Total Orders' do |user|
      if user.mentor?
        "#{user.mentor_orders.size}"
      elsif user.student?
        "#{user.student_orders.size}"
      end
    end

    column :sign_in_count
    column :last_sign_in_at

    column 'Profile' do |user|
      link_to 'Manage Profile', profile_path(user)
    end
  end

  filter :role, as: :select, collection: proc { User::SITE_ROLES }
  filter :full_name
  filter :username
  filter :email
  filter :current_country, as: :select, collection: proc { Wuniversity::Data::COUNTRIES.sort }
end
