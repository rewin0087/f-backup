ActiveAdmin.register Service do
  permit_params Service.available_params
  menu parent: 'Administration'

  scope :all
  scope :non_appointment_services
  scope :appointment_services

  filter :title
  filter :description
  filter :price
  filter :currency
  filter :service_type

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :price
      f.input :currency
      f.input :currency_symbol
      f.input :service_type, as: :select, collection: Service::TYPES
    end
    f.actions
  end
end
