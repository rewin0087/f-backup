ActiveAdmin.register Order do
  permit_params Order.available_params
  actions :all, except: [:new, :create, :destroy]
  menu parent: 'User Management'

  scope :all
  scope :all_pending
  scope :all_accepted
  scope :all_confirmed
  scope :all_in_progress
  scope :all_completed
  scope :all_declined
  scope :all_canceled
  scope :all_closed

  index do
    selectable_column
    id_column
    column :reference_number
    column :status
    column :price
    column :actual_price
    column :currency
    column :slug

    column 'Mentor' do |o|
      link_to o.mentor_user.full_name, profile_path(o.mentor_user)
    end

    column 'Student' do |o|
      link_to o.student_user.full_name, profile_path(o.student_user)
    end

    column 'Student Order' do |o|
      link_to 'Visit', profile_order_path(o.student_user, o)
    end

    column 'Mentor Order' do |o|
      link_to 'Visit', profile_order_path(o.mentor_user, o)
    end

    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs 'Order Detail' do
      f.input :additional_instructions
      f.input :additional_comments
      f.input :price
      f.input :actual_price
      f.input :currency
      f.input :currency_symbol
      f.input :status, collection: Order::STATUSES
    end
    f.actions
  end

  filter :id
  filter :slug
  filter :reference_number
  filter :mentor_user
  filter :student_user
  filter :status, as: :select, collection: proc { Order::STATUSES }
  filter :created_at
  filter :updated_at
end
