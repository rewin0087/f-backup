ActiveAdmin.register Schedule do
  actions :index
  menu parent: 'User Management'

  scope :all
  scope :all_available
  scope :all_occupied
  scope :all_closed
  scope :all_waiting_for_confirmation
  scope :all_request_schedule

  index do
    selectable_column
    id_column

    column :start_date

    column 'Start Time' do |s|
      DateTimeHelper.time_format(s.start_time)
    end

    column :end_date

    column 'End Time' do |s|
      DateTimeHelper.time_format(s.end_time)
    end

    column :timezone

    column 'Mentor' do |s|
      link_to s.mentor_user.full_name, profile_path(s.mentor_user) if s.mentor_user
    end

    column 'Student' do |s|
      link_to s.student_user.full_name, profile_path(s.student_user) if s.student_user
    end

    column 'Student Schedules' do |s|
      link_to 'Visit', profile_schedules_path(s.student_user) if s.student_user
    end
  end

  filter :mentor_user
  filter :student_user
  filter :status, as: :select, collection: proc { Schedule::STATUSES }
  filter :start_date, as: :date_range
  filter :end_date, as: :date_range
  filter :timezone, as: :select, collection: proc { User::TIMEZONES.sort.map{|u| u[1] } }
  filter :created_at
  filter :updated_at
end
