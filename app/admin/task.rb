ActiveAdmin.register Task do
  permit_params Task.available_params
  actions :all, except: [:new, :create, :destroy]
  menu parent: 'User Management'

  scope :all
  scope :all_accepted
  scope :all_in_progress
  scope :all_done
  scope :all_completed
  scope :all_created
  scope :all_pending
  scope :all_canceled
  scope :all_closed
  scope :all_payed_type
  scope :all_free_type
  scope :all_appointment_service
  scope :all_non_appointment_service

  index do
    selectable_column
    id_column
    column :reference_number
    column :status
    column :price
    column :actual_price
    column :currency
    column :task_type
    column :slug
    column :service

    column 'Service Type' do |t|
      t.service.service_type
    end

    column 'Mentor' do |t|
      link_to t.mentor_user.full_name, profile_path(t.mentor_user)
    end

    column 'Student' do |t|
      link_to t.student_user.full_name, profile_path(t.student_user)
    end

    column 'Student Task' do |t|
      link_to 'Manage Student Task', profile_task_path(t.student_user, t)
    end

    column 'Mentor Task' do |t|
      link_to 'Manage Mentor Task', profile_task_path(t.mentor_user, t)
    end

    column 'Student Order' do |t|
      link_to 'Visit', profile_order_path(t.student_user, t.order)
    end

    column 'Mentor Order' do |t|
      link_to 'Visit', profile_order_path(t.mentor_user, t.order)
    end

    column 'Messenger as Mentor' do |t|
      link_to 'Visit', profile_messenger_path(t.mentor_user, t.messenger) if t.messenger
    end

    column 'Messenger as Student' do |t|
      link_to 'Visit', profile_messenger_path(t.student_user, t.messenger) if t.messenger
    end

    column 'Appointment' do |t|
      "#{t.appointment.schedule.start_datetime} - #{t.appointment.schedule.end_datetime}" if t.appointment
    end

    column :created_at
    column :updated_at
    actions
  end

  filter :id
  filter :slug
  filter :reference_number
  filter :mentor_user
  filter :student_user
  filter :service
  filter :status, as: :select, collection: proc { Task::STATUSES }
  filter :task_type, as: :select, collection: proc { Task::TYPES }
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs 'Task Detail' do
      f.input :description
      f.input :requested_file
      f.input :revised_file
      f.input :status, collection: Task::STATUSES
      f.input :additional_instructions
      f.input :additional_comments
      f.input :price
      f.input :actual_price
      f.input :currency
      f.input :currency_symbol
      f.input :task_type, collection: Task::TYPES
    end
    f.actions
  end
end
