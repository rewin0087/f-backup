ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        div class: 'row' do
          div class: 'col-sm-12' do
            link_to 'Search for Mentors', mentors_path
          end

          div class: 'col-sm-12' do
            link_to 'Student List', students_path
          end
        end
      end
    end
  end # content
end
