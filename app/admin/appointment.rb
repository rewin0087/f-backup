ActiveAdmin.register Appointment do
  actions :index
  menu parent: 'User Management'

  includes :mentor, :schedule, :task, :student
  scope :all
  scope :all_pending
  scope :all_approved
  scope :all_completed
  scope :all_rescheduled
  scope :all_canceled
  scope :all_closed

  index do
    selectable_column
    id_column

    column 'Start Date' do |a|
      a.schedule.start_date
    end

    column 'Start Time' do |a|
      DateTimeHelper.time_format(a.schedule.start_time)
    end

    column 'End Date' do |a|
      a.schedule.end_date
    end

    column 'End Time' do |a|
      DateTimeHelper.time_format(a.schedule.end_time)
    end

    column 'Mentor' do |a|
      link_to a.mentor_user.full_name, profile_path(a.mentor_user)
    end

    column 'Student' do |a|
      link_to a.student_user.full_name, profile_path(a.student_user)
    end

    column 'Mentor Appointments' do |a|
      link_to 'Visit', profile_appointments_path(a.mentor_user)
    end

    column :timezone
    column :status
    column :created_at
    column :updated_at
  end

  filter :id
  filter :mentor_user
  filter :student_user
  filter :status, as: :select, collection: proc { Appointment::STATUSES }
  filter :timezone, as: :select, collection: proc { User::TIMEZONES.sort.map{|u| u[1] } }
  filter :created_at
  filter :updated_at
end
