# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text(65535)
#  original_filename :text(65535)
#  filename          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Document < ActiveRecord::Base
  belongs_to :documentable, polymorphic: true
  scope :recent_files, -> (limit){ order('created_at DESC').limit(limit || 4) }
  default_scope -> { order('created_at DESC') }

  mount_uploader :filename, ::DocumentUploader

  def self.available_params
    [
      :title,
      :description
    ]
  end
end
