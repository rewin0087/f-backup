# == Schema Information
#
# Table name: transactions
#
#  id                  :integer          not null, primary key
#  transaction_type    :string(255)
#  payment_gateway     :string(255)
#  amount              :decimal(10, )
#  notification_params :text(65535)
#  status              :string(255)
#  transaction_id      :string(255)
#  purchased_at        :datetime
#  wallet_id           :integer
#

class Transaction < ActiveRecord::Base
  belongs_to :wallet
  has_one :user, through: :wallet
  belongs_to :order

  attr_accessor :card_number, :expire_month, :expire_year, :cvv2, :item_name, :another_amount

  after_save :update_wallet

  ITEM_NAME = 'a friend abroad load wallet'

  TYPE = {
    debit: 'DEBIT',
    credit: 'CREDIT'
  }

  PAYMENT_GATEWAY = {
    alipay: 'ALIPAY',
    cc: 'CREDIT CARD',
    paypal: 'PAYPAL'
  }

  PAYPAL_STATE = {
    completed: 'Completed',
    created: 'created',
    approved: 'approved'
  }

  ALIPAY_STATE = {
    trade_finished: 'TRADE_FINISHED'
  }

  def self.available_params
    [
      :amount,
      :another_amount,
      :card_number,
      :expire_month,
      :expire_year,
      :cvv2,
      :payment_gateway
    ]
  end

  def update_wallet
    if transaction_type == TYPE[:debit] && (paypal_successful? || alipay_successful?)
      wallet.amount += amount
      wallet.save
    elsif transaction_type == TYPE[:credit] && order.present?
      case order.status
      when Order.pending
        # deduct payment from wallet of the student and add it to the pending_balance of the mentor
        order.student.user.wallet.pending_balance += amount
        order.mentor.user.wallet.pending_balance += amount
      when Order.canceled, Order.declined
        # revert payment
        order.student.user.wallet.pending_balance -= amount
        order.mentor.user.wallet.pending_balance -= amount
      when Order.closed
        # apply the real transfer
        order.student.user.wallet.amount -= amount
        order.mentor.user.wallet.amount += amount
        # and remove pending balance
        order.student.user.wallet.pending_balance -= amount
        order.mentor.user.wallet.pending_balance -= amount
      else
      end
      order.student.user.wallet.save
      order.mentor.user.wallet.save
    end
  end

  def paypal?
    payment_gateway == PAYMENT_GATEWAY[:paypal]
  end

  def credit_card?
    payment_gateway == PAYMENT_GATEWAY[:cc]
  end

  def alipay?
    payment_gateway == PAYMENT_GATEWAY[:alipay]
  end

  def self.student_payment_history(profile, page = 1)
    joins(:order).where("student_id = ? OR wallet_id = ?", profile.student.id, profile.student.id).
    order('created_at DESC').
    page(page)
  end

  def self.mentor_payment_history(profile, page = 1)
    joins(:order).where("mentor_id = ? OR wallet_id = ?", profile.mentor.id, profile.mentor.id).
    order('created_at DESC').
    page(page)
  end

  private
    def paypal_successful?
      status_changed? && (status == PAYPAL_STATE[:approved] || status == PAYPAL_STATE[:completed])
    end

    def alipay_successful?
      status_changed? && status == ALIPAY_STATE[:trade_finished]
    end
end
