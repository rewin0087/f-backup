module StatusHelper
  extend ActiveSupport::Concern

  included do
    self::STATUSES.each do |status|
      define_method "#{status.underscore}?" do
        self.status == status
      end

      define_singleton_method "#{status.underscore}" do
        status
      end
    end
  end
end