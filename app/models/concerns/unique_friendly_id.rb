module UniqueFriendlyId
  extend ActiveSupport::Concern

  def uuid
    "#{self.class.name.underscore}-#{SecureRandom.uuid}-#{id}"
  end

  def slug_candidates
    [:uuid]
  end
end