module StudentVirtualAttributesHelper
  extend ActiveSupport::Concern

  included do
    attr_accessor :target_countries,
      :target_degree,
      :questions_to_mentor,
      :remarks,
      :looking_for
  end
end