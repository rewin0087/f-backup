module MentorValidationHelper
  extend ActiveSupport::Concern

  included do
    validates_presence_of :university_email
    validates_format_of :university_email, :with => Devise::email_regexp
    validates :university_email, uniqueness: true
    validates_presence_of :home_town
    validates_presence_of :degree
    validate :minimum_length_of_degree
    validates_presence_of :undergraduate_universities
    validates :undergraduate_universities_early_acceptance, :inclusion => { :in => [true, false], :message => I18n.t('models.errors.please_select') }
    validate :minimum_length_of_undergraduate_universities
    validates_presence_of :graduate_universities
    validates :graduate_universities_early_acceptance, :inclusion => { :in => [true, false], :message => I18n.t('models.errors.please_select') }
    validate :minimum_length_of_graduate_universities
    validates_presence_of :basic_consultation_charge, message: I18n.t('models.mentor.errors.basic_consultation_charge.blank')
    validates :mentor_student, :inclusion => { :in => [true, false], :message => I18n.t('models.errors.please_select') }
    validates_presence_of :reason
    validates_presence_of :free_tip
    validates_presence_of :essay_services
    validate :minimum_length_of_essay_services
    validates_presence_of :free_tip
  end

  def minimum_length_of_undergraduate_universities
    if undergraduate_universities.present? and undergraduate_universities.reject(&:empty?).size < 1
      errors.add(:undergraduate_universities, I18n.t('models.errors.please_select_one'))
    end
  end

  def minimum_length_of_graduate_universities
    if graduate_universities.present? and graduate_universities.reject(&:empty?).size < 1
      errors.add(:graduate_universities, I18n.t('models.errors.please_select_one'))
    end
  end

  def minimum_length_of_essay_services
    if essay_services.present? and essay_services.reject(&:empty?).size < 1
      errors.add(:essay_services, I18n.t('models.errors.please_select_one'))
    end
  end

  def minimum_length_of_degree
    if degree.present? and degree.reject(&:empty?).size < 1
      errors.add(:degree, I18n.t('models.errors.please_select_one'))
    end
  end
end