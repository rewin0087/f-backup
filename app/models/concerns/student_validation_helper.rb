module StudentValidationHelper
  extend ActiveSupport::Concern

  included do
    validates_presence_of :target_countries
    validate :target_countries
    validates_presence_of :target_degree
    validate :target_degree
  end

  def minimum_length_of_target_countries
    if target_countries.present? and target_countries.reject(&:empty?).size < 1
      errors.add(:target_countries, I18n.t('models.errors.please_select_one'))
    end
  end

  def minimum_length_of_target_degree
    if target_degree.present? and target_degree.reject(&:empty?).size < 1
      errors.add(:target_degree, I18n.t('models.errors.please_select_one'))
    end
  end
end