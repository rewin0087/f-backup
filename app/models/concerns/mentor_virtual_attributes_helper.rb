module MentorVirtualAttributesHelper
  extend ActiveSupport::Concern

  included do
    attr_accessor :university_email,
      :home_town,
      :degree,
      :undergraduate_universities,
      :undergraduate_universities_early_acceptance,
      :graduate_universities,
      :graduate_universities_early_acceptance,
      :basic_consultation_charge,
      :basic_consultation_charge_currency,
      :work,
      :mentor_student,
      :reason,
      :free_tip,
      :essay_services,
      :free_tip,
      :reply_speed,
      :terms_of_use,
      :experience,
      :awards_and_honors,
      :display_reviews,
      :display_ratings
  end
end