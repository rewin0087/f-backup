module NotificationHelper
  extend ActiveSupport::Concern

  def clear_notifications(user)
    notifications.where(status: Notification.pending, user: user).map(&:seen!)
  end
end