# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

class Mentor < ActiveRecord::Base
  include MentorValidationHelper

  belongs_to :user
  has_many :bookmarks
  has_many :schools, as: :scholastic
  has_many :videos, as: :videoable
  has_many :alternative_contacts, as: :contact
  has_many :documents, as: :documentable, dependent: :destroy
  has_many :recent_documents, -> { order('created_at DESC').limit(4) }, as: :documentable, class_name: 'Document'
  has_many :tasks
  has_many :appointments
  has_many :schedules, through: :appointments
  has_many :orders
  has_many :ratings, through: :tasks

  accepts_nested_attributes_for :schools
  accepts_nested_attributes_for :alternative_contacts

  ESSAY_SERVICES = [
    'Basic essay review services (providing comments and editing errors) at $25 per 500 words (48 hr response time), including scholarship application review',
    'Advanced essay review, logic review and two rounds of editing with feedback at 500 words for $75 with 30 minute video chat (96 hr response time for first contact; 48 hr response time for subsequent requests)',
    'No, I do not want to offer essay review services.'
  ]

  ESSAY_SERVICES_WITH_INDEX = ESSAY_SERVICES.map.with_index { |v,i| [v, i] }

  paginates_per 16
  serialize :degree, Array
  serialize :essay_services, Array
  serialize :undergraduate_universities, Array
  serialize :graduate_universities, Array

  def self.available_params
    [
      :university_email,
      :home_town,
      :work,
      :mentor_student,
      :reason,
      :experience,
      :awards_and_honors,
      :display_reviews,
      :display_ratings,
      :free_tip,
      :terms_of_use,
      :reply_speed,
      :undergraduate_universities_early_acceptance,
      :graduate_universities_early_acceptance,
      :basic_consultation_charge,
      :essay_services => [],
      :degree => [],
      :undergraduate_universities => [],
      :graduate_universities => []
    ]
  end

  def self.top_nachers(page = 1, limit = 16)
    joins(:user).
    includes(:user => [:mentor => [:ratings]]).
    order('ratings.average desc').
    page(page).
    per(limit)
  end

  def self.filter_search(keywords = [], page = 1, advance_filter = {})
    values = keywords.join('|')
    query = includes(:user => [:services, :user_services, :mentor => [:schools, :ratings]])
      .where(
        search_keywords_statement,
        # Fullname
        values,
        # majors
        values,
        # degree
        values,
        # schools last attended
        values,
        # undergraduate_universities
        values,
        # users.current_country
        values,
        # home_town
        values,
        # users.other_degree
        values,
        # services.title
        values
      )

    # ADVANCE FILTER
    # SUB SELECT
    if advance_filter.values.compact.count > 0
      original_query = query.select(:id)
      advance_query = includes(:user => [:services, :user_services, :mentor => [:schools, :ratings]])
      # services
      if advance_filter[:services] and !advance_filter[:services].empty?
        advance_query = advance_query.where('services.id IN (?)', advance_filter[:services].map{|s| s.to_i})
      end
      # majors
      if advance_filter[:majors] and !advance_filter[:majors].empty?
        advance_query = advance_query.where('schools.major REGEXP (?)', advance_filter[:majors].join('|'))
      end
      # current country |  origin
      if advance_filter[:current_country] and !advance_filter[:current_country].blank?
        advance_query = advance_query.where('(users.current_country REGEXP (?) OR mentors.home_town REGEXP (?))', advance_filter[:current_country], advance_filter[:current_country])
      end
      # degrees
      if advance_filter[:degrees] and !advance_filter[:degrees].empty?
        advance_query = advance_query.where('(mentors.degree REGEXP (?) OR users.other_degree REGEXP (?))', advance_filter[:degrees].join('|'), advance_filter[:degrees].join('|'))
      end
      # ratings
      if advance_filter[:ratings] and !advance_filter[:ratings].empty?
        advance_query = advance_query.where('(ratings.average BETWEEN ? AND ?)', advance_filter[:ratings].to_i, advance_filter[:ratings].to_i + 1)
      end

      query = advance_query.where('mentors.id IN (?)', original_query)
    end

    query.order('ratings.average desc').page(page)
  end

  def self.matches_for_student(keywords, page = 1)
    statement = 'schools.major REGEXP (?) OR mentors.degree REGEXP (?) OR mentors.home_town REGEXP (?) OR users.current_country REGEXP (?) OR schools.university REGEXP (?) OR other_degree LIKE "%?%"'
    includes(:schools, :user => [:user_services, :mentor => [:schools, :ratings]]).
      where('user_services.service_id IN (?)', keywords[:services].join(',')).
      where(statement, keywords[:majors].join('|'), keywords[:degrees].join('|'), keywords[:countries].join('|'), keywords[:countries].join('|'), keywords[:universities].join('|'), keywords[:other_degree]).
      order('ratings.average desc').
      page(page).
      uniq!
  end

  private

  def self.search_keywords_statement
    '(' <<
    'users.full_name REGEXP (?) ' <<
    'OR schools.major REGEXP (?) ' <<
    'OR mentors.degree REGEXP (?) ' <<
    'OR schools.university REGEXP (?) ' <<
    'OR mentors.undergraduate_universities REGEXP (?) ' <<
    'OR users.current_country REGEXP (?) ' <<
    'OR mentors.home_town REGEXP (?) ' <<
    'OR users.other_degree REGEXP (?) ' <<
    'OR services.title REGEXP (?) ' <<
    ')'
  end
end
