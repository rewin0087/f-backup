# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  url            :text(65535)
#  source         :text(65535)
#  videoable_id   :integer
#  videoable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Video < ActiveRecord::Base
  belongs_to :videoable, polymorphic: true

  validates :url, presence: true
  validates :url, format: { with: URI.regexp }, if: Proc.new { |a| a.url.present? }

  def parse_url!
    thumbnailer = LinkScraper::Thumbnailer.parse_url!(url)
    if thumbnailer.video.blank?
      errors.add(:url, I18n.t('models.video.errors.url.not_a_video'))
      return false
    end

    self.source = thumbnailer.video
    true
  end
end
