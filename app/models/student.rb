# == Schema Information
#
# Table name: students
#
#  id                  :integer          not null, primary key
#  target_countries    :text(65535)
#  target_degree       :text(65535)
#  looking_for         :text(65535)
#  questions_to_mentor :text(65535)
#  remarks             :text(65535)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Student < ActiveRecord::Base
  include StudentValidationHelper

  belongs_to :user
  has_many :bookmarks
  has_many :schools, as: :scholastic
  has_many :alternative_contacts, as: :contact
  has_many :favorite_mentors, through: :bookmarks, source: :mentor
  has_many :schedules
  has_many :appointments, through: :schedules
  has_many :orders
  has_many :tasks, through: :orders

  accepts_nested_attributes_for :schools
  accepts_nested_attributes_for :alternative_contacts

  serialize :target_countries, Array
  serialize :target_degree, Array

  def self.matches_for_mentor(keywords, page = 1)
    statement = 'schools.major REGEXP (?) OR students.target_degree REGEXP (?) OR students.target_countries REGEXP (?) OR users.current_country REGEXP (?) OR schools.university REGEXP (?) OR other_degree LIKE "%?%"'
    includes(:schools).joins(:user => [:user_services, :student => [:schools]]).
      where('service_id IN (?)', keywords[:services]).
      where(statement, keywords[:majors].join('|'), keywords[:degrees].join('|'), keywords[:countries].join('|'), keywords[:countries].join('|'), keywords[:universities].join('|'), keywords[:other_degree]).
      page(page).
      uniq!
  end

  def self.available_params
    [
      :questions_to_mentor,
      :remarks,
      :looking_for,
      target_countries: [],
      target_degree: []
    ]
  end
end
