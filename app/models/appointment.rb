# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Appointment < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include NotificationHelper
  STATUSES = %w(PENDING APPROVED COMPLETED RESCHEDULED CANCELED CLOSED)
  NOTIFICATION_TEMPLATE = {
    PENDING: I18n.t('models.appointment.templates.pending'),
    APPROVED: I18n.t('models.appointment.templates.approved'),
    COMPLETED: I18n.t('models.appointment.templates.completed'),
    CANCELED: I18n.t('models.appointment.templates.canceled'),
    DECLINED: I18n.t('models.appointment.templates.declined'),
    REQUEST_SCHEDULE: I18n.t('models.appointment.templates.request_schedule')
  }
  include StatusHelper

  scope :all_pending, -> { where(status: Appointment.pending) }
  scope :all_approved, -> { where(status: Appointment.approved) }
  scope :all_completed, -> { where(status: Appointment.completed) }
  scope :all_rescheduled, -> { where(status: Appointment.rescheduled) }
  scope :all_canceled, -> { where(status: Appointment.canceled) }
  scope :all_closed, -> { where(status: Appointment.closed) }

  belongs_to :schedule
  belongs_to :mentor
  belongs_to :task
  has_one :messenger, through: :task
  has_one :student, through: :schedule
  has_many :notifications, as: :notifiable, auto_include: false, dependent: :destroy
  has_one :mentor_user, through: :mentor, source: :user
  has_one :student_user, through: :student, source: :user

  validates :schedule, presence: true
  validates :mentor, presence: true
  validates :status, presence: true
  validates :task, presence: true

  validate do |appointment|
    if appointment.schedule.present?
      appointment.schedule.valid?
      messages = appointment.schedule.errors.full_messages
      messages.map {|message| self.errors[:base] << message }
    end
  end

  def self.available_params
    [
      :timezone,
      :status,
      :schedule_id,
      :mentor_id,
      :task_id
    ]
  end

  def presenter
    @presenter ||= ::AppointmentPresenter.new(self)
  end

  def decline!
    if schedule.request_schedule?
      if schedule.destroy
        AppointmentMailer.delay.decline_request_appointment_schedule_notification_to_mentor(task.id)
        WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:REQUEST_SCHEDULE], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, task)).destroy
      end
    else
      schedule.status = Schedule.available

      if schedule.save
        AppointmentMailer.delay.decline_appointment_notification_to_mentor(self)
        WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:DECLINED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, task)).destroy
        destroy
      end
    end

    if schedule.errors.any?
      errors.add(:schedule, schedule.errors.full_messages.first)
    end

    self
  end

  def confirm!
    self.status = Appointment.approved
    schedule.status = Schedule.occupied

    users = []
    users << mentor.user
    users << student.user
    @messenger = TaskMessenger.new(task, users).create

    if @messenger.errors.any?
      errors[:base] << @messenger.errors.full_messages.first
    end

    if errors.empty? && save && schedule.save
      AppointmentMailer.delay.approved_notification_to_student(id)
      AppointmentMailer.delay.approved_notification_to_mentor(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:APPROVED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, task))
    end

    if schedule.errors.any?
      errors.add(:schedule, schedule.errors.full_messages.first)
    end

    self
  end

  def cancel!
    schedule.status = Schedule.available

    if schedule.save && messenger.destroy
      AppointmentMailer.delay.cancel_notification_to_mentor(id)
      AppointmentMailer.delay.cancel_notification_to_student(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CANCELED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, task))
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CANCELED], student.user, I18n.t('views.tasks.view_task_label'), profile_task_path(student.user, task))
      destroy
    end

    if schedule.errors.any?
      errors.add(:schedule, schedule.errors.full_messages.first)
    end

    self
  end

  def close!
    self.status = Appointment.closed
    save
    self
  end

  def completed!
    unless schedule.occupied? && approved?
      errors[:base] << I18n.t('models.appointment.errors.completed')
    end

    self.status = Appointment.completed
    save if errors.empty?
    # send email here and notification
    AppointmentMailer.delay.completed_notification_to_mentor(id)
    AppointmentMailer.delay.completed_notification_to_student(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:COMPLETED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, task))
    self
  end
end
