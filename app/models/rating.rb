# == Schema Information
#
# Table name: ratings
#
#  id               :integer          not null, primary key
#  speed            :float(24)
#  knowledgeability :float(24)
#  overall_rating   :float(24)
#  average          :float(24)
#  feedbacks        :string(255)
#  task_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Rating < ActiveRecord::Base
  belongs_to :task
  has_one :service, through: :task

  after_create :update_average

  def self.available_params
    [
      :speed,
      :knowledgeability,
      :overall_rating,
      :feedbacks
    ]
  end

  def update_average
    average = (speed + knowledgeability + overall_rating) / 3
    self.update_attributes(average: average)
  end
end
