# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text(65535)
#  price           :decimal(10, )
#  currency        :string(255)
#  currency_symbol :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_type    :string(255)
#

class Service < ActiveRecord::Base
  TYPES = %w(NON_APPOINTMENT_SERVICE APPOINTMENT_SERVICE)

  TYPES.each do |type|
    define_method "#{type.underscore}?" do
      self.service_type == type
    end

    define_singleton_method "#{type.underscore}" do
      type
    end
  end

  ALL = [
    { title: 'Basic Essay Review', description: ' (including scholarship app review) Basic essay review services (providing comments and editing errors) at $25 per 500 words (48 hr response time)', price: 25, currency: 'USD', currency_symbol: '$', service_type: Service.non_appointment_service },
    { title: 'Advanced essay review', description: 'logic review and two rounds of editing with feedback at 500 words for $75 with 30 minute video chat (96 hr response time for first contact, 48 hr response time for subsequent requests)', price: 75, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Consultation', description: 'School selection, CV review, detailed application timeline, student life and etc.', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Scholarships', description: 'Finding scholarships and application review', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Recommendation', description: 'Recommendation letter questions', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Mock Interviews', description: '$20 per hour (includes preparation work by mentor)', price: 20, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'College Visit', description: 'Personal Tour Guide', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Find a job', description: 'How to find a job after college', price: 5, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service },
    { title: 'Other consultation services', description: 'email admin@afriendabroad.com with requested service before booking', price: 20, currency: 'USD', currency_symbol: '$', service_type: Service.appointment_service }
  ]

  scope :non_appointment_services, -> { where(service_type: Service.non_appointment_service) }
  scope :appointment_services, -> { where(service_type: Service.appointment_service) }

  has_many :user_services
  has_many :users, through: :user_services
  has_many :tasks
  has_many :orders, through: :tasks

  def self.available_params
    [
      :title,
      :description,
      :price,
      :currency,
      :currency_symbol,
      :service_type
    ]
  end
end
