# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

class Order < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include NotificationHelper
  STATUSES_WITH_ACTION = %w(PENDING ACCEPTED CONFIRMED IN_PROGRESS COMPLETED)
  STATUSES = %w(DECLINED CANCELED CLOSED) + STATUSES_WITH_ACTION
  NOTIFICATION_TEMPLATE = {
    PENDING: I18n.t('models.order.templates.pending'),
    ACCEPTED: I18n.t('models.order.templates.accepted'),
    CONFIRMED: I18n.t('models.order.templates.confirmed'),
    IN_PROGRESS: I18n.t('models.order.templates.in_progress'),
    COMPLETED: I18n.t('models.order.templates.completed'),
    DECLINED: I18n.t('models.order.templates.declined'),
    CANCELED: I18n.t('models.order.templates.canceled'),
    CLOSED: I18n.t('models.order.templates.closed'),
    ADDITIONAL_INSTRUCTIONS: I18n.t('models.order.templates.additional_instructions'),
    ADDITIONAL_COMMENTS: I18n.t('models.order.templates.additional_comments')
  }

  extend FriendlyId
  friendly_id :slug_candidates, use: [:history]
  include UniqueFriendlyId
  include StatusHelper

  default_scope -> { order('created_at DESC') }
  scope :all_pending, -> { where(status: Order.pending) }
  scope :all_accepted, -> { where(status: Order.accepted) }
  scope :all_confirmed, -> { where(status: Order.confirmed) }
  scope :all_in_progress, -> { where(status: Order.in_progress) }
  scope :all_completed, -> { where(status: Order.completed) }
  scope :all_declined, -> { where(status: Order.declined) }
  scope :all_canceled, -> { where(status: Order.canceled) }
  scope :all_closed, -> { where(status: Order.closed) }

  belongs_to :student
  belongs_to :mentor
  has_many :tasks
  has_many :notifications, as: :notifiable, auto_include: false, dependent: :destroy
  has_many :services, through: :tasks
  has_many :transactions
  has_one :mentor_user, through: :mentor, source: :user
  has_one :student_user, through: :student, source: :user
  validates :status, presence: true
  validates :student, presence: true
  validates :mentor, presence: true
  validate :service_tasks_presence
  validates :price, presence: true, numericality: true
  validates :actual_price, presence: true, numericality: true
  validates :currency, presence: true
  validates :currency_symbol, presence: true
  validate :sufficient_wallet?

  after_create :generate_reference_number

  def generate_reference_number
    created = created_at.to_time.to_i
    self.reference_number = "ORD-#{id}-#{created}"
    save
  end

  def service_tasks_presence
    if tasks.empty?
      errors[:base] << I18n.t('models.appointment.errors.service_tasks')
    end
  end

  def compute_price_and_set_currency
    self.price = tasks.inject(0) { |sum, task| sum + task.price }
    self.actual_price = self.price
    self.currency = tasks.map(&:currency).uniq.first
    self.currency_symbol = tasks.map(&:currency_symbol).uniq.first
  end

  def self.available_params
    [
      :additional_instructions,
      :additional_comments,
      :student_id,
      :mentor_id,
      :status,
      :price,
      :actual_price,
      :currency,
      :currency_symbol
    ]
  end

  def accept!
    self.status = Order.accepted
    self.tasks.map(&:pending!)
    save
    # send email here and notification
    OrderMailer.delay.accepted_notification_to_student(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:ACCEPTED], student.user, I18n.t('views.orders.view_order_label'), profile_order_path(student.user, self))
    self
  end

  def confirm!
    self.status = Order.confirmed
    self.tasks.map(&:accept!)
    save
    # send email here and notification
    OrderMailer.delay.confirmed_notification_to_mentor(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CONFIRMED], mentor.user, I18n.t('views.orders.view_order_label'), profile_order_path(mentor.user, self))
    self
  end

  def in_progress!
    self.status = Order.in_progress
    missed_requested_file = self.tasks.map {|task| task.requested_file.file }.include?(nil)

    if missed_requested_file
      self.errors[:base] << I18n.t('models.order.errors.missed_requested_file')
    end

    if errors.empty?
      save
      # send email here and notification
      OrderMailer.delay.in_progress_notification_to_mentor(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:IN_PROGRESS], mentor.user, I18n.t('views.orders.view_order_label'), profile_order_path(mentor.user, self))
    end

    self
  end

  def completed!
    self.status = Order.completed
    missed_revised_file = self.tasks.map {|task| task.revised_file.file }.include?(nil)
    completed_tasks = self.tasks.map(&:completed?).uniq

    if missed_revised_file
      self.errors[:base] << I18n.t('models.order.errors.missed_revised_file')
    end

    if completed_tasks.include?(false)
      self.errors[:base] << I18n.t('models.order.errors.incomplete_tasks.completed')
    end

    if errors.empty?
      save
      # send email here and notification
      OrderMailer.delay.completed_notification_to_mentor(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:COMPLETED], mentor.user, I18n.t('views.orders.view_order_label'), profile_order_path(mentor.user, self))
    end

    self
  end

  def decline!
    self.status = Order.declined
    self.tasks.map(&:cancel!)
    save
    # create transaction
    create_new_transaction
    # send email here and notification
    OrderMailer.delay.declined_notification_to_student(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:DECLINED], student.user, I18n.t('views.orders.view_order_label'), profile_order_path(student.user, self))
    self
  end

  def cancel!
    self.status = Order.canceled
    self.tasks.map(&:cancel!)
    save
    # create transaction
    create_new_transaction
    # send email here and notification
    OrderMailer.delay.canceled_notification_to_student(id)
    OrderMailer.delay.canceled_notification_to_mentor(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CANCELED], student.user, I18n.t('views.orders.view_order_label'), profile_order_path(student.user, self))
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CANCELED], mentor.user, I18n.t('views.orders.view_order_label'), profile_order_path(mentor.user, self))
    self
  end

  def close!
    self.status = Order.closed
    completed_tasks = self.tasks.map(&:completed?).uniq

    if completed_tasks.include?(false)
      self.errors[:base] << I18n.t('models.order.errors.incomplete_tasks.close')
    else
      self.tasks.map(&:close!)
    end

    if errors.empty?
      save
      # create transaction
      create_new_transaction
      # send email here and notification
      OrderMailer.delay.closed_notification_to_mentor(id)
      OrderMailer.delay.closed_notification_to_student(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CLOSED], mentor.user, I18n.t('views.orders.view_order_label'), profile_order_path(mentor.user, self))
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CLOSED], student.user, I18n.t('views.orders.view_order_label'), profile_order_path(student.user, self))
    end

    self
  end

  def sufficient_wallet?
    errors[:base] << I18n.t('models.order.errors.insufficient_wallet') if id.nil? && (price && student) && (price > student.user.wallet.current_balance)
  end

  def create_new_transaction
    transactions.create(
      transaction_type: Transaction::TYPE[:credit],
      amount: actual_price,
      status: status
    )
  end
end
