# == Schema Information
#
# Table name: user_services
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  service_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserService < ActiveRecord::Base
  default_scope -> { order('service_id ASC') }

  belongs_to :user
  belongs_to :service, dependent: :destroy
end
