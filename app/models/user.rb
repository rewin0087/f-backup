# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  full_name              :string(255)
#  username               :string(255)
#  role                   :string(255)
#  about_me               :text(65535)
#  additional_information :text(65535)
#  primary_photo          :string(255)
#  current_country        :string(255)
#  other_degree           :string(255)
#  phone_number           :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  timezone               :string(255)
#  slug                   :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :async,
         :recoverable, :rememberable, :trackable, :validatable

  extend FriendlyId
  include MentorVirtualAttributesHelper
  include StudentVirtualAttributesHelper

  MAX_FILESIZE = 3.megabytes.to_i # 3mb

  ROLES = %w(ADMINISTRATOR STUDENT MENTOR)
  SITE_ROLES = %w(STUDENT MENTOR)

  DEGREES = {
    bachelor: "Bachelor's",
    llm: "LLM",
    jd: "JD",
    master: "Master's NON-MBA",
    mba: "MBA",
    md: "MD",
    phd: "PhD",
    other: "Other"
  }

  MENTOR = 'Mentor'
  STUDENT = 'Student'

  DEGREE_NAMES = DEGREES.values

  TIMEZONES = Timezone::Zone.list.map{|t| [t[:title].gsub('/', ' ').gsub('_', ' '), t[:title]]}.sort

  scope :mentors, -> { where(role: User.mentor) }
  scope :students, -> { where(role: User.student) }

  has_one :student, dependent: :destroy
  has_one :mentor, dependent: :destroy
  has_many :user_services
  has_many :services, through: :user_services
  has_many :images, as: :imageable, dependent: :destroy
  has_many :alternative_contacts, as: :contact
  has_many :recent_images, -> { order('created_at DESC').limit(4) }, as: :imageable, class_name: 'Image'
  has_many :notifications, class_name: 'Notification', foreign_key: 'user_id'
  has_many :pending_notifications, -> { where(status: Notification.pending) }, class_name: 'Notification', foreign_key: 'user_id'
  has_one :wallet, dependent: :destroy
  has_many :transactions, through: :wallet, dependent: :destroy
  has_many :mentor_orders, through: :mentor, source: :orders
  has_many :student_orders, through: :student, source: :orders

  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone_number, presence: true, :if => :mentor?
  validates :current_country, presence: true
  validates_associated :services
  validate :minimum_length_of_services
  validates :primary_photo, presence: true, :if => :mentor?
  validates :role, presence: true
  validates :timezone, presence: true
  # virtual attributes
  validates :reply_speed, :acceptance => { :accept => '1' }, :on => :create, :if => :mentor?
  validates :terms_of_use, :acceptance => { :accept => '1' }, :on => :create, :if => :mentor?
  # validate child
  validate do |user|
    if mentor?
      user.mentor.valid?
      messages = user.mentor.errors.messages
      messages.map {|m| errors.add(m[0], m[1].first)}
    end

    if student?
      user.student.valid?
      messages = user.student.errors.messages
      messages.map {|m| errors.add(m[0], m[1].first)}
    end
  end

  before_save :set_full_name
  after_create :generate_wallet

  accepts_nested_attributes_for :student, allow_destroy: true
  accepts_nested_attributes_for :mentor, allow_destroy: true

  mount_uploader :primary_photo, ::UserAvatarUploader
  friendly_id :slug_candidates, use: [:history]

  ROLES.each do |role|
    define_method "#{role.underscore}?" do
      self.role == role
    end

    define_singleton_method "#{role.underscore}" do
      role
    end
  end

  DEGREES.keys.each do |key|
    define_method "#{key}?" do
      self.services.include? DEGREES[key]
    end

    define_singleton_method "#{key}" do
      DEGREES[key]
    end
  end

  def self.available_params
    [
      :username,
      :password,
      :password_confirmation,
      :email,
      :first_name,
      :last_name,
      :phone_number,
      :current_country,
      :about_me,
      :additional_information,
      :primary_photo,
      :timezone,
      :other_degree,
      :service_ids => []
    ]
  end

  def assign_virtual_attributes
    set_mentor_virtual_attributes if mentor?
    set_student_virtual_attributes if student?
  end

  def set_mentor_virtual_attributes
    mentor_attributes = mentor.attributes.symbolize_keys
    mentor_attributes.delete(:id)
    mentor_attributes.delete(:user_id)
    mentor_attributes.delete(:created_at)
    mentor_attributes.delete(:updated_at)
    assign_attributes(mentor_attributes)
  end

  def set_student_virtual_attributes
    student_attributes = student.attributes.symbolize_keys
    student_attributes.delete(:id)
    student_attributes.delete(:user_id)
    student_attributes.delete(:created_at)
    student_attributes.delete(:updated_at)
    assign_attributes(student_attributes)
  end

  def set_full_name
    self.full_name = "#{first_name.humanize} #{last_name.humanize}" if last_name.present? && first_name.present?
  end

  def slug_candidates
    [
      :username,
      :full_name,
      [:full_name, :id],
      [:first_name, :last_name],
      [:first_name, :last_name, :id]
    ]
  end

  def minimum_length_of_services
    if user_services.size < 1
      errors.add(:service_ids, I18n.t('models.errors.please_select_one'))
    end
  end

  def add_primary_photo_to_images
    images.create(source: primary_photo, status: Image.public)
  end


  def find_bookmark_by_mentor(mentor)
    student.bookmarks.find_by(mentor: mentor)
  end

  def my_students(page = nil)
    User.joins(:student => [:orders => :student]).where('orders.mentor_id = ?', mentor.id).page(page).uniq!
  end

  def my_mentors(page = nil)
    User.joins(:mentor => [:orders => :mentor]).where('orders.student_id = ?', student.id).page(page).uniq!
  end

  def generate_wallet
    create_wallet(amount: 0, pending_balance: 0)
  end
end
