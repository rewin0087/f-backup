# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  text         :text(65535)
#  messenger_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Message < ActiveRecord::Base
  belongs_to :messenger
  belongs_to :user
  has_one :task, through: :messenger
  has_one :appointment, through: :messenger

  default_scope -> { order('created_at DESC') }

  validates :text, presence: { message: 'message must not be empty.' }
  validates :user_id, presence: true
  validates :messenger_id, presence: true
end
