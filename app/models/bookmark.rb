# == Schema Information
#
# Table name: bookmarks
#
#  id         :integer          not null, primary key
#  student_id :integer
#  mentor_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Bookmark < ActiveRecord::Base
  belongs_to :student
  belongs_to :mentor
end
