# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  description             :text(65535)
#  requested_file          :string(255)
#  revised_file            :string(255)
#  status                  :string(255)      default("CREATED")
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  task_type               :string(255)      default("PAYED")
#  service_id              :integer
#  order_id                :integer
#  mentor_id               :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

class Task < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include NotificationHelper
  STATUSES_WITH_ACTION = %w(ACCEPTED IN_PROGRESS DONE COMPLETED)
  STATUSES = %w(CREATED PENDING CANCELED CLOSED) + STATUSES_WITH_ACTION
  TYPES = %w(PAYED FREE DISCOUNTED)
  NOTIFICATION_TEMPLATE = {
    IN_PROGRESS: I18n.t('models.task.templates.in_progress'),
    BACK_TO_IN_PROGRESS: I18n.t('models.task.templates.back_to_in_progress'),
    DONE: I18n.t('models.task.templates.done'),
    COMPLETED: I18n.t('models.task.templates.completed'),
    CLOSED: I18n.t('models.task.templates.closed'),
    ADDITIONAL_INSTRUCTIONS: I18n.t('models.task.templates.additional_instructions'),
    ADDITIONAL_COMMENTS: I18n.t('models.task.templates.additional_comments'),
    REQUEST_FILE_UPLOADED: I18n.t('models.task.templates.request_file_uploaded'),
    REVISED_FILE_UPLOADED: I18n.t('models.task.templates.revised_file_uploaded')
  }

  extend FriendlyId
  friendly_id :slug_candidates, use: [:history]
  include UniqueFriendlyId
  include StatusHelper

  scope :all_accepted, -> { where(status: Task.accepted) }
  scope :all_in_progress, -> { where(status: Task.in_progress) }
  scope :all_done, -> { where(status: Task.done) }
  scope :all_completed, -> { where(status: Task.completed) }
  scope :all_created, -> { where(status: Task.created) }
  scope :all_pending, -> { where(status: Task.pending) }
  scope :all_canceled, -> { where(status: Task.canceled) }
  scope :all_closed, -> { where(status: Task.closed) }
  scope :all_payed_type, -> { where(task_type: Task.payed) }
  scope :all_free_type, -> { where(task_type: Task.free) }
  scope :all_appointment_service, -> { joins(:service).where(service: [service_type: Service.appointment_service]) }
  scope :all_non_appointment_service, -> { joins(:service).where(service: [service_type: Service.non_appointment_service]) }

  belongs_to :service
  belongs_to :order
  belongs_to :mentor
  has_many :notifications, as: :notifiable, auto_include: false, dependent: :destroy
  has_one :student, through: :order
  has_one :appointment
  has_one :messenger
  has_one :rating
  has_one :mentor_user, through: :mentor, source: :user
  has_one :student_user, through: :student, source: :user

  validates :mentor, presence: true
  validates :status, presence: true
  validates :order, presence: true
  validates :service, presence: true

  mount_uploader :requested_file, FileUploader
  mount_uploader :revised_file, FileUploader

  after_create :generate_reference_number

  TYPES.each do |type|
    define_method "#{type.underscore}?" do
      self.task_type == type
    end

    define_singleton_method "#{type.underscore}" do
      type
    end
  end

  def generate_reference_number
    created = created_at.to_time.to_i
    self.reference_number = "TAS-#{id}-#{created}"
    save
  end

  def clear_all_notifications(user)
    clear_notifications(user)

    if appointment.present?
      appointment.clear_notifications(user)

      if appointment.schedule.present?
        appointment.schedule.clear_notifications(user)
      end
    end
  end

  def self.available_params
    [
      :description,
      :requested_file,
      :revised_file,
      :status,
      :additional_instructions,
      :additional_comments,
      :price,
      :actual_price,
      :currency,
      :currency_symbol,
      :task_type,
      :service_id,
      :order_id,
      :mentor_id
    ]
  end

  def pending!
    self.status = Task.pending
    save
    self
  end

  def accept!
    self.status = Task.accepted
    save
    self
  end

  def in_progress!(revert = nil)
    self.status = Task.in_progress

    unless requested_file.file
      self.errors[:base] << I18n.t('models.task.errors.requested_file')
    end

    unless order.in_progress?
      self.errors[:base] << I18n.t('models.task.errors.order_in_progress')
    end

    if service.service_type == Service.appointment_service
      if appointment.blank?
        self.errors[:base] << I18n.t('models.task.errors.no_appointment')
      else
        unless (appointment.status == Appointment.completed)
          self.errors[:base] << I18n.t('models.task.errors.appointment_not_completed.in_progress')
        end
      end
    end

    if errors.empty?
      save
      # send email here and notification
      if revert
        TaskMailer.delay.back_to_in_progress_notification_to_mentor(id)
        WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:BACK_TO_IN_PROGRESS], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, self))
      else
        TaskMailer.delay.in_progress_notification_to_student(id)
        WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:IN_PROGRESS], student.user, I18n.t('views.tasks.view_task_label'), profile_task_path(student.user, self))
      end
    end

    self
  end

  def done!
    self.status = Task.done

    unless revised_file.file
      self.errors[:base] << I18n.t('models.task.errors.revised_file')
    end

    if service.service_type == Service.appointment_service
      unless appointment.status == Appointment.completed
        self.errors[:base] << I18n.t('models.task.errors.appointment_not_completed.done')
      end
    end

    if errors.empty?
      save
      # send email here and notification
      TaskMailer.delay.done_notification_to_student(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:DONE], student.user, I18n.t('views.tasks.view_task_label'), profile_task_path(student.user, self))
    end

    self
  end

  def completed!
    self.status = Task.completed

    if service.service_type == Service.appointment_service
      if appointment.status == Appointment.completed
        appointment.close!
      else
        self.errors[:base] << I18n.t('models.task.errors.appointment_not_completed.completed')
      end
    end

    if errors.empty?
      save
      # send email here and notification
      TaskMailer.delay.completed_notification_to_mentor(id)
      WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:COMPLETED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, self))
    end

    self
  end

  def cancel!
    self.status = Task.canceled
    save
    self
  end

  def close!
    self.status = Task.closed
    save
    # send email here and notification
    TaskMailer.delay.closed_notification_to_mentor(id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:CLOSED], mentor.user, I18n.t('views.tasks.view_task_label'), profile_task_path(mentor.user, self))
    self
  end
end
