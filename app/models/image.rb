# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  source         :string(255)
#  status         :string(255)
#  imageable_id   :integer
#  imageable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Image < ActiveRecord::Base
  STATUSES = %w(PUBLIC PRIVATE)

  belongs_to :imageable, polymorphic: true
  scope :recent_images, -> (limit){ order('created_at DESC').limit(limit || 4) }
  default_scope -> { order('created_at DESC') }

  mount_uploader :source, ::UserAvatarUploader

  STATUSES.each do |status|
    define_method "#{status.underscore}?" do
      self.status == status
    end

    define_singleton_method "#{status.underscore}" do
      status
    end
  end
end
