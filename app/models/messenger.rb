# == Schema Information
#
# Table name: messengers
#
#  id         :integer          not null, primary key
#  task_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  slug       :string(255)
#

class Messenger < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: [:history]
  include UniqueFriendlyId

  belongs_to :task
  has_many :messages, dependent: :destroy
  has_many :messenger_participants, dependent: :destroy
  has_one :appointment, through: :task

  validates :task, presence: true
end
