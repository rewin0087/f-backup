# == Schema Information
#
# Table name: recommendations
#
#  id         :integer          not null, primary key
#  message    :text(65535)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Recommendation < ActiveRecord::Base
  belongs_to :user

  validates :user_id, presence: true
  validates :message, presence: true

  def self.available_params
    [
      :user_id,
      :message
    ]
  end
end
