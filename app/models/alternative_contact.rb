# == Schema Information
#
# Table name: alternative_contacts
#
#  id            :integer          not null, primary key
#  account_type  :string(255)
#  account_value :string(255)
#  contact_id    :integer
#  contact_type  :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

class AlternativeContact < ActiveRecord::Base
  belongs_to :contact, polymorphic: true
  belongs_to :user

  after_create :attach_to_user

  scope :students, -> { where(contact_type: 'Mentor') }
  scope :mentors, -> { where(contact_type: 'Student') }

  validates_presence_of :account_value, if: :account_type?

  def self.available_params
    [
      :account_type,
      :account_value
    ]
  end

  def attach_to_user
    self.user = contact.user
    save
    self
  end

  def account_type?
    account_type.present?
  end
end
