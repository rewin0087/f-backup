# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Schedule < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include NotificationHelper
  STATUSES = %w(AVAILABLE OCCUPIED CLOSED WAITING_FOR_CONFIRMATION REQUEST_SCHEDULE)
  NOTIFICATION_TEMPLATE = {
    WAITING_FOR_CONFIRMATION: I18n.t('models.schedule.templates.waiting_for_confirmation'),
    REQUEST_SCHEDULE: I18n.t('models.schedule.templates.request_schedule')
  }

  MAX_TIME_CHECKING = 2.weeks
  MAX_SCHEDULES_PER_TWO_WEEKS = 10
  APPOINTMENT_CANCELLATION_OFFSET = 4.hours

  include StatusHelper

  scope :all_available, -> { where(status: Schedule.available) }
  scope :all_occupied, -> { where(status: Schedule.occupied) }
  scope :all_closed, -> { where(status: Schedule.closed) }
  scope :all_waiting_for_confirmation, -> { where(status: Schedule.waiting_for_confirmation) }
  scope :all_request_schedule, -> { where(status: Schedule.request_schedule) }

  belongs_to :student

  has_one :appointment, dependent: :destroy
  has_one :mentor, through: :appointment
  has_many :notifications, as: :notifiable, auto_include: false, dependent: :destroy
  has_one :task, through: :appointment
  has_one :mentor_user, through: :mentor, source: :user
  has_one :student_user, through: :student, source: :user

  validates :timezone, presence: true
  validates :start_date, presence: true
  validates :start_time, presence: true
  validates :end_date, presence: true
  validates :end_time, presence: true
  validates :student, presence: true
  validates_uniqueness_of :start_time, scope: [:start_date, :student_id]
  validates_uniqueness_of :end_time, scope: [:start_date, :student_id]

  def self.available_params
    [
      :start_date,
      :start_time,
      :end_date,
      :end_time,
      :timezone,
      :student_id,
      :status,
    ]
  end

  def start_datetime
    "#{start_date} #{formatted_start_time}"
  end

  def end_datetime
    "#{end_date} #{formatted_end_time}"
  end

  def formatted_start_time
    start_time.strftime('%I:%M %p')
  end

  def formatted_end_time
    end_time.strftime('%I:%M %p')
  end

  def waiting_for_confirmation!
    self.status = Schedule.waiting_for_confirmation
    save
    # send email here and notifcation
    AppointmentMailer.delay.appointment_request_notification_to_mentor(appointment.id)
    AppointmentMailer.delay.appointment_request_notification_to_student(appointment.id)
    WebClientNotification.deliver(self, NOTIFICATION_TEMPLATE[:WAITING_FOR_CONFIRMATION], student.user, I18n.t('views.tasks.view_task_label'), profile_task_path(student.user, appointment.task))
  end

  def allow_cancellation?
    schedule_time = start_datetime
    is_allowed = DateTimeHelper.new(Time.now, timezone).convert_with_timezone.datetime + APPOINTMENT_CANCELLATION_OFFSET < DateTimeHelper.new(schedule_time, timezone).convert_with_timezone.datetime
    status == Schedule.occupied && is_allowed
  end

  def start_appointment?(offset = 20) # 20 minutes
    converted = converted_start_datetime
    now = converted[:now]
    datetime = converted[:start_datetime] - offset.minutes
    now >= datetime
  end

  def converted_start_datetime
    now = DateTimeHelper.new(Time.now, timezone).convert_with_timezone.datetime
    schedule_time = start_datetime
    datetime = DateTimeHelper.new(schedule_time, timezone).convert_with_timezone.datetime

    { now: now, start_datetime: datetime }
  end

  def start_datetime_remaining
    datetime = converted_start_datetime
    time_remaining = (datetime[:start_datetime] - datetime[:now]).round
    time_remaining > 0 ? time_remaining : 0
  end

  def end_appointment?
    schedule_time = end_datetime
    now = DateTimeHelper.new(Time.now, timezone).convert_with_timezone.datetime
    end_datetime = DateTimeHelper.new(schedule_time, timezone).convert_with_timezone.datetime
    now >= end_datetime
  end

  def close!
    appointment.completed!

    unless appointment.errors.empty?
      errors[:base] << appointment.errors.full_messages.first
    end

    self.status = Schedule.closed
    save if errors.empty?
    # send email here
    self
  end
end
