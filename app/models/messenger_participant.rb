# == Schema Information
#
# Table name: messenger_participants
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  messenger_id :integer
#  user_type    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class MessengerParticipant < ActiveRecord::Base
  belongs_to :user
  belongs_to :messenger

  validates :user, presence: true
  validates :messenger, presence: true
  validates :user_type, presence: true
end
