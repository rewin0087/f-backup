# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  university      :text(65535)
#  major           :text(65535)
#  scholastic_id   :integer
#  scholastic_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class School < ActiveRecord::Base
  belongs_to :scholastic, polymorphic: true

  validates_presence_of :university, :if => :mentor?
  validates_presence_of :major

  def self.available_params
    [
      :university,
      :major
    ]
  end

  def mentor?
    self.scholastic_type == User::MENTOR
  end
end
