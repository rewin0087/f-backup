# == Schema Information
#
# Table name: wallets
#
#  id      :integer          not null, primary key
#  amount  :decimal(10, )
#  user_id :integer
#  pending_balance  :decimal(10, )
#

class Wallet < ActiveRecord::Base
  belongs_to :user
  has_many :transactions

  def current_balance
    if user.student?
      amount - pending_balance
    elsif user.mentor?
      amount + pending_balance
    end
  end

  def self.available_params
    [
      :amount,
      :pending_balance
    ]
  end
end
