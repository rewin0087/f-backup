module Presenter
  class Transaction
    attr_reader :data
    delegate :as_json, :to => :data

    def initialize(data, options = {})
      get_data(data, options)
    end

    private

    def get_data(transaction, options)
      name = if options.student?
        transaction.order.mentor.user.first_name
      elsif options.mentor?
        transaction.order.student.user.first_name
      end

      @data = {
        transaction_date: DateTimeHelper.date_format(transaction.order.created_at),
        user_name: name,
        order: transaction.order,
        services: transaction.order.services.pluck(:title).join(', '),
        amount: transaction.amount,
        status: transaction.paypal? ? "Paypal #{transaction.status}" : transaction.status
      }

      @data
    end
  end
end