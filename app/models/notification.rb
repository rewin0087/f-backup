# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  message         :string(255)
#  status          :string(255)      default("PENDING")
#  user_id         :integer
#  notifiable_type :string(255)
#  notifiable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  url             :string(255)
#

class Notification < ActiveRecord::Base
  STATUSES = %w(PENDING SEEN)
  include StatusHelper
  default_scope -> { order('created_at DESC') }
  scope :all_seen, -> { where(status: Notification.seen) }
  scope :all_pending, -> { where(status: Notification.pending) }
  belongs_to :notifiable, polymorphic: true
  belongs_to :user

  def seen!
    self.status = Notification.seen
    save
  end
end
