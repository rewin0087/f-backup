class ImagePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    # access control is in the action itself
    true
  end

  def create?
    # access control is in the action itself
    true
  end

  def destroy?
    allow_user?
  end

  def set_as_primary_photo?
    allow_user?
  end

  protected

  def allow_user?
    user.administrator? || (user == record.imageable)
  end
end
