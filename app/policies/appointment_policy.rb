class AppointmentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    # access control is in the action itself
    true
  end

  def new?
    # access control is in the action itself
    true
  end

  def create?
    # access control is in the action itself
    true
  end

  def request_schedule?
    # access control is in the action itself
    true
  end

  def show?
    allow_appointment_users?
  end

  def decline?
    allow_student?
  end

  def confirm?
    allow_student?
  end

  def cancel?
    allow_appointment_users?
  end

  protected

  def allow_appointment_users?
    user.administrator? || user == record.student.user || user == record.mentor.user
  end

  def allow_mentor?
    user.administrator? || (user == record.mentor.user && user.mentor?)
  end

  def allow_student?
    user.administrator? || (user == record.student.user && user.student?)
  end
end
