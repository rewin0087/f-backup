class BookmarkPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    user.student?
  end

  def destroy?
    user.student? && user == record.student.user
  end
end
