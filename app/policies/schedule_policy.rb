class SchedulePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    # access control is in the action itself
    true
  end

  def checker?
    # access control is in the action itself
    true
  end

  def new?
    # access control is in the action itself
    true
  end

  def create?
    # access control is in the action itself
    true
  end

  def edit?
    allow_student? || allow_mentor?
  end

  def update?
    allow_student?
  end

  def destroy?
    allow_student?
  end

  def close?
    allow_student?
  end

  protected

  def allow_mentor?
    user.administrator? || (user == record.mentor.user && user.mentor?)
  end

  def allow_student?
    user.administrator? || (user == record.student.user && user.student?)
  end
end
