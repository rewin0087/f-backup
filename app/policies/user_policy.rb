class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    (user.mentor? && record.student?) || (user.student? && record.mentor?) || allow_user?
  end

  def edit?
    allow_user?
  end

  def update?
    allow_user?
  end

  def primary_photo?
    allow_user?
  end

  def students?
    allow_mentor?
  end

  def mentors?
    allow_student?
  end

  def account_settings?
    allow_user?
  end

  def private_message?
    allow_user?
  end

  protected

  def allow_mentor?
    user.administrator? || (user == record  && user.mentor?)
  end

  def allow_student?
    user.administrator? || (user == record  && user.student?)
  end

  def allow_user?
    user.administrator? || user == record
  end
end
