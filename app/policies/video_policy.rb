class VideoPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    # access control is in the action itself
    true
  end

  def destroy?
    user.administrator? || (user == record.videoable.user && user.mentor?)
  end
end
