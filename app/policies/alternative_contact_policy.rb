class AlternativeContactPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    # access control is in the action itself
    true
  end

  def update?
    allow?
  end

  def destroy?
    allow?
  end

  protected

  def allow?
    user.administrator? || user == record.contact.user
  end
end
