class MessengerPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def video_call?
    (participant? && record.appointment.approved?)
  end

  def decline_video_call?
    (participant? && record.appointment.approved?)
  end

  def show?
    participant? || user.administrator?
  end

  def message?
    allowed?
  end

  private

  def allowed?
    (participant? && record.appointment.approved?) || user.administrator?
  end

  def participant?
    record.messenger_participants.find_by(user: user)
  end
end