class SchoolPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    # access control is in the action itself
    true
  end

  def update?
    allow_user?
  end

  def destroy?
    allow_user?
  end

  protected

  def allow_user?
    user.administrator? || (user == record.scholastic.user)
  end
end
