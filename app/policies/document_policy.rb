class DocumentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    # access control is in the action itself
    true
  end

  def create?
    # access control is in the action itself
    true
  end

  def destroy?
    user.administrator? || (user == record.documentable.user && user.mentor?)
  end
end
