class TaskPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    allow_task_users?
  end

  def update?
    allow_student?
  end

  def upload_request_file?
    allow_student?
  end

  def upload_revised_file?
    allow_mentor?
  end

  def in_progress?
    allow_task_users?
  end

  def done?
    allow_mentor?
  end

  def completed?
    allow_student?
  end

  def instruction?
    allow_student?
  end

  def comment?
    allow_mentor?
  end

  def appointment?
    allow_mentor?
  end

  protected

  def allow_task_users?
    user.administrator? || user == record.student.user || user == record.mentor.user
  end

  def allow_mentor?
    user.administrator? || (user == record.mentor.user && user.mentor?)
  end

  def allow_student?
    user.administrator? || (user == record.student.user && user.student?)
  end
end
