class OrderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    # access control is in the action itself
    true
  end

  def new?
    # access control is in the action itself
    true
  end

  def create?
    # access control is in the action itself
    true
  end

  def show?
    allow_order_users?
  end

  def instruction?
    allow_student?
  end

  def comment?
    allow_mentor?
  end

  def accept?
    allow_mentor?
  end

  def confirm?
    allow_student?
  end

  def in_progress?
    allow_student?
  end

  def completed?
    allow_student?
  end

  def decline?
    allow_mentor?
  end

  def cancel?
    allow_student?
  end

  def close?
    allow_student?
  end

  protected

  def allow_order_users?
    user.administrator? || user == record.student.user || user == record.mentor.user
  end

  def allow_mentor?
    user.administrator? || (user == record.mentor.user && user.mentor?)
  end

  def allow_student?
    user.administrator? || (user == record.student.user && user.student?)
  end
end
