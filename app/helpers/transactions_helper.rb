# == Schema Information
#
# Table name: transactions
#
#  id                  :integer          not null, primary key
#  transaction_type    :string(255)
#  payment_gateway     :string(255)
#  amount              :decimal(10, )
#  notification_params :text(65535)
#  status              :string(255)
#  transaction_id      :string(255)
#  purchased_at        :datetime
#  wallet_id           :integer
#

module TransactionsHelper
end
