# == Schema Information
#
# Table name: orders
#
#  id                      :integer          not null, primary key
#  additional_instructions :text(65535)
#  additional_comments     :text(65535)
#  price                   :decimal(10, )
#  actual_price            :decimal(10, )
#  currency                :string(255)
#  currency_symbol         :string(255)
#  student_id              :integer
#  mentor_id               :integer
#  status                  :string(255)      default("PENDING")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  slug                    :string(255)
#  reference_number        :string(255)
#

module OrdersHelper
end
