module ApplicationHelper
  def site_title(page_title = '', separator = " | ")
    base_title = ENV['SITE_TITLE'] || 'A Friend Abroad'
    if page_title.empty?
      base_title
    else
      [page_title, base_title].join(separator)
    end
  end

  def pending_notification_presenter(total)
    total if total > 0
  end
end
