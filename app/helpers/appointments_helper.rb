# == Schema Information
#
# Table name: appointments
#
#  id          :integer          not null, primary key
#  timezone    :string(255)
#  status      :string(255)      default("PENDING")
#  schedule_id :integer
#  mentor_id   :integer
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module AppointmentsHelper
end
