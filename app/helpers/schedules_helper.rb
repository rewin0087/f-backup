# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  start_date :date
#  start_time :time
#  end_date   :date
#  end_time   :time
#  timezone   :string(255)
#  status     :string(255)      default("AVAILABLE")
#  student_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module SchedulesHelper
  def options_for_timezone(selected_timezone)
    timezones = User::TIMEZONES
    timezones.unshift(['Please Select your Timezone',''])
    timezones.uniq!
    options_for_select timezones, selected: selected_timezone
  end

  def default_time_format datetime
    DateTime.parse(datetime).strftime('%B %e, %Y %I:%M %p').gsub('00:00:00', '') unless datetime.blank?
  end
end
