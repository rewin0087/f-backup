# == Schema Information
#
# Table name: mentors
#
#  id                                          :integer          not null, primary key
#  university_email                            :string(255)
#  home_town                                   :string(255)
#  degree                                      :text(65535)
#  undergraduate_universities                  :text(65535)
#  undergraduate_universities_early_acceptance :boolean
#  graduate_universities                       :text(65535)
#  graduate_universities_early_acceptance      :boolean
#  basic_consultation_charge                   :decimal(10, )
#  basic_consultation_charge_currency          :string(255)      default("$")
#  work                                        :string(255)
#  mentor_student                              :boolean
#  reason                                      :text(65535)
#  free_tip                                    :text(65535)
#  essay_services                              :text(65535)
#  experience                                  :text(65535)
#  awards_and_honors                           :text(65535)
#  display_ratings                             :boolean
#  display_reviews                             :boolean
#  user_id                                     :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

module MentorsHelper
  def options_for_search_tags(tags)
    selected_tags = tags.present? ? tags.map {|t| [t, t] } : []
    options_for_select selected_tags, selected: tags
  end

  def options_for_country(country)
    options_for_select [country], selected: country
  end

  def options_for_majors(majors)
    selected_majors = majors.is_a?(Array) ? majors.map {|t| [t, t] } : []
    options_for_select selected_majors, selected: majors
  end

  def options_for_langauge(language)
    return [] if language.nil?
    options_for_select [language], selected: language
  end
end
