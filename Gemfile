source 'https://rubygems.org'

gem 'active_admin_theme' # activeadmin theme
gem 'country_select' # activeadmin dependent
gem 'activeadmin', '~> 1.0.0.pre2' # admin site
gem 'ejs'
gem 'rails', '4.2.2'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'therubyracer',  platforms: :ruby
gem 'jquery-turbolinks'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'puma' # app server
gem 'thin', '~> 1.6.4' # app server
gem 'airbrake'
gem 'sdoc', '~> 0.4.0',          group: :doc
gem 'carrierwave' # uploader
gem 'fog'
gem 'mysql2', '~> 0.3.13' # database
gem 'pundit' # authorization
gem 'friendly_id', '~> 5.1.0' # pretty url
gem "select2-rails" # dropdown
gem 'devise' # authentication
gem "mini_magick" # image manipulation for carrierwave
gem 'figaro' # environment config
gem 'rails-timeago', '~> 2.0' # lazy updating of time e.g. 4 minutes ago
gem 'kaminari' # pagination
gem 'simple_form' # form helper
gem 'bootstrap-sass', '~> 3.3' # bootstrap
gem 'websocket-rails' # websocket
gem 'ionicons-rails' # icons
gem 'slim-rails' # template engine
gem 'countries' # list of countries
gem 'university_major_subjects' # list of major subjects
gem 'bootstrap-wysihtml5-rails' # editor
gem 'underscore-rails'
gem 'gon' # ruby variables to js
gem 'momentjs-rails' # moment
gem 'timezone' # timzone list
gem 'language_list', '~> 1.1.0' # language list
gem 'sidekiq' # job queue
gem "devise-async" # devise async mailer
gem 'sinatra', :require => nil # for sidekiq web ui monitoring
gem 'htmlcompressor' # compress css
gem 'dropzonejs-rails' # image | file uploader
gem 'fullcalendar-rails' # calendar
gem 'jquery-timepicker-rails' # timepicker
gem 'flipclockjs-rails', '~> 0.7.7' # countdown timer
# Video Link Scraper
gem 'open_uri_redirections' # nokigir open-uri redirection
gem 'fastimage' # image manipulation through uri
gem 'nokogiri' # html parser
# eager loading optimization
gem 'goldiloader'
gem 'multi_fetch_fragments' # frament caching
gem 'redis-rails' # for cache store
gem 'paypal-sdk-rest' # paypal
gem 'annotate' # annotate db model structure
gem 'rack-timeout' # timeout for puma
gem 'alipay' # alipay payment

group :development do
  gem 'bullet'
  gem 'rack-mini-profiler'
  gem 'query_diet'
  gem 'terminal-notifier-guard'
  gem 'spring'
  gem 'letter_opener'
  gem 'rubocop', require: false
end

group :development, :test do
  gem 'quiet_assets'
  gem 'byebug'
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'rspec-rails', '~> 3.1.0'
  gem 'rspec', '~> 3.1.0'
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'brakeman'
  gem 'guard'
  gem 'guard-rspec'
  gem 'rb-fsevent' if `uname` =~ /Darwin/
  gem 'rspec-activemodel-mocks'
end

group :test do
  gem 'shoulda'
  gem 'sqlite3'
  gem 'simplecov', require: false
  gem 'simplecov-rcov', require: false
  gem 'ci_reporter_rspec'
  gem 'rspec-its'
  gem 'webmock'
  gem 'timecop'
  gem 'capybara'
  gem 'capybara-email'
  gem 'rspec-sidekiq'
  gem 'database_cleaner'
end